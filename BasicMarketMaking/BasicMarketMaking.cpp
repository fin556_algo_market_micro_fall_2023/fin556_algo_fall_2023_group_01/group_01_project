// Refer to the header files for the explanation of the functions in detail
// In VSCode, hovering your mouse above the function renders the explanation of from the .h file as a pop up
#ifdef _WIN32
    #include "stdafx.h"
#endif

#include "BasicMarketMaking.h"

using namespace RCM::StrategyStudio;
using namespace RCM::StrategyStudio::MarketModels;
using namespace RCM::StrategyStudio::Utilities;

using namespace std;

// Constructor to initialize member variables of the class to their initial values.
BasicMarketMaking::BasicMarketMaking(StrategyID strategyID,
                    const string& strategyName,
                    const string& groupName):
    Strategy(strategyID, strategyName, groupName),
    market_state(0),
    debug_(false),
    inventory_liquidation_decrement(20),
    max_inventory(100),
    regime_map_(),
    vol_map_(),
    long_window_size(30),
    short_window_size(50),
    vol_window_size(30),
    threshold(0.1),
    strategy_active(true),
    inventory_liquidation_interval(20),
    bar_interval(1)
    {}
// Destructor for class
BasicMarketMaking::~BasicMarketMaking(){

}

void BasicMarketMaking::DefineStrategyParams(){
}

void BasicMarketMaking::DefineStrategyCommands(){
}

// By default, SS will register to trades/quotes/depth data for the instruments you have requested via command_line.
void BasicMarketMaking::RegisterForStrategyEvents(StrategyEventRegister* eventRegister, DateType currDate)
{    
    for (SymbolSetConstIter it = symbols_begin(); it != symbols_end(); ++it) {
        eventRegister->RegisterForBars(*it, BAR_TYPE_TIME, bar_interval);
    }
    eventRegister->RegisterForRecurringScheduledEvents("liquidation_time",USEquityCloseUTCTime(currDate)-boost::posix_time::minutes((inventory_liquidation_decrement+1)*inventory_liquidation_interval),USEquityCloseUTCTime(currDate)-boost::posix_time::minutes(inventory_liquidation_interval),boost::posix_time::minutes(inventory_liquidation_interval));
    eventRegister->RegisterForSingleScheduledEvent("terminate strategy",USEquityCloseUTCTime(currDate)-boost::posix_time::minutes(inventory_liquidation_interval));
    eventRegister->RegisterForRecurringScheduledEvents("print stats",USEquityOpenUTCTime(currDate),USEquityCloseUTCTime(currDate),boost::posix_time::minutes(10));
    ReadParameters();
}

void BasicMarketMaking::OnTrade(const TradeDataEventMsg& msg) {
    
    const Instrument* instrument = &msg.instrument();
    RecordAccountStats(instrument);
}

void BasicMarketMaking::RecordAccountStats(const Instrument* instrument){
    cout << "Account stats" << endl;
    cout << "Cash: " << portfolio().position(instrument) << endl;
    cout << "Total PnL: " << portfolio().total_pnl() << endl;
    cout << "Unrealized PnL: " << portfolio().unrealized_pnl() << endl;
    cout << "Realized PnL: " << portfolio().realized_pnl() << endl;
}

void BasicMarketMaking::OnScheduledEvent(const ScheduledEventMsg& msg) {
    if (msg.name() == "liquidation_time"){
        InventoryLiquidation();
    }
    if (msg.name() == "terminate strategy"){
        TerminateStrategy();
    }
}

void BasicMarketMaking::TerminateStrategy(){
    strategy_active = false;
}

void BasicMarketMaking::OnOrderUpdate(const OrderUpdateEventMsg& msg) {
}

void BasicMarketMaking::OnBar(const BarEventMsg& msg){ 

    RegimeMapIterator regime_iter = regime_map_.find(&msg.instrument());
    if (regime_iter == regime_map_.end()) {
        regime_iter = regime_map_.insert(make_pair(&msg.instrument(), MarketRegime(short_window_size,long_window_size))).first;
    }

    VolatilityMapIterator vol_map = vol_map_.find(&msg.instrument());
    if (vol_map == vol_map_.end()) {
        vol_map = vol_map_.insert(make_pair(&msg.instrument(), VolatilityEstimator(vol_window_size))).first;
    }

    double price = CalculateMidPrice(&msg.instrument());
    double indicator = regime_iter->second.Update(price);
    double vol = vol_map->second.Update(price);

    cout << "volatility: " << vol << endl;
    cout << "indicator: " << indicator << endl;
    cout << "price: " << price << endl;

    if (regime_iter->second.FullyInitialized()) {
        if (indicator > threshold){
            cout << "bullish market" << endl;
            market_state = 1;
        }
        else if (indicator < -threshold){
            cout << "bearish market" << endl;
            market_state = -1;
        }
        else{
            cout << "neutral market" << endl;
            market_state = 0;
        }
    }
    cout << "market state: " << market_state << endl;
}

double BasicMarketMaking::CalculateMidPrice(const Instrument* instrument){
    return (instrument->top_quote().bid() + instrument->top_quote().ask())/2;
}

void BasicMarketMaking::OnQuote(const QuoteEventMsg& msg){
}

void BasicMarketMaking::InventoryLiquidation(){
    max_inventory -= inventory_liquidation_decrement;
    if (max_inventory < 0)
        max_inventory = 0;

    if (debug_){
        cout << "liquidating inventory by " << inventory_liquidation_decrement << endl;
        cout << "max inventory is now " << max_inventory << endl;
    }
}

void BasicMarketMaking::SetInventoryParams(const Instrument* instrument){
    if (max_inventory == -1){
        double price = CalculateMidPrice(instrument);
        cout << price << endl;
        if (std::isnan(price)) 
            return;
        cout << price << " " << portfolio().initial_cash() << endl;
        max_inventory = int(portfolio().initial_cash()/price);
        if (debug_)
            cout << "max inventory initially set to " << max_inventory << endl;
        inventory_liquidation_decrement = int(max_inventory/inventory_liquidation_decrement)+1;
        if (debug_)
            cout << "liquidation decrement :- " << inventory_liquidation_decrement << endl;
    }
}

void BasicMarketMaking::ReadParameters(){
    string line;
    ifstream myfile("/home/vagrant/ss/sdk/RCM/StrategyStudio/examples/strategies/group_01_project/RegressionMeanReversion/parameters.txt"); 
    if (myfile.is_open()) {
        while ( getline (myfile, line) ) {
            string key;
            string value;
            istringstream iss(line);
            getline(iss, key, '='); // Extract string up to the '=' character
            getline(iss, value);
            SetParamFromKey(key,value);
        }
        myfile.close();
    } else {
        cout << "Unable to open file to read parameters from file" << endl;
    }
}

void BasicMarketMaking::SetParamFromKey(string key, string value){

    if (key == "debug_"){
        debug_ = (value == "true" || value == "1");
    }
    else if (key == "max_inventory"){
        max_inventory = stoi(value);
    }
    else if (key == "long_window_size"){
        long_window_size = stoi(value);
    }
    else if (key == "short_window_size"){
        short_window_size = stoi(value);
    }
    else if (key == "threshold"){
        threshold = stod(value); // Assuming it's a double
    }
    else if (key == "inventory_liquidation_decrement"){
        inventory_liquidation_decrement = stoi(value);
    }
    else if (key == "inventory_liquidation_interval"){
        inventory_liquidation_interval = stoi(value);
    }
    else if (key == "bar_interval"){
        bar_interval = stoi(value);
    }
}


void BasicMarketMaking::OnMarketState(const MarketStateEventMsg& msg){
    cout << msg.name() << endl;
}

void BasicMarketMaking::OnResetStrategyState() {
}

void BasicMarketMaking::OnParamChanged(StrategyParam& param) {
}
