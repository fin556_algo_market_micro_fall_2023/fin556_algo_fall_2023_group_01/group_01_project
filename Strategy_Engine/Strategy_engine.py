__author__ = "Samanvay Malapally Sudhakara"
'''
Order book matching engine and strategy backtesting engine
'''
import sys
import csv
import gzip
import os
import re
from datetime import datetime, timezone, timedelta

    
class Orderbook():
    def __init__(self):
        self.bid_orderbook = {} # create the initial orderbook as a dictionary
        self.ask_orderbook = {} # create the initial orderbook as a dictionary
        
    def update(self, row):
        price = float(row[2]) # price is the key
        qty = float(row[3]) # qty is the value
        order_type = row[1] # order type helps decide whether to match or add to the orderbook
        if qty >= 0 and order_type != 'take': # if qty is positive, it is a bid order
            self.bid_orderbook[price] = qty 
        elif qty <= 0 and order_type != 'take': # if qty is negative, it is an ask order
            self.ask_orderbook[price] = abs(qty)
        else: # if order type is take, it is a Trade order
            if qty >= 0: # if qty is positive, it is a bid order
                print('take ask')
                diff = self.ask_orderbook[price] - qty # calculate the difference between the orderbook and the trade
                if diff > 0: # if the difference is positive we still have orders at this position so we update the order book
                    self.ask_orderbook[price] = diff
                elif diff < 0: # if the difference is negative, we have no orders at this position on the ask side so we add the order to the bid side
                    self.bid_orderbook[price] = -diff # we add the order to the bid side with a positive quantity
                    del self.ask_orderbook[price] # we delete the order from the ask side
                else:
                    del self.ask_orderbook[price] # delete the price level if the quantity is 0
            else: # if qty is negative, it is an ask order
                print('hit bid') # same logic as above but for the bid side
                diff = self.bid_orderbook[price] + qty # calculate the difference between the orderbook and the trade
                if diff > 0: # if the difference is positive we still have orders at this position so we update the order book
                    self.bid_orderbook[price] = diff # we update the order book with the new quantity
                elif diff < 0: # if the difference is negative, we have no orders at this position on the bid side so we add the order to the ask side
                    self.ask_orderbook[price] = -diff # we add the order to the ask side with a positive quantity
                    del self.bid_orderbook[price] # we delete the order from the bid side
                else: # if the difference is 0, we have no orders at this price level so we delete the price level
                    del self.bid_orderbook[price] # delete the price level if the quantity is 0
    
    def get_best_bid_offer(self):
        best_bid = max(self.bid_orderbook.keys()) # get the best bid price
        best_offer = min(self.ask_orderbook.keys()) # get the best ask price
        return best_bid, best_offer
    
    def get_order_book_levels(self,levels):
        if levels == -1: # if levels is -1, return the entire sorted orderbook
            return sorted(self.bid_orderbook.keys(), reverse=True), sorted(self.ask_orderbook.keys())
        bids = sorted(self.bid_orderbook.keys(), reverse=True)[:levels] # get the top n bids
        asks = sorted(self.ask_orderbook.keys())[:levels] # get the top n asks
        return bids, asks
    
    def get_mid_price(self,weighted=False,levels = 1,lot_weighted=0):
        if weighted: # if weighted is true, return the weighted mid price
            bids,asks = self.get_order_book_levels(levels) # get the top n bids and asks
            bid_qty_total = sum([self.bid_orderbook[bid] for bid in bids]) # get the total quantity of bids
            ask_qty_total = sum([self.ask_orderbook[ask] for ask in asks]) # get the total quantity of asks
            bid_sum = 0 # initialize the bid sum
            ask_sum = 0 # initialize the ask sum
            for bid in bids: # iterate through the bids
                bid_sum += self.bid_orderbook[bid]*bid # add the bid price multiplied by the quantity to the bid sum
            for ask in asks: # iterate through the asks
                ask_sum += self.ask_orderbook[ask]*ask # add the ask price multiplied by the quantity to the ask sum
            return (bid_sum + ask_sum)/(bid_qty_total + ask_qty_total) # return the weighted mid price
        elif lot_weighted > 0: # if lot_weighted is greater than 0, return the lot weighted mid price
            bids,asks = self.get_order_book_levels(levels) # get the top n bids and asks
            bid_sum = 0 # initialize the bid sum
            ask_sum = 0 # initialize the ask sum
            bid_value = 0 # initialize the bid value
            ask_value = 0 # initialize the ask value
            lots = lot_weighted # initialize the lots variable
            for bid in bids: # iterate through the bids
                bid_sum += min(self.bid_orderbook[bid],lots)*bid # checks if the quantity at the price level is greater than the lots variable, if it is, it adds the lots variable multiplied by the price to the bid sum
                lots = abs(lots - self.bid_orderbook[bid]) # lots left to used for calculation
                if lots == 0: # if lots is 0, we have used all the lots so we break
                    break
            if lots == 0:
                bid_value = bid_sum/lot_weighted # if lots is 0, we have used all the lots so we calculate the bid value
            else:
                bid_value = bid_sum/(lot_weighted-lots) # if lots is not 0, we have not used all the lots so we calculate the bid value
            lots = lot_weighted # reset the lots variable
            while lots != 0: # iterate through the asks
                ask_sum += min(self.ask_orderbook[ask],lots)*ask # checks if the quantity at the price level is greater than the lots variable, if it is, it adds the lots variable multiplied by the price to the ask sum
                lots = abs(lots - self.ask_orderbook[ask]) # lots left to used for calculation
                if lots == 0: # if lots is 0, we have used all the lots so we break
                    break
            if lots == 0:
                ask_value = ask_sum/lot_weighted # if lots is 0, we have used all the lots so we calculate the bid value
            else:
                ask_value = ask_sum/(lot_weighted-lots) # if lots is not 0, we have not used all the lots so we calculate the bid value
            return (bid_value + ask_value)/2 # return the lot weighted mid price
        else: # if weighted and lot_weighted are false, return the regular mid price
            best_bid, best_offer = self.get_best_bid_offer() # get the best bid and best offer
            return (best_bid + best_offer)/2 # return the mid price

class Matching_Engine(Orderbook):
    def __init__(self):
        super().__init__()
        
    @staticmethod
    def unix_to_utc(unix_time):
        utc_time = datetime.fromtimestamp(float(unix_time), timezone.utc)
        return utc_time.strftime('%Y-%m-%d %H:%M:%S.%f')

    def run_simulation(self,filepath,mid_price_method='mid_price',levels=1,lot_weighted=1):
        mid_prices = {}
        weighted_mid_prices = {}
        lot_weighted_mid_prices = {}
        with gzip.open(filepath, 'rt') as f:
            reader = csv.reader(f)
            for row in reader:
                self.update(row)
                mid_prices[self.unix_to_time(row[0])] = self.get_mid_price()
                weighted_mid_prices[self.unix_to_time(row[0])] = self.get_mid_price(weighted=True,levels=5)
                lot_weighted_mid_prices[self.unix_to_time(row[0])] = self.get_mid_price(lot_weighted=100)
        return mid_prices, weighted_mid_prices, lot_weighted_mid_prices


        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
class Strategy(Orderbook):
    def __init__(self, Fees):
        super().__init__()
        self.inventory = 0
        self.realised_PNL = 0
        self.unrealised_PNL = 0
        self.total_PNL = 0
        self.total_fees = Fees
        self.trades = {}
        
