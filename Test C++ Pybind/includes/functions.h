#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <iostream>

int add(int, int);

int sub(int, int);

class message
{
    public:
        void output_message(std::string);
};

// struct Pet {
//     Pet(const std::string &name) : name(name) { }
//     void setName(const std::string &name_) { name = name_; }
//     const std::string &getName() const { return name; }

//     std::string name;
// };

// // Derive Dog from Pet
// struct Dog : Pet {
//     Dog(const std::string &name) : Pet(name) { }
//     std::string bark() const { return "woof!"; }
// };

struct Pet
{
    public:
        Pet(const std::string &name);
        void setName(const std::string &name_);
        const std::string &getName() const;
    // private:
        std::string name;

};

// Derive Dog from Pet
struct Dog : Pet
{
    Dog(const std::string &name);

    std::string bark() const;
};


// Base class
struct PolymorphicPet {
    virtual ~PolymorphicPet() = default;
    virtual std::string bark() const = 0; // Making it a pure virtual function if you want it to be overridden.
};

// Derived class
struct PolymorphicDog : PolymorphicPet {
    std::string bark() const override; // Declaration only
};

struct Pet_overloaded
{
    Pet_overloaded(const std::string &name, int age);
    void set(int &age_);
    void set(const std::string &name_);

    std::string name;
    int age;
};

// class MyClass
// {
//     public:
//         MyClass(double x);

//         void set(double x);
//         double get() const;
//         double div(double y) const;

//     private:
//         double x_;
// };

#endif // FUNCTIONS_H
