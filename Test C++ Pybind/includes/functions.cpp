#include <iostream>
#include "functions.h"

int add(int x, int y)
{
    return x + y;
}

int sub(int x, int y)
{
    return x - y;
}

void message::output_message(std::string message)
{
    std::cout << message << std::endl;
}


Pet::Pet(const std::string &name) : name(name){}

void Pet::setName(const std::string &name_)
{
    name = name_;
}

const std::string &Pet::getName() const
{
    return name;
}


Dog::Dog(const std::string &name) : Pet(name) {}

std::string Dog::bark() const
{
    return "woof!";
}

std::string PolymorphicDog::bark() const {
    return "woof!"; // Definition
}


Pet_overloaded::Pet_overloaded(const std::string &name, int age) : name(name), age(age){}

void Pet_overloaded::set(int &age_)
{
    age = age_;
}

void Pet_overloaded::set(const std::string &name_)
{
    name = name_;
}


// MyClass::MyClass(double x) : x_(x) {}  // This line was incorrect. The `class::` part is not required.

// void MyClass::set(double x) {
//     x_ = x;
// }

// double MyClass::get() const {
//     return x_;
// }

// double MyClass::div(double y) const {
//     if(y == 0.0) {
//         // Handle division by zero, maybe throw an exception or return a default value
//         throw std::runtime_error("Division by zero is not allowed.");
//     }
//     return x_ / y;
// }