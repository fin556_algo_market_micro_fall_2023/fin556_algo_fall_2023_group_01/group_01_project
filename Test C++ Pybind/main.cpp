#include <iostream>
#include <string>
#include "includes/functions.h"

// Pybind includes would work but intellisense would not detect it. Modify your c_cpp_properties.json by adding "/usr/include/python3.10/" to includePath
#include "pybind11/include/pybind11/pybind11.h"
// #include "pybind11/include/pybind11/stl.h"

using namespace std;
namespace py = pybind11;
using namespace pybind11::literals; // for shorthand notation (see multiply)

template <typename... Args>
using overload_cast_ = pybind11::detail::overload_cast_impl<Args...>;

int multiply(int i, int j) {
    return i * j;
}

PYBIND11_MODULE(output, m) {
    m.doc() = "pybind11 example plugin"; // optional module docstring

    m.def("sub", &sub);
    m.def("add", &add, "A function that adds two numbers",
        py::arg("x"), py::arg("y"));
    
    m.def("multiply", &multiply, "Function that multiplies 2 numbers", "i"_a=1, "j"_a=2);

    m.attr("the_answer") = 42;
    py::object world = py::cast("World");
    m.attr("what") = world;

    py::class_<Pet>(m, "Pet", py::dynamic_attr()) // class_ is for both struct and class
        .def(py::init<const std::string &>())
        .def_readwrite("name", &Pet::name)

        // If name is private
        // .def_property("name", &Pet::getName, &Pet::setName)
        // also a way to only expose getter as readonly

        .def("setName", &Pet::setName)
        .def("getName", &Pet::getName);

    py::class_<Dog, Pet>(m, "Dog")
        .def(py::init<const std::string &>())
        .def("bark", &Dog::bark);
        // Return a base pointer to a derived instance
    m.def("pet_store", []() { return std::unique_ptr<Pet>(new Dog("Molly")); });

    // Same binding code
    py::class_<PolymorphicPet>(m, "PolymorphicPet");
    py::class_<PolymorphicDog, PolymorphicPet>(m, "PolymorphicDog")
        .def(py::init<>())
        .def("bark", &PolymorphicDog::bark);

    // Again, return a base pointer to a derived instance
    m.def("pet_store2", []() { return std::unique_ptr<PolymorphicPet>(new PolymorphicDog); });

    py::class_<Pet_overloaded>(m, "Pet_overloaded")
        .def(py::init<const std::string &, int>())
        
        // // Raw cast
        // .def("set", static_cast<void (Pet_overloaded::*)(int &)>(&Pet_overloaded::set), "Set the pet's age")
        // .def("set", static_cast<void (Pet_overloaded::*)(const std::string &)>(&Pet_overloaded::set), "Set the pet's name")
        
        // // Below is for c++14 compatible compilers
        // .def("set", py::overload_cast<int &>(&Pet_overloaded::set), "Set the pet's age")
        // .def("set", py::overload_cast<const std::string &>(&Pet_overloaded::set), "Set the pet's name")
        .def("set", overload_cast_<int &>()(&Pet_overloaded::set), "Set the pet's age")
        .def("set", overload_cast_<const std::string &>()(&Pet_overloaded::set), "Set the pet's name")
        
        .def_readwrite("age", &Pet_overloaded::age)
        .def_readwrite("name", &Pet_overloaded::name);

    // Returns basePtr in c++: Base* basePtr = new Derived();

    // // Binding for MyClass
    // py::class_<MyClass>(m, "MyClass")
    //     .def(py::init<double>())  // Constructor
    //     .def("set", &MyClass::set)
    //     .def("get", &MyClass::get)
    //     .def("div", &MyClass::div);
}

#ifndef PYBIND11_MODULE
int main()
{
    cout << "Starting program..." << endl;
    cout << add(3,4) << "\n";

    string sample_message = "This message is printed by header file";
    message m;
    m.output_message(sample_message);

    Pet myPet("Buddy");
    std::cout << myPet.getName() << std::endl; // Outputs "Buddy"
    
    myPet.setName("Charlie");
    std::cout << myPet.getName() << std::endl; // Outputs "Charlie"

    // Create a Dog object
    Dog myDog("Rex");
    std::cout << myDog.getName() << std::endl; // Outputs "Rex"

    // Call the bark method
    std::cout << myDog.bark() << std::endl; // Outputs "woof!"

    // MyClass(20.0);

}
#endif
