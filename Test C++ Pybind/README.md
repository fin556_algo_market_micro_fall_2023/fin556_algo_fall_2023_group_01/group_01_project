#### Understanding things

From this folder, you can understand the following:

- Makefile
- How cpp and h files work together
- How to define in cpp and h files and hide source code from main user by just using signatures in the .h file
- Pybind
- How Pybind has been implemented for our use case for Strategy Studio

This code was written inside a Linux environment.

To run:
- clone this repo and cd into it
- clone pybind official repo from this link: https://github.com/pybind/pybind11

Commands to run
- Use the 'make' command to build the project
- Then you can run the python script
- Use 'make clean' to remove the built .so file

Note:
- Download the compiler to compile the code in your linux environment
- Make sure you have installed python inside your linux environment
- You can change some stuff on the Makefile and get an executable instead which you can run on your terminal to view the output of main.cpp inside your terminal
- Use chatgpt to explain line by line, that helps alot