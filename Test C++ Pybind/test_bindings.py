import output

# We can uncomment checkpoints and see the outputs on the terminal.

def main():
    print(dir(output)) # lists all functions we can use in python

    ######## Checkpoint 1
    result1 = output.add(5, 3)
    result2 = output.sub(10,5)
    result3 = output.multiply(5, 3)
    print(result1)  # Should print 8
    print(result2)  # Should print 5
    print(result3)  # Should print 15
    print(output.the_answer)
    print(output.what)
    ########

    ######## Checkpoint 2
    p = output.Pet("Buddy")
    print(p) # output.Pet object
    print(p.name) # variable exposed directly
    print(p.getName()) # should print Buddy
    # # When name is a private variable (make the change in functions.h to test)
    # p = Pet("Mittens")  # Calls the C++ Pet constructor
    # print(p.name)       # Calls the C++ Pet::getName method
    # p.name = "Whiskers" # Calls the C++ Pet::setName method
    # print(p.name)

    p.setName("Charlie")
    print(p.getName()) # should print Charlie

    p.age = 2 # not defined in c++, dynamically created
    print(p.age)
    ########

    ######## Checkpoint 3
    # Dog is a derived struct
    t = output.Dog("Doggy")
    print(t.name)
    print(t.bark())
    t = output.pet_store()
    print(type(t)) # Returns Pet as no downcasting for regular non polymorphic type
    # print(t.bark()) # Try running this line and it would throw an AttributeError as this is non polymorphic

    p = output.pet_store2()
    print(type(p)) # should print PolymorphicDog because it is downcast automatically
    print(p.bark())
    ########

    ######### Checkpoint 4
    p = output.Pet_overloaded("Cat", 3)
    print(type(p))
    print(p.name)
    # print(help(output.Pet_overloaded)) # this will show a docstring, exit by using q in terminal
    # Examples of overloaded calls
    p.set(25)
    print(p.age)
    p.set("Kitten")
    print(p.name)
    ##########

    ########## Checkpoint 5
    # # Create an instance of MyClass
    # obj = output.MyClass(5.0)  # This calls the constructor

    # # Use the set method
    # obj.set(10.0)

    # # Use the get method
    # print(obj.get())  # This should print 10.0

    # # Use the div method
    # print(obj.div(2.0))  # This should print 5.0


if __name__ == "__main__":
    main()
