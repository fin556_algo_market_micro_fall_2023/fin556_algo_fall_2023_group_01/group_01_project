import dask.dataframe as dd
import pandas as pd
import matplotlib.pyplot as plt
from pathlib import Path
import warnings
from itertools import combinations
from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from pandas.errors import SettingWithCopyWarning
import time
from tqdm import tqdm
import pairs_trading as pt
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=SettingWithCopyWarning)

data_path = Path("data")
crsp_file = "CRSP_1925-12-31_1982-12-31.csv"
# From 1962 ~ 1982
crsp_col_names = {
    "date": "date",         # timestamp
    "PERMNO": "permno",     # unique identifier for individual securities   
    "TICKER": "ticker",     # ticker symbols associated with the securities
    "RET": "ret",           # Price Returns
    "VOL": "vol",           # trading volume
    "SICCD": "sic",         # Standard Industrial Classification (SIC) code
    "PRC": "price",         # Stock Price
    "DIVAMT": "dividends",  # dividends
    "SHRCD": "class"        # share class
}
# From 1983 to 2002
crsp_cs_files = [
    "CRSP_CS_1983-01-01_1989-12-31.csv",
    "CRSP_CS_1990-01-01_1999-12-31.csv",
    "CRSP_CS_2000-01-01_2009-12-31.csv",
    # "CRSP_CS_2010-01-01_2020-12-31.csv",
]
crsp_cs_cols = [
    "datadate",
    "LPERMNO",
    "tic",
    "cshtrd",
    "sic",
    "prccd",
    "ajexdi",
    "trfd"
]
crsp_cs_col_names = {
    "datadate": "date",     # timestamp
    "LPERMNO": "permno",    # unique identifier for individual securities 
    "tic": "ticker",        # ticker symbols associated with the securities
    "cshtrd": "vol",     # trading volume
    "sic": "sic",           # Standard Industrial Classification (SIC) code
    "prccd": "price"
}

sic_financials = {2741, 3600, 5051, 5812, 5900, 6020, 6035, 6036, 6111, 6141,
                  6153, 6159, 6162, 6163, 6172, 6199, 6200, 6211, 6282, 6311,
                  6321, 6324, 6331, 6351, 6361, 6411, 6797, 6798, 6799, 7323,
                  7359, 7370, 7389, 8741, 9995, 9997}

sic_transportation = {4011, 4100, 4210, 4213, 4400, 4412, 4512, 4513, 4522, 4581,
                      4731, 6159, 7370, 7510}

sic_industry = {1540, 1600, 1623, 1700, 1731, 2221, 2400, 2421, 2430, 2452, 2670, 2800, 2821, 2950, 3050, 3080,
                      3089, 3290, 3310, 3317, 3350, 3357, 3420, 3430, 3440, 3442, 3443, 3448, 3452, 3460, 3480, 3490,
                      3510, 3523, 3530, 3531, 3533, 3537, 3540, 3550, 3559, 3560, 3561, 3562, 3567, 3569, 3577, 3578,
                      3580, 3585, 3612, 3613, 3620, 3621, 3630, 3640, 3663, 3670, 3672, 3674, 3679, 3690, 3711, 3713,
                      3714, 3715, 3720, 3721, 3724, 3728, 3730, 3743, 3760, 3812, 3823, 3825, 3829, 4700, 5000, 5030,
                      5031, 5051, 5063, 5065, 5070, 5072, 5080, 5084, 5090, 5110, 5160, 5211, 5500, 6512, 7350, 7359,
                      7370, 8711, 8731, 8744, 9997}

sic_utilities = {4911, 4923, 4924, 4931, 4932, 4941, 4950, 4991, 5900}


def data_preprocessing(starting_date: str) -> (dict, dict):
    starting_time = time.time()
    date_format = '%Y-%m-%d'
    date_start = datetime.strptime(starting_date, date_format).date()
    if date_start < date(1962, 7, 1):
        RuntimeWarning('The starting time is 1962-7-1')
    pairing_ending_date = date_start + relativedelta(months=+12)
    ending_date = date_start + relativedelta(months=+18)
    ending_date = ending_date.strftime('%Y-%m-%d')
    pairing_ending_date = pairing_ending_date.strftime('%Y-%m-%d')
    if date_start < date(1982, 1, 1):
        print("Read all the data to a dask dataframe")
        df_crsp_data = pd.read_parquet('data/data_before_1983.parquet')
    else:
        df_crsp_data = pd.read_parquet('data/data_after_1983.parquet')

    print(f"cut the original crsp to data from {starting_date} to {ending_date}")
    data_for_trading = (df_crsp_data[(df_crsp_data['date'] >= pairing_ending_date)
                                     & (df_crsp_data['date'] <= ending_date)]
                        .sort_values(by='date').set_index('date'))
    data_for_pairing = (df_crsp_data[(df_crsp_data['date'] >= starting_date)
                                     & (df_crsp_data['date'] <= pairing_ending_date)]
                        .sort_values(by='date').set_index('date'))

    data_columns = ['permno', 'ajexdi', 'vol', 'price', 'trfd']
    if starting_date > '1982-01-01':
        data_for_pairing = data_for_pairing.drop_duplicates(subset=data_columns)
        data_for_trading = data_for_trading.drop_duplicates(subset=data_columns)

    ending_time = time.time()
    print(f'finished! using {round(ending_time - starting_time, 1)} seconds')

    unique_sic_count, unique_permno_count, unique_ticker_count = (data_for_pairing['sic'],
                                                                  data_for_pairing['permno'],
                                                                  data_for_pairing['ticker'])
    for series in [unique_sic_count, unique_permno_count, unique_ticker_count]:
        print(f"{series.nunique()} unique {series.name}")

    data_for_pairing = data_filter(data_for_pairing, starting_date)

    paring_data_set = {}
    trading_data_set = {}
    pairing_permnos = data_for_pairing['permno'].unique()
    pairing_groups = data_for_pairing.groupby('permno')
    trading_groups = data_for_trading.groupby('permno')
    permno_to_remove = []

    # Setting two dict, one is trading, one is pairing
    for permno in pairing_permnos:
        paring_data_set[permno] = pairing_groups.get_group(permno)
        if permno not in trading_groups.groups:
            permno_to_remove.append(permno)
            continue
        else:
            trading_data_set[permno] = trading_groups.get_group(permno)

    # Remove permno that only appears in the training dataset
    for permno in permno_to_remove:
        del paring_data_set[permno]

    for i, permno in enumerate(tqdm(paring_data_set.keys(),
                                    desc='data return calculation', leave=True, position=0)):
        if date_start < date(1982, 1, 1):
            paring_data_set[permno] = cumulative_total_return_calculation_before_1983(paring_data_set[permno])
            trading_data_set[permno] = cumulative_total_return_calculation_before_1983(trading_data_set[permno])
        else:
            paring_data_set[permno] = cumulative_total_return_calculation_after_1983(paring_data_set[permno])
            trading_data_set[permno] = cumulative_total_return_calculation_after_1983(trading_data_set[permno])

    return paring_data_set, trading_data_set
    

def read_data_before_1983(print_columns: bool = True) -> dd:
    df_crsp_only = dd.read_csv(
        urlpath=data_path/crsp_file,
        assume_missing=True,
        usecols=list(crsp_col_names.keys()),
        parse_dates=["date"],
        na_values={
            "RET": ["B", "C"],
            "SICCD": ["Z"]
        },
        dtype={
            "TICKER": "object"
        }
    ).rename(columns=crsp_col_names)
    print(df_crsp_only.columns)
    df_crsp_only.to_parquet('data/data_before_1983')
    return df_crsp_only


def read_data_after_1983(print_columns: bool = True) -> dd:
    dfs_crsp_cs = []
    # now combine the crsp/cs datasets
    for file in crsp_cs_files:
        print(f"Processing {file}...")
        df = dd.read_csv(
            urlpath=data_path / file,
            assume_missing=True,
            usecols=crsp_cs_cols,
            parse_dates=["datadate"],
        ).rename(columns=crsp_cs_col_names)
        dfs_crsp_cs.append(df)
        print("Done!")
    data = dd.concat(dfs_crsp_cs)
    data.to_parquet('data/data_after_1983')
    if print_columns:
        print(data.columns)
    return data


'''
filter permnos with volume = 0 or NA price or trading days < trading_days
'''


def data_filter(data: pd.DataFrame, starting_date: str) -> pd.DataFrame:
    starting_time = time.time()
    print(f"filter permnos with volume = 0 or NA price")
    permnos_with_zeros = data.loc[data['vol'] == 0, 'permno'].unique()
    permnos_with_na = data.loc[data['price'].isna(), 'permno'].unique()
    data = data.loc[~data['permno'].isin(permnos_with_zeros)]
    data = data.loc[~data['permno'].isin(permnos_with_na)]
    ending_time = time.time()
    print(f'finished! using {round(ending_time - starting_time, 1)} seconds')

    permno_counts = data['permno'].value_counts()
    trading_days = permno_counts.mode().iloc[0]
    if trading_days < 200:
        data.to_csv(f'{starting_date}-low_trading_days_data.csv')
    print(f"The trading days for pairing period is {trading_days}")
    print(f"filter permnos with trading days not equals to {trading_days}")
    return data.groupby('permno').filter(lambda x: len(x) == trading_days)


def cumulative_total_return_calculation_before_1983(df: pd.DataFrame) -> pd.DataFrame:
    if 'date' in df.columns:
        df = df.set_index('date')
    df['dividends'] = df['dividends'].fillna(0)
    df['cumulative_dividends'] = df['dividends'].cumsum()
    df['adjusted'] = (df['price'] + df['cumulative_dividends'])
    df['adjusted_ret'] = df['adjusted'] / df['adjusted'].shift(1) - 1
    df['cumulative_total_adjusted_ret'] = (1 + df['adjusted_ret']).cumprod() - 1
    df.drop('cumulative_dividends', axis=1)
    return df


def cumulative_total_return_calculation_after_1983(df: pd.DataFrame) -> pd.DataFrame:
    if 'date' in df.columns:
        df = df.set_index('date')
    df['ajexdi'] = df['ajexdi'].fillna(1)
    df['trfd'] = df['ajexdi'].fillna(1)
    df['adjusted_price'] = (df['price'] / df['ajexdi'] * df['trfd'])
    df['adjusted_ret'] = df['adjusted_price'] / df['adjusted_price'].shift(1) - 1
    df['cumulative_total_adjusted_ret'] = (1 + df['adjusted_ret']).cumprod() - 1
    return df


def calculate_squared_deviation(series1, series2):
    if len(series1) != len(series2):
        raise ValueError("Two series have different length")
    squared_diff = (series1 - series2) ** 2
    sum_squared_diff = squared_diff.sum()
    return sum_squared_diff


'''
series_dic: key: permno, value: cumulative_total_adjusted_ret
return min_squares_and_keys: a list of tuples (squared_deviation_sum, (key1, key2), standard_deviation)
SIC: Utilities: 
'''


def pairs_formation(series_dic: dict, industry_sector: bool = False) -> pd.DataFrame:
    combinations_list = list(combinations(list(series_dic.keys()), 2))
    min_squares_and_keys = []
    for pair in tqdm(combinations_list, desc='pairs feature calculation'):
        key1, key2 = pair
        if series_dic[key1]['sic'].iloc[1] not in sic_utilities:
            continue
        if series_dic[key2]['sic'].iloc[1] not in sic_utilities:
            continue
        time_series_1 = series_dic[key1]['cumulative_total_adjusted_ret']
        time_series_2 = series_dic[key2]['cumulative_total_adjusted_ret']
        trading_days = len(time_series_2)
        squared_deviation_sum = calculate_squared_deviation(time_series_1, time_series_2)
        standard_deviation = (squared_deviation_sum / trading_days) ** 0.5

        min_squares_and_keys.append((squared_deviation_sum, key1, key2, standard_deviation))

    # Convert to Pandas
    pairs_df = pd.DataFrame(min_squares_and_keys,
                            columns=['squared_deviation_sum', 'key1', 'key2', 'standard_deviation'])

    del min_squares_and_keys
    print('begin pairs selection')
    starting_time = time.time()
    pairs_df = sort_pairs(len(series_dic) // 2, pairs_df)
    print(f'finished! using {round(time.time() - starting_time, 1)} seconds')

    return pairs_df


def sort_pairs(numbers: int, df: pd.DataFrame):

    result = pd.DataFrame(columns=['squared_deviation_sum', 'key1', 'key2', 'standard_deviation'])

    while not df.empty:

        idxmin = df['squared_deviation_sum'].idxmin()
        row = df.loc[idxmin]
        result.loc[len(result.index)] = row
        key_1, key_2 = row['key1'], row['key2']

        length = result.shape[0]
        df = df[df['key1'] != key_1]
        df = df[df['key1'] != key_2]
        df = df[df['key2'] != key_1]
        df = df[df['key2'] != key_2]

        if length % (numbers // 5) == 0:
            print(f"{length / numbers * 100:.0f}%", end="...")

    return result


def pair_plot(all_data: dict, pair: tuple, feature: str): # The pair is changed
    plt.figure(figsize=(8, 4))  # Adjust the figure size if needed
    plt.plot(all_data[pair[1][0]][feature], label=pair[1][0])
    plt.plot(all_data[pair[1][1]][feature], label=pair[1][1])

    plt.xlabel('Time')
    plt.ylabel(feature)
    plt.title(f'Sum of Squared Deviation: {pair[0]:.3f}')
    plt.legend()
    plt.grid(True)

    plt.show()


def date_generator(start, end):
    # Convert the input date strings to datetime objects
    start_date = datetime.strptime(start, '%Y-%m-%d')
    end_date = datetime.strptime(end, '%Y-%m-%d')

    # Initialize the result list
    date_list = []

    # Generate date strings spaced by one month
    current_date = start_date
    while current_date <= end_date:
        date_list.append(current_date.strftime('%Y-%m-%d'))
        # Increment the current date by one month
        year = current_date.year + (current_date.month + 1) // 13
        month = ((current_date.month + 1) % 13) or 1
        current_date = current_date.replace(year=year, month=month)

    return date_list


date_list_before_1983 = date_generator('1963-07-01', '1981-07-01')
date_list_after_1983 = date_generator('1984-01-01', '2000-07-01')

if __name__ == "__main__":

    # for i, t in enumerate(date_list_before_1983):
    #     pairing_data_set, trading_dataset = data_preprocessing(t)
    #     pairs = pt.back_testing(pairing_data_set, trading_dataset, t)
    #
    # for i, t in enumerate(date_list_after_1983):
    #     pairing_data_set, trading_dataset = data_preprocessing(t)
    #     pairs = pt.back_testing(pairing_data_set, trading_dataset, t)

    pairing_data_set, trading_dataset = data_preprocessing(date_list_before_1983[3]) ######
    pairs = pt.back_testing(pairing_data_set, trading_dataset, date_list_before_1983[4]) ######

    # read_data_after_1983(True)
    # read_data_before_1983(True)



