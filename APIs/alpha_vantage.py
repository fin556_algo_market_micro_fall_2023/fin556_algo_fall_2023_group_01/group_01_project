import requests
import pandas as pd
from datetime import datetime

def fetch_stock_data(start_date, end_date, stock_symbol, api_key):
    base_url = 'https://www.alphavantage.co/query'
    function = 'TIME_SERIES_INTRADAY'
    interval = '1min'
    month = start_date[:7]  # Extracting the year and month from the start date
    outputsize = 'full'
    datatype = 'json'

    url = f"{base_url}?function={function}&symbol={stock_symbol}&interval={interval}&month={month}&outputsize={outputsize}&datatype={datatype}&apikey={api_key}"

    response = requests.get(url)
    data = response.json()

    # Extracting time series data and converting to DataFrame
    time_series_key = next(key for key in data.keys() if "Time Series" in key)
    df = pd.DataFrame(data[time_series_key]).T
    df.columns = ['Open', 'High', 'Low', 'Close', 'Volume']

    # Converting index to datetime and filtering based on start and end dates
    df.index = pd.to_datetime(df.index)
    start_datetime = datetime.fromisoformat(start_date)
    end_datetime = datetime.fromisoformat(end_date)
    filtered_df = df[(df.index >= start_datetime) & (df.index <= end_datetime)]

    # Make a copy of the filtered DataFrame to avoid SettingWithCopyWarning
    final_df = filtered_df.copy()

    # Resetting index to include time as a column
    final_df.reset_index(inplace=True)
    final_df.rename(columns={'index': 'Time'}, inplace=True)

<<<<<<< HEAD
    final_df.to_csv(f"./data_csv/alphaV_{stock_symbol}.csv")
=======
    final_df.to_csv(f"../data_csv/alphaV_{stock_symbol}.csv")
>>>>>>> sz65

    return final_df

# Example usage
api_key = '123456'  # Replace with your API key
start_date = '2023-10-23'
end_date = '2023-10-30'
stock_symbol = 'AAPL'

data_frame = fetch_stock_data(start_date, end_date, stock_symbol, api_key)
print(data_frame)
