import requests
import pandas as pd
import time
from datetime import datetime

def fetch_intraday_data(stock_symbol, start_date, end_date, interval, api_key):
    base_url = 'https://eodhd.com/api/intraday'
    fmt = 'json'

    # Convert dates to UNIX timestamps
    start_timestamp = int(time.mktime(datetime.strptime(start_date, '%Y-%m-%d').timetuple()))
    end_timestamp = int(time.mktime(datetime.strptime(end_date, '%Y-%m-%d').timetuple()))

    url = f"{base_url}/{stock_symbol}?api_token={api_key}&interval={interval}&fmt={fmt}&from={start_timestamp}&to={end_timestamp}"

    response = requests.get(url)


    # Check if the response was successful
    if response.status_code != 200:
        print(f"Error: Received status code {response.status_code}")
        print("Response:", response.text)
        return None

    try:
        data = response.json()
    except requests.exceptions.JSONDecodeError as e:
        print("Failed to decode JSON:", e)
        print("Response:", response.text)
        return None

    # Convert JSON to DataFrame
    df = pd.DataFrame(data)
    
    # Convert timestamps to datetime
    df['datetime'] = pd.to_datetime(df['timestamp'], unit='s')
    df.drop('timestamp', axis=1, inplace=True)

<<<<<<< HEAD
    df.to_csv(f"./data_csv/eodhd_{stock_symbol}.csv")
=======
    df.to_csv(f"../data_csv/eodhd_{stock_symbol}.csv")
>>>>>>> sz65

    return df

# Example usage with your own API key
stock_symbol = 'AAPL.US'
start_date = '2023-10-23'
end_date = '2023-10-30'
interval = '1h'  # or '5m', '1h'
api_key = '123456'  # Replace with your API key

data_frame = fetch_intraday_data(stock_symbol, start_date, end_date, interval, api_key)
print(data_frame)

