import time
import datetime
import pandas as pd

def download_stock_data(ticker, date1, date2):
    # Convert date1 and date2 from string to datetime objects
    start_date = datetime.datetime.strptime(date1, '%Y-%m-%d')
    end_date = datetime.datetime.strptime(date2, '%Y-%m-%d')

    period1 = int(time.mktime(start_date.timetuple()))
    period2 = int(time.mktime(end_date.timetuple()))
    interval = '1d'  # 1wk, 1m

    url = f'https://query1.finance.yahoo.com/v7/finance/download/{ticker}?period1={period1}&period2={period2}&interval={interval}&events=history&includeAdjustedClose=true'

    df = pd.read_csv(url)
<<<<<<< HEAD
    df.to_csv(f"./data_csv/yahoofin_{ticker}.csv")
=======
    df.to_csv(f"../data_csv/yahoofin_{ticker}.csv")
>>>>>>> sz65
    print(f"Downloaded data for {ticker}")

# Example usage
download_stock_data('NVDA', '2023-01-02', '2023-10-30')
download_stock_data('TSLA', '2023-01-02', '2023-10-30')
