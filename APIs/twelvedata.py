import requests
import pandas as pd
from datetime import datetime, timedelta

# Function to fetch data
def fetch_data(symbol, start_date, end_date, interval, apikey):
    base_url = "https://api.twelvedata.com/time_series"
    data = []
    params = {
            'symbol': symbol,
            'interval': interval,
            'start_date': start_date.strftime('%Y-%m-%d'),
            'end_date': end_date.strftime('%Y-%m-%d'),
            'apikey': apikey
        }
    while start_date < end_date:
        
        response = requests.get(base_url, params=params)
        if response.status_code == 200:
            result = response.json()
            if 'values' in result:
                data.extend(result['values'])
                last_date = datetime.strptime(data[-1]['datetime'], '%Y-%m-%d %H:%M:%S')
                start_date = last_date + timedelta(minutes=1)
            else:
                break
        else:
            print(f"Error: {response.status_code}")
            break
        
    df = pd.DataFrame(data)
<<<<<<< HEAD
    df.to_csv(f"./data_csv/12data_{symbol}.csv")
=======
    df.to_csv(f"../data_csv/12data_{symbol}.csv")
>>>>>>> sz65
    return data

# Your API key
apikey = '123456'

# Example Fetch data
data = fetch_data('AAPL', datetime(2023, 10, 23), datetime(2023, 10, 30), '1min', apikey)

# Convert to DataFrame
df = pd.DataFrame(data)
print(df)