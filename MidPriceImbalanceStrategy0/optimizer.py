import pandas as pd
import numpy as np
import subprocess
import itertools
import random
import json
import time
import os
import sys
import matplotlib.pyplot as plt

class StrategyOptimiser():
    def __init__(self):
        self.df = None # df to store the symbols dataset
        self.backtest_id = None # backtest id used to segragte backtests 
        self.train_stocks = [] # list of stocks to run the backtest on 
        self.dates = None # list of dates either for generating the train data , for evaluating or for testing
        self.makefile = 'Makefile' # name of the makefile
        self.instance_name = None
        self.output_directory = None 
        self.new_parameters = None
        self.train_cutoff_date = None
        self.start_date = None
        self.combinations = None
        self.load_available_data()
        self.init_makefile()

    def init_makefile(self):
        with open(self.makefile, 'r') as file:
            lines = file.readlines()

        with open(self.makefile, 'w') as file:
            for line in lines:
                if line.startswith('INSTANCE_NAME='):
                    line = f'INSTANCE_NAME={self.start_date}\n'  # Ensure newline is included
                elif line.startswith('STRATEGY_NAME='):
                    line = f'STRATEGY_NAME={self.start_date}\n' 
                elif line.startswith('LIBRARY='):
                    line = f'LIBRARY={self.start_date}.so\n' 
                elif line.startswith('SOURCES='):
                    line = f'SOURCES={self.start_date}.cpp\n'
                elif line.startswith('HEADERS='):
                    line = f'HEADERS={self.start_date}.h\n' 
                elif line.startswith('BACKTESTID='):
                    line = f'BACKTESTID={self.start_date}\n' 
                file.write(line)


    def load_available_data(self,filepath = "../Resources/Data/Equity_and_Commodity/symbols_dates_df.csv",opt_info = "optimisation_info.txt" ):
        
        # load the stock and date data
        self.df = pd.read_csv(filepath)
        print(self.df.head())
        # removes repeated names
        stocks = self.df['stock'].unique()
        # open athe file withb optimisation info
        with open(opt_info, 'r') as file:
            lines = file.readlines()
        for line in lines:
            if line.startswith('INSTANCE_NAME'):
                self.instance_name = line.split('=')[1]
            if line.startswith('BACKTEST_ID'):
                self.backtest_id = line.split('=')[1]
            if line.startswith('TRAIN_STOCKS'):
                self.train_stocks = line.split('=')[1].split(',')
                self.train_stocks = [stock.strip() for stock in self.train_stocks]
                print(self.train_stocks)
            if line.startswith('TRAIN_CUTOFF_DATE'):
                self.train_cutoff_date = line.split('=')[1]
                print(self.train_cutoff_date)
            if line.startswith('ACCOUNTSIZE'):
                self.account_size = line.split('=')[1]
                print(self.account_size)
        self.output_directory = '/groupstorage/group01/backtest_data/'+self.instance_name+'/'+self.backtest_id+'/'
        if self.train_stocks not in stocks:
            raise ValueError("Stock does not exist in the dataset")
        
    def generate_combinations(self,combinations_file = 'parameter_combinations.txt'):
        #open the parameters file
        with open(combinations_file, 'r') as file:
            lines = file.readlines()

        # Extract parameter names and values
        param_names = [line.strip().split('=')[0] for line in lines]
        choices = [line.strip().split('=')[1].split(',') for line in lines]

        # Generate combinations
        combinations = list(itertools.product(*choices))

        # Format combinations as strings
        formatted_combinations = []
        for combo in combinations:
            formatted_combo = '|'.join(f"{name}={value}" for name, value in zip(param_names, combo))
            formatted_combinations.append(formatted_combo)
        return formatted_combinations

    def get_stocks(self):
        aggregated_volumes = self.df.groupby('stock')['Volume'].sum().reset_index()
        sorted_stocks = aggregated_volumes.sort_values(by='Volume', ascending=False)
        high_volume_stocks = sorted_stocks['stock'].head(5).tolist()
        low_volume_stocks = sorted_stocks['stock'].tail(len(sorted_stocks) - 5).tolist()
        return high_volume_stocks, low_volume_stocks
        
    def get_train_stock(self):
        high_stocks,low_stocks = self.get_stocks()
        self.train_stocks.append(random.choice(high_stocks))
        self.train_stocks.append(random.shuffle(low_stocks))
    
    def load_json(self, file_name = None):
        if file_name is None:
            file_name = self.output_directory+'backtest_data.json'
        try:
            with open(file_name, 'r') as file:
                return json.load(file)
        except Exception as e:
            print(f"Error reading {file_name}: {e}")
            return None

    def write_json(self, data):
        path = self.output_directory+'best_params.json'
        json_data = self.load_json(path)
        if json_data is None:
            json_data = []
        json_data.insert(0, data)
        with open(path, 'w') as file:
            json.dump(json_data, file, indent=4)
        

    def convert_to_dict(self):
        params = self.new_parameters.split('|')
        match = {}
        for param in params:
            key,value = param.split('=')
            match[key] = value
        return match

    def find_matching_entries(self, symbols):
        json_data = self.load_json(self.output_directory)
        if json_data is None:
            return []

        for entry in json_data:
            if (entry.get('symbols') == 'XNAS.ITCH-'+symbols and 
                entry.get('backtest_id') == self.backtest_id and 
                entry.get('start_date') == self.start_date and
                entry.get('parameters') == self.convert_to_dict()):
                    return entry.get('unique_id'), entry.get('stats')
            else:
                raise ValueError('missing json corresponding to test')

    def get_dates(self,stock):
        self.dates = self.df[self.df['stock']==stock]['Date'].unique()
        self.dates =  self.dates[self.dates<=self.train_cutoff_date]
        print('length of train dates',len(self.dates))
    
    def get_validation_dates(self,stock):
        self.dates = self.df[self.df['stock']==stock]['Date'].unique()
        self.dates =  self.dates[self.dates>self.train_cutoff_date]
        print('length of test dates',len(self.dates))
        
    def gen_train_data(self):
        if len(self.train_stocks) == 0:
            self.get_train_stock()
        self.combinations = self.generate_combinations()
        for stock in self.train_stocks:
            for comb in self.combinations:
                self.new_parameters=comb
                self.get_dates(stock)
                print('length of train dates',len(self.dates))
                for date in self.dates:
                    self.start_date = date
                    print('modfied_makefile')
                    self.modify_makefile(stock)
                    print('run_make_run_backtest')
                    self.run_make_target('clean')
                    self.run_make_target('run_backtest')
                    print('backtest_complete')
    
    def gen_train_results(self):
        self.combinations = self.generate_combinations()
        for stock in self.train_stocks:
            for comb in self.combinations:
                self.new_parameters=comb
                self.get_dates(stock)
                for date in self.dates:
                    self.start_date = date
                    print('modfied_makefile')
                    self.modify_makefile(stock)
                    print('run_make_get_results')
                    self.run_make_target('get_results')
                    print('result_gen_complete')
    
    def modify_makefile(self, stock):
        with open(self.makefile, 'r') as file:
            lines = file.readlines()

        with open(self.makefile, 'w') as file:
            for line in lines:
                if line.startswith('START_DATE=') and self.start_date is not None:
                    line = f'START_DATE={self.start_date}\n'  # Ensure newline is included
                elif line.startswith('END_DATE=') and self.train_cutoff_date is not None:
                    line = f'END_DATE={self.start_date}\n'  # Ensure newline is included
                elif line.startswith('SYMBOLS=XNAS.ITCH-'):
                    line = f'SYMBOLS=XNAS.ITCH-{stock}\n'  # Ensure newline is included
                elif line.startswith('PARAMETERS='):
                    line = f'PARAMETERS={self.new_parameters}\n'
                elif line.startswith('ACCOUNTSIZE='):
                    line = f'ACCOUNTSIZE={self.account_size}\n'
                elif line.startswith('BACKTESTID='):
                    line = f'BACKTESTID={self.backtest_id}\n'
                file.write(line)

    def run_make_target(self, target='run_backtest'):
        subprocess.run(['make', target])
        
    def find_best_parameters(self):
        best_params = {}
        best_sharpe = {}
        best_sortino = {}
        best_MDD = {}
        best_net_PNL = {}
        for stock in self.train_stocks:
            self.get_dates(stock)
            for comb in self.combinations:
                self.new_parameters=comb
                u_id_list = []
                sharpe_list = []
                sortino_list = []
                MDD_list = []
                net_PNL_list = []
                for date in self.dates:
                    self.start_date = date
                    u_id,stats = self.find_matching_entries(stock)
                    u_id_list.append(u_id)
                    sharpe_list.append(float(stats['pnl']['sharpe_ratio']))
                    sortino_list.append(float(stats['pnl']['sortino_ratio']))
                    MDD_list.append(float(stats['pnl']['max_drawdown']))
                    net_PNL_list.append(float(stats['pnl']['net_pnl']))
                sharpe = np.mean(sharpe_list)
                sortino = np.mean(sortino_list)
                MDD = np.max(MDD_list)
                net_PNL = np.sum(net_PNL_list)
                if sharpe>best_sharpe:
                    best_sharpe = sharpe
                    sharpe_id = u_id_list
                if sortino>best_sortino:
                    best_sortino = sortino
                    sortino_id = u_id_list
                if MDD>best_MDD:
                    best_MDD = MDD
                    MDD_id = u_id_list
                if net_PNL>best_net_PNL:
                    best_net_PNL = net_PNL
                    net_pnl_id = u_id_list
            best_params[stock]['sharpe']['value'] = best_sharpe
            best_params[stock]['sharpe']['id_list'] = sharpe_id
            best_params[stock]['sortino']['value'] = best_sortino
            best_params[stock]['sortino']['id_list'] = sortino_id
            best_params[stock]['MDD']['value'] = best_MDD
            best_params[stock]['MDD']['id_list'] = MDD_id
            best_params[stock]['net_PNL']['value'] = best_net_PNL
            best_params[stock]['net_PNL']['id_list'] = net_pnl_id
        self.write_json(best_params)
        print(best_params)
        
    def get_best_parameters(self,criteria = 'sharpe'):
        filename = self.output_directory+'best_params.json'
        with filename as file:
            data = json.load(file)
        for stock in self.train_stocks:
            u_id_list = data[stock][criteria]['id_list']
            json_data = self.load_json(self.output_directory)
            for entry in json_data:
                if entry.get('unique_id') == u_id_list[0]:
                    params = entry.get('parameters')
                    formatted_combo = '|'.join(f"{params.keys()}={params.values()}")
                    self.new_parameters = formatted_combo
                pnls = {}
                for u_id in u_id_list:
                    if entry.get('unique_id') == u_id:
                        pnl = entry.get('stats')['pnl']['net_pnl']
                        date = entry.get('start_date')
                        pnls[date] = pnl
                plt.plot(pnls.keys(),pnls.values())
                plt.xlabel('Date')
                plt.ylabel('Net PNL')
                plt.title('Net PNL vs Date')
                plt.grid(True)
                file_path = os.path.join(self.output_directory, 'train_results_plot.png')
                plt.savefig(file_path)
            
    def test_set_gen(self):
        for stock in self.test_stocks:
            self.get_best_parameters()
            self.get_validation_dates(stock)
            print('length of test dates',len(self.dates))
            for date in self.dates:
                self.start_date = date
                self.modify_makefile(stock)
                self.run_make_target('clean')
                self.run_make_target('run_backtest')
            print('Validation dataset gen complete')
    
    def test_result_gen(self):
        for stock in self.test_stocks:
            self.get_best_parameters()
            self.get_validation_dates(stock)
            print('length of test dates',len(self.dates))
            for date in self.dates:
                self.start_date = date
                self.modify_makefile(stock)
                self.run_make_target('get_results')
            print('Validation result gen complete')
    # def test_result_plot(self):
    

if __name__ == "__main__":
    task = sys.argv[1]
    optim = StrategyOptimiser()
    print('loading the data')
    optim.load_available_data()
    if task == "0":
        print('generating the combinations')
        optim.gen_train_data()
        optim.gen_train_results()
    elif task == "1":
        print('finding the best parameters')
        optim.find_best_parameters()
    elif task == "2":
        print('testing the best parameters')
        optim.test_set()
    else:
        raise ValueError("invalid value for task : task has to take 0 (gen data) , 1 (find best parameters) , 2 (test on testing period)")