// Refer to the header files for the explanation of the functions in detail
// In VSCode, hovering your mouse above the function renders the explanation of from the .h file as a pop up
#ifdef _WIN32
    #include "stdafx.h"
#endif

#include "MidPriceImbalanceStrategy0.h"

using namespace RCM::StrategyStudio;
using namespace RCM::StrategyStudio::MarketModels;
using namespace RCM::StrategyStudio::Utilities;

using namespace std;

// Constructor to initialize member variables of the class to their initial values.
MidPriceImbalanceStrategy0::MidPriceImbalanceStrategy0(StrategyID strategyID,
                    const string& strategyName,
                    const string& groupName):
    Strategy(strategyID, strategyName, groupName),
    strategy_active(true),
    debug_(false),
    absolute_stop(0.2),
    max_inventory(100),
    inventory_liquidation_decrement(10),
    inventory_liquidation_interval(10),
    levels_(10),
    smoothing_interval(10),
    Z_threshold(2)
{
    // Constructor implementation goes here, if needed
}
// Destructor for class
MidPriceImbalanceStrategy0::~MidPriceImbalanceStrategy0() {
}

void MidPriceImbalanceStrategy0::DefineStrategyParams() {
}

// By default, SS will register to trades/quotes/depth data for the instruments you have requested via command_line.
void MidPriceImbalanceStrategy0::RegisterForStrategyEvents(StrategyEventRegister* eventRegister, DateType currDate)
{    
    ReadParameters();
    SetInventoryParams();
    eventRegister->RegisterForRecurringScheduledEvents("liquidation_time",USEquityCloseUTCTime(currDate)-boost::posix_time::minutes((inventory_liquidation_decrement+1)*inventory_liquidation_interval),USEquityCloseUTCTime(currDate)-boost::posix_time::minutes(inventory_liquidation_interval),boost::posix_time::minutes(inventory_liquidation_interval));
    eventRegister->RegisterForRecurringScheduledEvents("print_account_stats",USEquityOpenUTCTime(currDate),USEquityCloseUTCTime(currDate),boost::posix_time::minutes(15));
}

void MidPriceImbalanceStrategy0::OnTrade(const TradeDataEventMsg& msg) {
    const Instrument* instrument = &msg.instrument();
    if (debug_)
        RecordAccountStats(instrument);
}

void MidPriceImbalanceStrategy0::OnScheduledEvent(const ScheduledEventMsg& msg) {
    if (msg.scheduled_event_name() == "liquidation_time"){
        InventoryLiquidation();
    }
    if (msg.scheduled_event_name() == "print_account_stats"){
        for (InstrumentSetConstIter iter = instrument_begin(); iter != instrument_end(); ++iter) {
            const Instrument* instrument = iter->second;
            RecordAccountStats(instrument);
        }
    }
}

void MidPriceImbalanceStrategy0::OnDepth(const MarketDepthEventMsg& msg){
    const Instrument* instrument = &msg.instrument();
    if (strategy_active)
        StrategyLogic(instrument);
}

void MidPriceImbalanceStrategy0::OnOrderUpdate(const OrderUpdateEventMsg& msg) {
    if (debug_){
    cout << "name = " << msg.name() << endl;
    cout << "order id = " << msg.order_id() << endl;
    cout << "fill occurred = " << msg.fill_occurred() << endl;
    cout << "update type = " << msg.update_type() << endl;
    cout << "time " << msg.update_time() << endl;
    }
}

void MidPriceImbalanceStrategy0::InventoryLiquidation(){
    max_inventory -= inventory_liquidation_decrement;
    if (max_inventory < 0)
        max_inventory = 0;

    if (debug_){
        cout << "liquidating inventory by " << inventory_liquidation_decrement << endl;
        cout << "max inventory is now " << max_inventory << endl;
    }
}

void MidPriceImbalanceStrategy0::SetInventoryParams(){
    inventory_liquidation_decrement = ceil(max_inventory/inventory_liquidation_decrement)+1;
    if (debug_)
        cout << "liquidation decrement :- " << inventory_liquidation_decrement << endl;
}

void MidPriceImbalanceStrategy0::StrategyLogic(const Instrument* instrument){
    
    AbsoluteStopCheck(instrument);
    int cond = CheckImbalance(instrument, levels_);
    if (cond == 1){
        if (debug_)
        cout << "entered short " << endl;
        SendTakeOrder(instrument,true);
    } else if (cond == -1) {
        if (debug_)
        cout << "entered long " << endl;
        SendTakeOrder(instrument,false);
    } else {
        if (debug_)
            cout << "no trade" << endl;
    }
}

double MidPriceImbalanceStrategy0::CalculateMidPrice(const Instrument* instrument, int levels) {
    const IAggrOrderBook* orderbook = &instrument->aggregate_order_book();
    double mid_price = 0;
    double t_ask_price = 0;
    double t_bid_price = 0;
    int t_ask_size = 0;
    int t_bid_size = 0;
    int existing_bid_levels = orderbook->NumBidLevels();
    int existing_ask_levels = orderbook->NumAskLevels();

    if ((existing_ask_levels == 0) || (existing_bid_levels == 0)){
        return 0;
    }
    levels = min(existing_bid_levels,levels);
    levels = min(existing_ask_levels,levels);
    for (int i = 0; i < levels; i++) {
        const IPriceLevelBase* AskPrice = orderbook->AskPriceLevelAtLevel(i);
        const IPriceLevelBase* BidPrice = orderbook->BidPriceLevelAtLevel(i);
        double ask = AskPrice->price();
        double bid = BidPrice->price();
        int ask_q = AskPrice->size();
        int bid_q = BidPrice->size();
        t_ask_price += ask * ask_q;
        t_bid_price += bid * bid_q;
        t_ask_size += ask_q;
        t_bid_size += bid_q;
    }

    mid_price = (t_ask_price+t_bid_price)/(t_ask_size+t_bid_size);
    if (debug_){
        cout << "price for " << levels << " is " << mid_price << endl;
    }
    return mid_price;
}

double MidPriceImbalanceStrategy0::CalculateCurrentSpread(const Instrument* instrument){
    double top_bid = instrument->top_quote().bid();
    double top_ask = instrument->top_quote().ask();
    double spread = abs(top_ask - top_bid);
    return spread;
}

int MidPriceImbalanceStrategy0::CheckImbalance(const Instrument* instrument, int levels){
    double simple = CalculateMidPrice(instrument,1);
    double weighted = CalculateMidPrice(instrument,levels);

    PriceDiffIterator price_diff_iter = price_diff_map.find(instrument);
    if (price_diff_iter == price_diff_map.end()) {
        price_diff_iter = price_diff_map.insert(make_pair(instrument, PriceDiff(smoothing_interval))).first;
    }
    double score = price_diff_iter->second.Update(weighted-simple);

    if (debug_)
        cout << "simple price is :- " << simple << " and weighted price is :- " << weighted << endl;

    if (simple == 0)
        return 0;
    if (score > Z_threshold) {
        return 1;
    } else if (score < Z_threshold) {
        return -1;
    } else {
        return 0;
    }
}

int MidPriceImbalanceStrategy0::MaxPossibleLots(const Instrument* instrument,int side){
    int current_position = portfolio().position(instrument);
    int max_order_size = 0;
    if (side == 1) {
        max_order_size = max_inventory - current_position;
    } else {
        max_order_size = max_inventory + current_position;
    }
    if (debug_){
        cout << "max position:- " << max_inventory << endl;
        cout << "position :- " << current_position << endl;
        cout << "max order size :- " << max_order_size << endl;
    }
    return max_order_size;
}

void MidPriceImbalanceStrategy0::RecordAccountStats(const Instrument* instrument){
    cout << "Total profit " << portfolio().total_pnl(instrument) << endl;
    cout << "Unrealised profit :- " << portfolio().unrealized_pnl(instrument) << endl;
    cout << "Realised Profit :- " << portfolio().realized_pnl(instrument) << endl;
    cout << "Max position :- " << max_inventory << endl;
    cout << "Current position :- " << portfolio().position(instrument) << endl;
}

void MidPriceImbalanceStrategy0::AdjustPortfolio() {
}

void MidPriceImbalanceStrategy0::SendTakeOrder(const Instrument* instrument, bool hit_bid) {

    double price;
    int qty;
    int max_lot_size = MaxPossibleLots(instrument,(hit_bid) ? -1 : 1);
    string action;

    if (hit_bid) { // sell
        price = instrument->top_quote().bid();
        qty = instrument->top_quote().bid_size();
        qty = min(qty,max_lot_size);
        action = "hit the bid ";
    } else { // buy
        price = instrument->top_quote().ask();
        qty = instrument->top_quote().ask_size();
        qty = min(qty,max_lot_size);
        action = "Take the ask ";
    }
    // Create an order object with the specified parameters
    OrderParams params(
                    *instrument,     // Instrument to trade
                    qty, // Absolute value of trade size
                    price,           // Price at which to trade
                    MARKET_CENTER_ID_NASDAQ, // Market center ID
                    (hit_bid) ? ORDER_SIDE_SELL : ORDER_SIDE_BUY, // Order side (buy or sell)
                    ORDER_TIF_IOC,   // Time in force (how long the order is valid for) - Immediate or Cancel in this case
                    ORDER_TYPE_LIMIT // Order type (limit or market)
                    );

    if (debug_){
    // Print a message indicating that a new order is being sent
    cout << "About to send new order for size "
            << qty
            << " at $"
            << price
            << " to "
            << action
            << instrument->symbol()
            << endl;
    }
    
    TradeActionResult tra = trade_actions()->SendNewOrder(params);
    if (debug_){
        // Check if the order was sent successfully and print a message indicating the result
        if (tra == TRADE_ACTION_RESULT_SUCCESSFUL) {
                std::cout << "Sending new trade order successful!" << std::endl;
        } else {
                std::cout << "Error sending new trade order..." << tra << std::endl;
        }
    }
}

void MidPriceImbalanceStrategy0::OnMarketState(const MarketStateEventMsg& msg){
}

void MidPriceImbalanceStrategy0::ReadParameters(){
    string line;
    ifstream myfile("/home/vagrant/ss/sdk/RCM/StrategyStudio/examples/strategies/group_01_project/MidPriceImbalanceStrategy0/parameters.txt"); 
    if (myfile.is_open()) {
        while ( getline (myfile, line) ) {
            string key;
            string value;
            istringstream iss(line);
            getline(iss, key, '='); // Extract string up to the '=' character
            getline(iss, value);
            SetParamFromKey(key,value);
        }
        myfile.close();
    } else {
        cout << "Unable to open file to read parameters from file" << endl;
    }
}

void MidPriceImbalanceStrategy0::SetParamFromKey(string key, string value){

    if (key == "debug_"){
        debug_ = (value == "true" || value == "1");
    }
    if (key == "inventory_liquidation_decrement"){
        inventory_liquidation_decrement = stoi(value);
    }
    if (key == "inventory_liquidation_interval"){
        inventory_liquidation_interval = stoi(value);
    }
    if (key=="levels_"){
        levels_ = stoi(value);
    }
    if (key=="max_inventory"){
        max_inventory = stoi(value);
    }
    if (key=="smoothing_interval"){
            smoothing_interval = stoi(value);
    }
    if (key=="Z_threshold"){
        Z_threshold = stod(value);
    }
    else if (key == "absolute_stop") {
        absolute_stop = stod(value);
    }
}

void MidPriceImbalanceStrategy0::LiquidationOrder(const Instrument* instrument){
    int inventory = portfolio().position(instrument);
    double price = (inventory > 0) ? instrument->top_quote().bid() - 10 : instrument->top_quote().ask() + 10;
    OrderSide action = (inventory > 0) ? ORDER_SIDE_SELL : ORDER_SIDE_BUY;

    OrderParams params(*instrument,
                       abs(inventory),
                       price,
                       MARKET_CENTER_ID_NASDAQ,
                       action,
                       ORDER_TIF_DAY,
                       ORDER_TYPE_LIMIT);

    if (debug_){
    // Print a message indicating that a new order is being sent
    std::cout << "Closing out all positions with order for size "
            << inventory
            << " at $"
            << price
            << " for symbol "
            << instrument->symbol()
            << std::endl;
    }
    trade_actions()->SendNewOrder(params);
}

void MidPriceImbalanceStrategy0::AbsoluteStopCheck(const Instrument* instrument){

    double pnl = portfolio().total_pnl(instrument)/portfolio().initial_cash();
    if (pnl < -absolute_stop){
        LiquidationOrder(instrument);
        trade_actions()->SendCancelAll();
        strategy_active = false;
        cout << "absolute stop hit" << endl;
    }
}

void MidPriceImbalanceStrategy0::OnResetStrategyState() {
}

void MidPriceImbalanceStrategy0::OnParamChanged(StrategyParam& param) {
}
