// This is a preprocessor directive that ensures that the header file is included only once in a given compilation unit, to avoid multiple definitions.
#pragma once

// These are include guards that prevent redefinition of class names, macro constants, and typedef names. 
// Include guards help avoiding name conflicts in large software projects.
#ifndef _STRATEGY_STUDIO_LIB_STRATEGY0_H_
#define _STRATEGY_STUDIO_LIB_STRATEGY0_H_

// This is a conditional preprocessor directive that defines a macro _STRATEGY_EXPORTS as __declspec(dllexport) on Windows platform, and empty on other platforms.
// This macro is used to export the HelloworldStrategy class to the dynamic link library (DLL) that is loaded by the trading engine.
#ifdef _WIN32
    #define _STRATEGY_EXPORTS __declspec(dllexport)
#else
    #ifndef _STRATEGY_EXPORTS
    #define _STRATEGY_EXPORTS
    #endif
#endif

/**
 * Below are header files that are used by the HelloworldStrategy class. We just tell the compiler to look for these files.
 * You will not have Strategy.h & Instrument.h in your directory. These are part of the SDK.
 * Strategy.h is the main header file for the strategy development kit and provides access to the core functionality of the trading engine.
 * Instrument.h is a header file for instrument specific data. 
 * The remaining headers provide various utility functions.
**/
#include <Strategy.h>
#include <Analytics/ScalarRollingWindow.h>
#include <Analytics/InhomogeneousOperators.h>
#include <Analytics/IncrementalEstimation.h>
#include <MarketModels/Instrument.h>
#include <MarketModels/IAggrOrderBook.h>
#include <IPositionRecord.h>
#include <Order.h>
#include <BarDataTypes.h>
#include <string>
#include <unordered_map>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm> 
#include <unistd.h>
#include <limits.h>
#include <cmath>

// Import namespace RCM::StrategyStudio to avoid explicit namespace qualification when using names from this namespace
using namespace RCM::StrategyStudio;
using namespace std;

class PriceDiff {
public:
    PriceDiff(int smoothing_interval = 10) : window(smoothing_interval) {}

    void Reset()
    {
        window.clear();
    }

    double Update(double val)
    {
        window.push_back(val);
        return window.ZScore();
    }

    bool FullyInitialized() { return (window.full()); }
    
    Analytics::ScalarRollingWindow<double> window;
};
// Class declaration
class MidPriceImbalanceStrategy0 : public Strategy {

public:

    typedef boost::unordered_map<const Instrument*, PriceDiff> PriceDiffMap; 
    // creates an unordered map where keys are instrument names and values are corresponding classes 
    // all these pairs are set to a map called MomentumMap
    typedef PriceDiffMap::iterator PriceDiffIterator; 
    // Constructor & Destructor functions for this class

    MidPriceImbalanceStrategy0(StrategyID strategyID,
        const std::string& strategyName,
        const std::string& groupName);
    ~MidPriceImbalanceStrategy0();

public:

    // Below are event handling functions the user can override on the cpp file to create their own strategies.
    // Polymorphic behavior is achieved in C++ by using virtual functions, which allows the same function to behave differently depending on the type of object it is called on.

    virtual void OnDepth(const MarketDepthEventMsg& msg);

    /**
    * Called whenever a trade event occurs for an instrument that the strategy is subscribed to.
    * A trade event refers to a specific occurrence related to a trade of a financial instrument, such as a stock or a commodity like the execution of a buy or sell order
    * The msg parameter is an object of type TradeDataEventMsg that contains information about the trade event that occurred
    */
    virtual void OnTrade(const TradeDataEventMsg& msg);

    /**
     * Called when a scheduled event occurs during the backtesting process.
     * Examples of actions include making trading decisions, adjusting parameters or indicators, updating strategy state, or triggering specific actions at predefined intervals and time-dependent trading strategies.
    */
    virtual void OnScheduledEvent(const ScheduledEventMsg& msg);

    // Called whenever there is an update to one of the orders placed by the strategy
    void OnOrderUpdate(const OrderUpdateEventMsg& msg);
 
    void OnResetStrategyState();

    void OnParamChanged(StrategyParam& param);

private:

    virtual void RegisterForStrategyEvents(StrategyEventRegister* eventRegister, DateType currDate);

    virtual void DefineStrategyParams();

    void AdjustPortfolio();

    void SendTakeOrder(const Instrument* instrument, bool hit_bid);

    double CalculateMidPrice(const Instrument* instrument, int levels);

    double CalculateCurrentSpread(const Instrument* instrument);

    void CalculateImbalance(const ImbalanceEventMsg& msg);

    int CheckImbalance(const Instrument* instrument, int levels);

    int MaxPossibleLots(const Instrument* instrument,int side);

    void RecordAccountStats(const Instrument* instrument);

    void StrategyLogic(const Instrument* instrument);

    void OnMarketState(const MarketStateEventMsg& msg);

    void InventoryLiquidation();

    void SetInventoryParams();

    void ReadParameters();

    void SetParamFromKey(string key, string value);

    void AbsoluteStopCheck(const Instrument* instrument);

    void LiquidationOrder(const Instrument* instrument);

private:
    // Used to store the state and data of the strategy.
    int smoothing_interval; // smoothing interval for price diff
    boost::unordered_map<const Instrument*, PriceDiff> price_diff_map; // creates an unordered map where keys are instrument names and values are corresponding classes
    int levels_; // levels to consider for weighted price 
    bool debug_; // a boolean variable that can be used to enable or disable debug mode.
    int max_inventory; // max position value we can accumulate in any direction.
    int interval_ms; // bar interval in minutes
    int inventory_liquidation_decrement; // decrement in which we will liquidate inventory
    int inventory_liquidation_interval; // interval in minutes in which we will liquidate inventory
    double Z_threshold; // threshold for z-score
    double absolute_stop; // absolute stop loss
    bool strategy_active;
};

// extern "C" is used to tell the compiler that these functions have C-style linkage instead of C++-style linkage, which means the function names will not be mangled.
// Except the strategy name, you don't need to change anything in this section.
extern "C" {

    _STRATEGY_EXPORTS const char* GetType() {
        return "MidPriceImbalanceStrategy0";
    }

    _STRATEGY_EXPORTS IStrategy* CreateStrategy(const char* strategyType,
                                   unsigned strategyID,
                                   const char* strategyName,
                                   const char* groupName) {
        if (strcmp(strategyType, GetType()) == 0) {
            return *(new MidPriceImbalanceStrategy0(strategyID, strategyName, groupName));
        } else {
            return NULL;
        }
    }

    _STRATEGY_EXPORTS const char* GetAuthor() {
        return "dlariviere";
    }

    _STRATEGY_EXPORTS const char* GetAuthorGroup() {
        return "UIUC";
    }

    _STRATEGY_EXPORTS const char* GetReleaseVersion() {
        return Strategy::release_version();
    }
}

// The #endif statement marks the end of the include guard to prevent the header file from being included multiple times.
#endif
