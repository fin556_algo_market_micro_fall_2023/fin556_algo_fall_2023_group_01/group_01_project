# LSTM Model Report

## Introduction
In the ever-evolving landscape of financial markets, predicting stock movements remains a cornerstone for strategic trading and investment decision-making. With the advent of sophisticated machine learning techniques, this endeavor has shifted towards more accurate and dynamic models capable of handling the complexity and volatility inherent in financial data. Among these, Long Short-Term Memory (LSTM) networks have emerged as a pivotal tool, especially when dealing with time-series data such as stock prices.

### Understanding LSTM in the Context of Financial Time-Series Data

LSTM, a variant of Recurrent Neural Networks (RNNs), is uniquely equipped to process and predict time-series data. Its architecture allows it to remember long-term dependencies, a crucial feature for understanding the sequential and often non-linear patterns in stock price movements. Unlike traditional models, LSTM can retain information over long periods, making it highly adept at capturing the intricate temporal dynamics of financial markets.

In the realm of stock market prediction, where data points are not independent but inherently sequential – each moment's price influenced by its predecessors – LSTM's ability to 'remember' and 'forget' information selectively becomes invaluable. This capability stems from its internal structure composed of gates: the input gate, output gate, and forget gate. These gates collaboratively regulate the flow of information, deciding what to retain and what to discard, thereby overcoming the vanishing gradient problem common in standard RNNs.

### The Project: Leveraging LSTM for Stock Movement Prediction

This project aims to harness the power of LSTM in predicting stock movements using tick-by-tick data over a span of three days. The fine granularity of tick-by-tick data provides a detailed view of stock activity, capturing every price change and trade execution, thereby offering a rich dataset for the LSTM model to learn from. The project involves training an LSTM model with relevant features – such as price changes, volume, and possibly order book dynamics – to predict future stock movements.

By focusing on short-term intervals, the project seeks to capture the micro-level fluctuations in stock prices, offering insights that are often masked in daily or longer-term data. This approach is particularly relevant in today's fast-paced trading environment, where high-frequency trading and algorithmic strategies dominate, and where even minor price movements can hold significant implications.

### Integration and Application in Real-world Trading

Post-training, the LSTM model will be exported into a C++ file, enabling integration with Strategy Studio for backtesting. This step is crucial, as it allows for the evaluation of the model's predictive power and robustness in a simulated trading environment, closely mimicking real-world conditions. Backtesting is an essential phase, providing insights into the model's effectiveness and helping fine-tune it before any actual deployment in live trading scenarios.

---

## Setup and Dependencies


In this project, we utilize a range of Python libraries, each serving a specific purpose in the process of data handling, preprocessing, and model building. Below is a comprehensive list of these libraries and their respective roles:

### Libraries and Their Roles

- **gzip**: This module provides a simple interface to compress and decompress files using the GZIP file format. It is likely used here for reading or writing data in a compressed format, potentially for efficient data storage and transfer.

- **pandas (as pd)**: Pandas is a fast, powerful, flexible, and easy-to-use open-source data analysis and manipulation tool. It is fundamental for data handling tasks such as data loading, cleaning, transformation, and exploration.

- **datetime (time)**: Imported from the datetime module, the time class represents time, independent of any particular day. This might be used for handling time-related data within datasets or for timing operations in the code.

- **matplotlib.pyplot (as plt)**: This is a widely used data visualization library in Python. It's instrumental for plotting graphs, charts, and other visual data representations.

- **itertools**: A standard library module that provides a collection of tools for handling iterators. It could be used for efficient looping and data manipulation tasks.

- **torch**: This is the main PyTorch library, a popular open-source machine learning library. It provides a wide range of functionalities for array operations, neural network construction, and training models.

- **torch.nn (as nn)**: A sub-module of PyTorch, nn provides a way to efficiently build and train neural networks with predefined layers, loss functions, etc.

- **numpy (as np)**: Numpy is fundamental for numerical operations in Python. It offers powerful n-dimensional array objects and a variety of tools for working with these arrays.

- **sklearn.preprocessing (MinMaxScaler)**: This import suggests that the project involves data normalization or scaling. MinMaxScaler is typically used to scale features to a given range, often a necessity for neural network inputs.

- **time (as time_module)**: This import might be used for tracking the duration of certain operations in the code, such as model training or data processing.

---



## Exploratory Data Analysis

### Data Loading Functions

- `load_dataframe_from_csv` and `load_dataframe_from_gz`: 
  - **Purpose**: These functions are designed to load data into pandas DataFrames from CSV files and gzip-compressed files, respectively. 
  - **Benefit**: This approach allows handling both standard and compressed data formats efficiently.

### Data Filtering and Processing

- `filter_stock_data`: 
  - **Functionality**: Filters the DataFrame to include data only for specified stock tickers, enabling targeted analysis of particular stocks.

- `calculate_price_and_indicators`: 
  - **Description**: A comprehensive function that adds various technical indicators to the DataFrame.
  - **Indicators Included**: 
    - Order Flow Imbalance (OFI)
    - Moving Average Convergence Divergence (MACD)
    - Relative Strength Index (RSI)
    - Volume-Weighted Average Price (VWAP)
    - Volatility
    - Exponential Moving Average (EMA)
  - **Use Case**: These indicators are crucial for analyzing market trends and making predictions.

- `filter_time_window`: 
  - **Purpose**: Filters the data within a specified start and end time, useful for analyzing data in specific time frames.

### Analysis of Trading Activity

- `find_max_trading_hour`: 
  - **Functionality**: Identifies the one-hour time window with the highest trading activity.
  - **Importance**: This analysis could be pivotal in understanding market dynamics and peak trading periods.

### Visualization Functions

- `plot_histograms` and `plot_histograms_and_stats`: 
  - **Features**: These functions visualize the distribution of different factors (like OFI, MACD, etc.) using histograms. 
  - **Enhancement**: The latter function also provides basic statistics, enhancing the understanding of data distribution and trends.

### Challenges and Opportunities

- **Data Quality and Integrity**: Ensuring high data quality is essential, as the accuracy of the analysis depends on it.
- **Indicator Selection and Interpretation**: The choice of indicators and their interpretation plays a crucial role in stock market analysis. Understanding their implications in different market conditions is key.
- **Time-Sensitive Analysis**: The algorithm's ability to analyze specific time windows can uncover crucial insights into trading patterns.



### Histogram Analysis

#### Distribution of OFI (Order Flow Imbalance)
- **Observation**: Shows a relatively uniform distribution across the range, with some spikes.
- **Implication**: Suggests varied order flow with periods of both high buying and selling pressure.


#### Distribution of MACD (Moving Average Convergence Divergence)
- **Observation**: Centered around zero, with most data points within a narrow range.
- **Implication**: Indicates that the stock price did not deviate significantly from its trend.


#### Distribution of RSI (Relative Strength Index)
- **Observation**: Concentrated around the 50 mark, fewer occurrences of extreme values.
- **Implication**: Suggests the stock was not typically in an overbought or oversold condition.


#### Distribution of VWAP (Volume Weighted Average Price)
- **Observation**: Normal-like distribution centered around 151.
- **Implication**: Suggests a consistent average price weighted by volume.


#### Distribution of Volatility
- **Observation**: Lower volatility values are more common, with a rapid decrease in frequency as volatility increases.
- **Implication**: Indicates relative stability in stock price during the period analyzed.


#### Distribution of EMA (Exponential Moving Average)
- **Observation**: Exhibits a multi-modal distribution.
- **Implication**: Indicates several price levels around which the stock price tended to stabilize.


- ![Distribution plots](images/histrogram.png "Distribution Image")


#### Distribution of BidpriceChange and AskpriceChange
- **BidPriceChange**: Represents changes in the bid price, indicating buying pressure.
- **AskPriceChange**: Represents changes in the ask price, indicating selling pressure.

- ![Distribution of BidpriceChange and AskpriceChange](images/price_hist.png "Distribution Image 2")

### Interpretation and Implications

- **OFI**: A spike could indicate potential price movement due to an imbalance in buy and sell orders.
- **MACD**: Distribution near zero might suggest the absence of strong trends or reversion to the average.
- **RSI**: Concentration around 50 implies no extreme bullish or bearish momentum.
- **VWAP**: Provides insights into the fair value of the stock, considering volume.
- **Volatility**: Lower values favor certain trading strategies, like options.
- **EMA**: Highlights price levels for potential trade setups around these stabilizing points.



## Data Processing

#### Data Loading and Preparation

##### `load_dataframe_from_gz` Function
- **Functionality**: This function is designed to read gzip-compressed CSV files into pandas DataFrames.
- **Process**: It employs `gzip.open` for decompression and `pd.read_csv` for reading the file content.

##### Data Aggregation
- **Files**: The script processes a series of `.gz` files containing updates from the stock market.
- **Concatenation**: It uses `pd.concat` to amalgamate data from multiple files into a singular DataFrame.

#### Mid Price Calculation
- **Calculation**: The `mid_price` is determined as the mean of `ASK_PRICE_1` and `BID_PRICE_1`.
- **Purpose**: This calculation aims to provide a straightforward estimate of the stock's current market price.

#### Filtering Specific Stock Data
- **Selection**: The code filters the data specifically for the stock symbol "AAPL".
- **Cleaning**: It eliminates rows with missing values to maintain data integrity.

#### Time Series Processing
- **Conversion**: `COLLECTION_TIME` is converted into a datetime format.
- **Indexing**: The script sets `COLLECTION_TIME` as the DataFrame's index, facilitating time series analysis.

#### OHLC Data Aggregation
- **Resampling**: It generates OHLC data for varying time frames (1 second, 5 seconds, 1 minute) using `DataFrame.resample`.
- **Aggregation**: An aggregation dictionary is applied to summarize `mid_price` and volume data.
- **Column Renaming**: The script modifies column names for clarity, merging multi-level columns into a single level.

#### Technical Indicator Computation

##### Order Flow Imbalance (OFI)
- **Computation**: Calculated from the difference between total bid and ask volumes.
- **Usage**: This metric is indicative of the equilibrium between buying and selling pressure.

##### Relative Strength Index (RSI)
- **Calculation**: It determines overbought or oversold conditions based on price changes.
- **Window**: The calculation uses a 14-period rolling window.

##### Volatility
- **Computation**: Volatility is assessed via the rolling standard deviation of the percentage changes in `MID_PRICE_close`.
- **Window**: A 20-period window is used to gauge the intensity of price fluctuations.

##### Moving Average Convergence Divergence (MACD)
- **Components**: This is calculated using fast and slow exponential moving averages and their divergence.
- **Signal Line**: The MACD includes a signal line, which is an EMA of the MACD itself.

##### Exponential Moving Average (EMA)
- **Purpose**: The EMA is used to smooth out price data, aiding in identifying underlying trends over a chosen period.

#### Feature Engineering

##### Price Movement Features
- **Construction**: The script creates features based on the changes in `MID_PRICE_close`.
- **Lagging**: It introduces a lagged feature (`Y2`) for predictive modeling purposes.

##### Data Cleaning
- **NaN Handling**: Rows with NaN values are removed to ensure the completeness of the dataset.

#### Data Scaling and Transformation

##### Adjustments for Specific Columns
- **Scaling Factors**: Specific columns (MACD, SIGNAL_LINE, Y2, OFI) are multiplied by a factor of 5.
- **Volatility and RSI Scaling**: The script adjusts `Volatility` and `RSI` for better data representation.

### Data Segmentation for Model Training and Testing

#### Data Splitting
- **Objective**: Divides the dataset into training and testing subsets.
- **Methodology**: The code splits the data based on a predefined ratio (e.g., 80% for training and 20% for testing).
- **Time Window Definition**: The script sets distinct time windows for training (`train_start` to `train_end`) and testing (`test_start` to `test_end`).

#### `filter_time_window` Function
- **Purpose**: Filters the dataset based on a specified time window.
- **Implementation**: Converts 'COLLECTION_TIME' to `datetime` if necessary and applies a mask to select data within the given time range.

#### `get_train_test_windows` Function
- **Functionality**: Retrieves specific windows for training and testing datasets from the main DataFrame.
- **Error Handling**: Includes checks to ensure the specified time windows are within the dataset's overall time range and error handling for exceptions.

### Feature and Label Preparation

#### Feature and Label Selection
- **Selection of Features**: Identifies relevant features for the model, such as 'OFI', 'MACD', 'Volatility'.
- **Target Variable**: Selects the label columns, such as 'Y2'.

#### Data Split into Features and Labels
- **Training Data**: Splits the training data into features (`train_features`) and labels (`train_labels`).
- **Testing Data**: Similarly, splits the testing data into features (`test_features`) and labels (`test_labels`).

### Conversion to Tensors and DataLoader Creation

#### Data Conversion
- **Purpose**: Converts data into tensors suitable for use with PyTorch.
- **Process**: Transforms both features and labels into tensors for the training and testing sets.

#### DataLoader Setup
- **Batch Size**: Sets a batch size for loading the data (e.g., 64).
- **Training DataLoader**: Creates a DataLoader for the training dataset, facilitating batch-wise processing.
- **Testing DataLoader**: Similarly, prepares a DataLoader for the testing dataset.


## LSTM Model Architecture

In this section, we delve into the intricacies of the LSTM model architecture designed for predicting stock movements. The architecture is meticulously structured, balancing complexity and computational efficiency, a crucial aspect for handling time-sensitive financial data.

### Class Definition

- **Inheritance from `nn.Module`**: The `LSTMModel` class is derived from PyTorch's `nn.Module`, which is foundational for crafting custom neural network models. This inheritance provides essential functionalities like easy parameter management, forward pass definition, and compatibility with other PyTorch utilities.

### Constructor and Layer Definitions

1. **`__init__` Method**:
   - This constructor method is the heart of the model, setting up the layers and defining the model's architecture. It initializes the LSTM, batch normalization, ReLU activation, and fully connected layers with specified dimensions and functionalities.

2. **Layer Parameters**:
   - `input_dim`: This parameter specifies the number of input features, reflecting the dimensionality of the input data. In the context of stock data, it corresponds to the number of variables like price, volume, or other technical indicators used for each time step.
   - `hidden_dim`: The size of the hidden layers in the LSTM. A larger `hidden_dim` enables the model to capture more complex patterns in the data but increases computational complexity.
   - `output_dim`: Set to 1, indicating the model's task to predict a single value, such as the next price movement.
   - `num_layers`: The number of LSTM layers. Multiple layers can increase the model's ability to learn complex patterns but also make the model more prone to overfitting and increase training time.
   - `dropout`: A regularization technique to prevent overfitting. It randomly zeroes some of the elements of the input tensor with probability equal to the dropout rate during training.

3. **Layer Definitions**:
   - `nn.LSTM`: The LSTM layer captures temporal dependencies and sequences in the data. The `batch_first=True` parameter indicates that the first dimension of the input and output tensors will be the batch size.
   - `nn.BatchNorm1d`: Batch normalization standardizes the outputs of the LSTM layer, stabilizing and accelerating the training process.
   - `nn.ReLU`: The ReLU activation function introduces non-linearity, enabling the model to learn complex patterns beyond linear relationships.
   - `nn.Linear`: The fully connected layer that maps the output of the LSTM to the desired output dimension, providing the final prediction.

### Forward Pass Function

- **`forward` Method**:
   - This method defines the forward pass of the network. It sequentially passes the input through the LSTM layer, batch normalization, the ReLU activation, and finally through the fully connected layer to obtain the output.
   - The architecture of the forward pass is crucial as it dictates how the data flows through the model, influencing the model's ability to learn from the sequential stock data effectively.

### Rationale Behind Architecture Choices

- The choice of parameters and layers is driven by the need to capture the complex, time-dependent patterns inherent in stock price movements. 
- The multi-layer LSTM structure is chosen for its proficiency in modeling time-series data, essential for understanding sequential dependencies in stock prices.
- The balance between model complexity (number of layers, hidden dimensions) and the risk of overfitting is carefully considered, ensuring the model remains robust and generalizable to unseen data.



## Training the Model

The model training process is a critical phase in the LSTM project, involving several steps such as data splitting, feature scaling, and careful selection of hyperparameters. Below is a detailed explanation of the training process:

### Data Splitting and Time Window Filtering

1. **Data Splitting**: The dataset is divided into training and testing sets based on the `COLLECTION_TIME` column. A 80%-20% split ratio is used, where the first 80% of the data is allocated for training, and the remaining 20% for testing.

2. **Time Window Filtering**: The `filter_time_window` function is employed to filter the DataFrame based on specific start and end times. This function ensures that the model trains and tests on data within the defined time windows.

### Feature and Label Separation

- The training and testing sets (`train_df` and `test_df`) are further divided into features (`X_train`, `X_test`) and labels (`y_train`, `y_test`). These are then converted to numpy arrays, preparing them for tensor conversion.

### DataLoader Creation

- PyTorch's `TensorDataset` and `DataLoader` are used to create iterable datasets for both training and testing. `DataLoader` provides the ability to batch the data, shuffle it, and load it in parallel using multiprocessing workers.

### Hyperparameters and Model Initialization

1. **Hyperparameter Selection**:
   - `batch_size`: Set to 64, determining the number of samples propagated through the network before updating the model parameters.
   - `input_dim`: Set to 3, indicating the number of input features.
   - `hidden_dim`: Set to 20, defining the size of the LSTM hidden layers.
   - `output_dim`: Set to 1, as the model predicts a single value.
   - `num_layers`: Set to 5, indicating the number of stacked LSTM layers.
   - `learning_rate`: Set to 0.001, controlling the step size during gradient descent.
   - `num_epochs`: Set to 40, defining the number of complete passes through the training dataset.

2. **Model, Loss Function, Optimizer, and Scheduler**:
   - The LSTM model is initialized with the specified dimensions and layers.
   - Mean Squared Error Loss (`MSELoss`) is used as the criterion for training.
   - Adam optimizer is selected for optimizing the model, with a learning rate scheduler to adjust the learning rate over epochs.

### Training Loop

1. **Training Process**:
   - For each epoch, the model enters the training mode, and a forward pass is conducted with subsequent loss calculation.
   - Backward pass and optimization steps are performed to update the model weights.

2. **Evaluation and Learning Rate Update**:
   - After each epoch, the model is set to evaluation mode, and the performance is evaluated on the test set.
   - The scheduler updates the learning rate based on the predefined schedule.

3. **Output**:
   - Training and testing losses are printed after each epoch to monitor the model's performance and convergence.


## Model Evaluation and Results

The model's performance was evaluated using a variety of metrics, including visual comparisons of predicted versus actual labels and a confusion matrix for classification accuracy.

- **Comparison of Actual and Predicted Values**: 
  - The first graph provides a broad comparison over the entire test set. It's evident that while the model captures the trend at times, there are discrepancies that could be addressed.
  - ![Overall Actual vs Predicted](images/modelfinal_1.png)
  - The second graph focuses on a narrower sample range, where it can be seen that the predicted labels track the actual labels more closely, indicating a more precise prediction in this segment.
  - ![Focused Actual vs Predicted](images/modelfinal_2.png)

- **Confusion Matrix and Accuracy**:
  - The confusion matrix is a critical tool to assess the model's performance in classification tasks. The matrix and the subsequent accuracy calculation suggest the model is performing at a level above random chance, but there is significant room for improvement.
  - The calculated accuracy of the model, around 50.94%, serves as a benchmark for future iterations and enhancements.

## Conclusion and Future Work

The key insights from the model evaluation indicate that while the model has learned to some extent from the data, its predictive power is limited. The results show that the model's effectiveness in accurately predicting stock price movements is near a binary classification threshold, which may not be sufficient for high-stakes financial decisions. This suggests the complexity of the market dynamics might require more sophisticated models or additional features.

To enhance the model's accuracy and reliability, future work could include:

- **Feature Engineering**: Exploring additional features that may have predictive power, such as sentiment analysis from news articles or social media.
- **Hyperparameter Tuning**: Further tuning the model's hyperparameters could yield better results.
- **Model Complexity**: Testing more complex models or ensemble methods that could capture the nuances of the stock market more effectively.
- **Data Quality**: Incorporating higher-quality, error-free datasets, and increasing the granularity of the data may provide the model with more information for learning.
- **Time Series Analysis**: Applying time series analysis techniques to account for temporal dependencies between data points.

Ongoing research and iterative testing are crucial to improving the model's performance in future developments.


