// Refer to the header files for the explanation of the functions in detail
// In VSCode, hovering your mouse above the function renders the explanation of from the .h file as a pop up
#ifdef _WIN32
    #include "stdafx.h"
#endif

#include "LSTMStrategy0.h"

using namespace RCM::StrategyStudio;
using namespace RCM::StrategyStudio::MarketModels;
using namespace RCM::StrategyStudio::Utilities;

using namespace std;

const std::string pathname = "/home/vagrant/ss/sdk/RCM/StrategyStudio/examples/strategies/group_01_project/LSTM/final_models/modelfinal.pt";

// Constructor to initialize member variables of the class to their initial values.
LSTMStrategy0::LSTMStrategy0(StrategyID strategyID,
                    const string& strategyName,
                    const string& groupName):
    Strategy(strategyID, strategyName, groupName),
    reg_map_(),
    debug_(true),
    max_inventory(100),
    window_size(30),
    previous_prediction(0),
    inventory_liquidation_increment(10),
    inventory_liquidation_interval(10),
    bar_interval(1){
    }
// Destructor for class
LSTMStrategy0::~LSTMStrategy0(){

}

void LSTMStrategy0::DefineStrategyParams(){
}

void LSTMStrategy0::DefineStrategyCommands(){
}

// By default, SS will register to trades/quotes/depth data for the instruments you have requested via command_line.
void LSTMStrategy0::RegisterForStrategyEvents(StrategyEventRegister* eventRegister, DateType currDate)
{    
    for (SymbolSetConstIter it = symbols_begin(); it != symbols_end(); ++it) {
        eventRegister->RegisterForBars(*it, BAR_TYPE_TIME, bar_interval);
    }
    eventRegister->RegisterForRecurringScheduledEvents("liquidation_time",USEquityCloseUTCTime(currDate)-boost::posix_time::minutes((inventory_liquidation_increment+1)*inventory_liquidation_interval),USEquityCloseUTCTime(currDate)-boost::posix_time::minutes(inventory_liquidation_interval),boost::posix_time::minutes(inventory_liquidation_interval));
    ReadParameters();
}

void LSTMStrategy0::OnTrade(const TradeDataEventMsg& msg) {
    
    const Instrument* instrument = &msg.instrument();
    RecordAccountStats(instrument);
}

void LSTMStrategy0::OnScheduledEvent(const ScheduledEventMsg& msg) {
}

void LSTMStrategy0::OnOrderUpdate(const OrderUpdateEventMsg& msg) {
}

void LSTMStrategy0::OnBar(const BarEventMsg& msg){ 
    if (debug_) {
        ostringstream str;
        str << msg.instrument().symbol() << ": " << msg.bar();
        logger().LogToClient(LOGLEVEL_DEBUG, str.str().c_str());
    }
    
    //std::string pathname = "models/model.pth";
    
    // Check if we're already tracking the model object for this instrument, if not create a new one
    NNIterator nn_iter = nn_map_.find(&msg.instrument());
    if (nn_iter == nn_map_.end()) {
        nn_iter = nn_map_.insert(make_pair(&msg.instrument(), PyTorchLSTMModel(pathname))).first;
    }

    // Assuming you have some way to calculate or get the features for prediction
    double feature1 = -4.98; /* some calculation or value  OFIx5 */
    double feature2 = -0.65; /* some calculation or value MACDx5 */;
    double feature3 = 0.32;/* some calculation or value Volatilityx100 */;

    // Get the prediction from the model
    torch::Tensor prediction = nn_iter->second.predict(feature1, feature2, feature3);

    // Convert the prediction to a usable format, e.g., double
    double current_prediction = prediction.item<double>();
    
    const double threshold = 0.15; // Set a threshold for decision making

    if (current_prediction >= threshold) {
        // The model predicts an upward movement
        int size = msg.instrument().top_quote().ask_size();
        SendOrder(&msg.instrument(), size); // Consider buying
    } else if (current_prediction <= -1*threshold){
        // The model predicts a downward movement
        int size = msg.instrument().top_quote().bid_size();
        SendOrder(&msg.instrument(), -size); // Consider selling
    } else {
            if (debug_)
                cout << "No trade" << endl;
    }
    
    // std::string pathname = "models/model.pth";
    // //check if we're already tracking the momentum object for this instrument, if not create a new one
    // NNIterator nn_iter = nn_map_.find(&msg.instrument());
    // if (nn_iter == nn_map_.end()) {
    //     nn_iter = nn_map_.insert(make_pair(&msg.instrument(), PyTorchLSTMModel(path_name))).first;
    // }

    // double price = CalculateMidPrice(&msg.instrument());
    // double current_prediction = reg_iter->second.Update(price);

    // if (reg_iter->second.FullyInitialized()) {
    //     if (debug_)
    //         cout << "predicted value is " << current_prediction << "while current bid is " << msg.instrument().top_quote().bid() << "and current ask is " << msg.instrument().top_quote().ask() << endl;
    //     if (previous_prediction < msg.instrument().top_quote().bid() and current_prediction > msg.instrument().top_quote().bid()) {
    //         int size = msg.instrument().top_quote().ask_size();
    //         SendOrder(&msg.instrument(), size);
    //     } else if (previous_prediction > msg.instrument().top_quote().ask() and current_prediction < msg.instrument().top_quote().ask()){
    //         int size = msg.instrument().top_quote().bid_size();
    //         SendOrder(&msg.instrument(), -size);
    //     } else {
    //         if (debug_)
    //             cout << "No trade" << endl;
    //     }
    //     previous_prediction = current_prediction;
    // }
}

void LSTMStrategy0::OnQuote(const QuoteEventMsg& msg){
}

void LSTMStrategy0::InventoryLiquidation(){
    max_inventory -= inventory_liquidation_increment;
    if (max_inventory < 0)
        max_inventory = 0;

    if (debug_){
        cout << "liquidating inventory by " << inventory_liquidation_increment << endl;
        cout << "max inventory is now " << max_inventory << endl;
    }
}

void LSTMStrategy0::SetInventoryParams(const Instrument* instrument){
    if (max_inventory == -1){
        double price = CalculateMidPrice(instrument);
        cout << price << endl;
        if (std::isnan(price)) 
            return;
        cout << price << " " << portfolio().initial_cash() << endl;
        max_inventory = int(portfolio().initial_cash()/price);
        if (debug_)
            cout << "max inventory initially set to " << max_inventory << endl;
        inventory_liquidation_increment = ceil(max_inventory/inventory_liquidation_increment)+1;
        if (debug_)
            cout << "liquidation increment :- " << inventory_liquidation_increment << endl;
    }
}

void LSTMStrategy0::ReadParameters(){
    string line;
    ifstream myfile("/home/vagrant/ss/sdk/RCM/StrategyStudio/examples/strategies/group_01_project/RegressionMeanReversion/parameters.txt"); 
    if (myfile.is_open()) {
        while ( getline (myfile, line) ) {
            string key;
            string value;
            istringstream iss(line);
            getline(iss, key, '='); // Extract string up to the '=' character
            getline(iss, value);
            SetParamFromKey(key,value);
        }
        myfile.close();
    } else {
        cout << "Unable to open file to read parameters from file" << endl;
    }
}

void LSTMStrategy0::SetParamFromKey(string key, string value){

    if (key == "debug_"){
        debug_ = (value == "true" || value == "1");
    }
    else if (key == "max_inventory"){
        max_inventory = stoi(value);
    }
    else if (key == "window_size"){
        window_size = stoi(value);
    }
    else if (key == "previous_prediction"){
        previous_prediction = stod(value); // Assuming it's a double
    }
    else if (key == "inventory_liquidation_increment"){
        inventory_liquidation_increment = stoi(value);
    }
    else if (key == "inventory_liquidation_interval"){
        inventory_liquidation_interval = stoi(value);
    }
    else if (key == "bar_interval"){
        bar_interval = stoi(value);
    }
}

double LSTMStrategy0::CalculateMidPrice(const Instrument* instrument) {
    double top_bid = instrument->top_quote().bid();
    double top_ask = instrument->top_quote().ask();
    double mid_price = (top_bid + top_ask)/2;
    return mid_price;
}

void LSTMStrategy0::RecordAccountStats(const Instrument* instrument){
    if (debug_){
        cout << "Total PNL " << portfolio().total_pnl(instrument) << endl;
        cout << "Unrealised PNL " << portfolio().unrealized_pnl(instrument) << endl;
        cout << "Realised PNL " << portfolio().realized_pnl(instrument) << endl;
    }
}

void LSTMStrategy0::AdjustPortfolio() {
}

void LSTMStrategy0::SendOrder(const Instrument* instrument, int trade_size){
    if (!instrument->top_quote().ask_side().IsValid() || !instrument->top_quote().ask_side().IsValid()) {
        std::stringstream ss;
        ss << "Skipping trade due to lack of two sided quote"; 
        logger().LogToClient(LOGLEVEL_DEBUG, ss.str());
        return;
     }

    double price = trade_size > 0 ? instrument->top_quote().ask() : instrument->top_quote().bid();

    OrderParams params(*instrument,
                       abs(trade_size),
                       price,
                       MARKET_CENTER_ID_NASDAQ,
                       (trade_size > 0) ? ORDER_SIDE_BUY : ORDER_SIDE_SELL,
                       ORDER_TIF_DAY,
                       ORDER_TYPE_LIMIT);

        // Print a message indicating that a new order is being sent
        std::cout << "SendTradeOrder(): about to send new order for size "
                << trade_size
                << " at $"
                << price
                << " for symbol "
                << instrument->symbol()
                << std::endl;
        
        trade_actions()->SendNewOrder(params);
}

void LSTMStrategy0::OnMarketState(const MarketStateEventMsg& msg){
    cout << msg.name() << endl;
}

void LSTMStrategy0::OnResetStrategyState() {
    reg_map_.clear();
}

void LSTMStrategy0::OnParamChanged(StrategyParam& param) {
}
