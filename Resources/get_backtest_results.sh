#!/bin/bash

STRATEGY=$1
# Define remote host information
REMOTE_USER="vagrant"
REMOTE_HOST="207.237.194.196"
REMOTE_PORT="10023"
REMOTE_DIR="/groupstorage/group01/backtest_data/${STRATEGY}"
# Define local directory
LOCAL_DIR="Backtest_results/${STRATEGY}/"

# SSH identity file
IDENTITY_FILE="~/.ssh/id_ed25519_sk_new"


# Check if strategy directory exists on the remote server
if ssh -i $IDENTITY_FILE -p $REMOTE_PORT $REMOTE_USER@$REMOTE_HOST "[ -d ${REMOTE_DIR} ]"; then
    echo "directory exists..."

    # Create local directory if it doesn't exist
    mkdir -p $LOCAL_DIR

    # Copy the entire directory
    echo "Copying directory..."
    scp -r -i $IDENTITY_FILE -P $REMOTE_PORT "${REMOTE_USER}@${REMOTE_HOST}:${REMOTE_DIR}" "$LOCAL_DIR"
else
    echo "directory does not exist on the remote server"
fi

