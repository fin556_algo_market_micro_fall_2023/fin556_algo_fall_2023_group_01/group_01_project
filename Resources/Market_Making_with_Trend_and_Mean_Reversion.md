# Market-Making Trading Strategy with Inventory Control and Order Size Scaling

## Table of Contents
1. [Introduction](#introduction)
    1.1. [Market Making](#market-making)
        - Mechanisms
        - Risks and Challenges
    1.2. [Inventory Control in Market Making](#inventory-control-in-market-making)
        - Objectives
        - Strategies
        - Quantitative Techniques

2. [Literature Review](#literature-review)
    2.1. [Market Making](#market-making-1)
        - High-Frequency Market Making
        - Enhancing Trading Strategies with Order Book Signals
    2.2. [Inventory Control](#inventory-control)
        - High-frequency Trading in a Limit Order Book
        - Dealing with the Inventory Risk: A solution to the market making problem
    2.3. [Indicators](#indicators)
        - The price impact of order book events

3. [Data Sourcing and Collection](#data-sourcing-and-collection)
    3.1. [Databento Overview](#databento-overview)
    3.2. [Data Offered](#data-offered)
    3.3. [Data Formats and Features](#data-formats-and-features)
    3.4. [Getting Started](#getting-started)
    3.5. [Additional Resources](#additional-resources)
    3.6. [IEX DEEP (Depth of Book) Data](#iex-deep-depth-of-book-data)
    3.7. [IEX TOPS (Top of Book) Data](#iex-tops-top-of-book-data)
    3.8. [Data Collection Methodology](#data-collection-methodology)
    3.9. [Data Parsing](#data-parsing)
    3.10. [Data periods to run tests on](#data-periods-to-run-tests-on)

4. [Methodology](#methodology)
    4.1. [Market Making](#market-making-2)
        - Mean-Reversion Positioning
        - Momentum-Based Positioning
    4.2. [Inventory Control](#inventory-control-1)
        - Parameters

## Introduction
### Market Making

Market making is a trading strategy that aims to provide liquidity to financial markets. Market makers quote both a buy and a sell price in a financial instrument or commodity, effectively acting as intermediaries between buyers and sellers. By doing so, market makers facilitate trade and reduce transaction costs by narrowing the bid-ask spread.

#### Mechanisms
- **Quoting Prices**: Market makers provide bid and ask quotes, creating a market where traders can buy or sell assets.
- **Order Matching**: Market makers match incoming market orders with limit orders on the opposite side.
- **Spread Profit**: The difference between the ask and bid price is known as the spread, which serves as the profit margin for market makers.

#### Risks and Challenges
- **Inventory Risk**: Holding a large position in an asset exposes the market maker to price volatility.
- **Adverse Selection**: Skilled traders may exploit the market maker's quotes, leading to potential losses.

### Inventory Control in Market Making

Inventory control is a critical aspect of market making, requiring rigorous quantitative strategies to manage the inherent risks. The primary objective is to maintain an optimal inventory level that balances profitability with risk exposure. 

#### Objectives

- **Risk Minimization**: Limit exposure to adverse price movements by maintaining a balanced or neutral inventory position.
- **Profit Maximization**: Optimize the holding period and size of inventory to take advantage of pricing inefficiencies and earn the spread.

#### Strategies

- **Mean-Reversion Positioning**: When the asset exhibits mean-reverting behavior, the inventory control system may allow for more significant positions, expecting the price to revert to the mean.
  
- **Momentum-Based Positioning**: In trending markets, the strategy may opt to hold positions that are in line with the trend to capture more of the price movement.

#### Quantitative Techniques

- **Optimal Control Theory**: Utilizes stochastic control methods to dynamically adjust inventory levels based on real-time market data and predefined risk parameters.
  
- **Utility-Based Models**: These models maximize a utility function that accounts for both the expected profit and the associated risk, often parameterized by the market maker's risk aversion.

By incorporating these advanced techniques and considerations, inventory control in market making becomes a highly sophisticated operation that plays a crucial role in the overall profitability and risk management of the trading strategy.

## Literature Review

This section provides an overview of key research papers that lay the foundational theories and methodologies relevant to market making and inventory control. Below are some seminal works:

### Market Making

#### format to be followed
1. **"High-Frequency Market Making" by [Your Name](link-to-your-paper)**
   - This paper discusses the application of high-frequency data in market-making strategies. It introduces a framework for using tick-level data to improve quoting strategies and risk management.

1. **"Enhancing Trading Strategies with Order Book Signals" by [Álvaro Cartea, Ryan Donnelly, Sebastian Jaimungal](https://ora.ox.ac.uk/objects/uuid:006addde-3a03-4d75-89c1-04b59026e1c0/download_file?file_format=application%2Fpdf&safe_filename=Imbalance_AMF_resubmit.pdf) , [article](https://towardsdatascience.com/price-impact-of-order-book-imbalance-in-cryptocurrency-markets-bf39695246f6)**
   - This paper utilizes high-frequency Nasdaq data to create a volume imbalance measure in the limit order book (LOB). The measure is shown to predict the sign of upcoming market orders and immediate price changes. The authors introduce a Markov chain modulated pure jump model for price, spread, and order arrivals, and solve a stochastic control problem to optimize trading strategies. The inclusion of volume imbalance significantly enhances strategy profits.

### Inventory Control

1. **"High-frequency Trading in a Limit Order Book" by [Marco Avellaneda and Sasha Stoikov](https://math.nyu.edu/~avellane/HighFrequencyTrading.pdf) ,[article](https://medium.com/hummingbot/a-comprehensive-guide-to-avellaneda-stoikovs-market-making-strategy-102d64bf5df6)**
   - This paper explores the optimal strategies for placing bid and ask orders in high-frequency trading within a limit order book. It combines market microstructure theory with econophysics to model a dealer's role and associated risks. The paper is particularly relevant for high-frequency trading and market-making strategies.

2. **"Dealing with the Inventory Risk: A solution to the market making problem" by [Olivier Guéant, Charles-Albert Lehalle, Joaquin Fernandez-Tapia](https://arxiv.org/pdf/1105.3115.pdf) , [alternate paper](https://hal.science/hal-00628528/document)**
   - This paper addresses the complex optimization problem faced by market makers in setting bid and ask quotes. It extends the model introduced by Avellaneda and Stoikov by adding inventory constraints. The paper employs stochastic optimal control and Hamilton-Jacobi-Bellman equations to derive optimal quoting strategies. It also provides closed-form approximations of the optimal quotes. The paper is particularly relevant for understanding the dynamics of high-frequency market making and inventory risk management.

### Indicators
1. **"The price impact of order book events" by [Rama Cont, Arseniy Kukanov and Sasha Stoikov](https://arxiv.org/pdf/1011.6402.pdf) , [article](https://towardsdatascience.com/price-impact-of-order-book-imbalance-in-cryptocurrency-markets-bf39695246f6)**
   - The study utilizes NYSE TAQ data for 50 U.S. stocks to examine the price impact of different types of order book events, including limit orders, market orders, and cancellations. It finds that short-term price changes are predominantly influenced by the order flow imbalance, which is the disparity between supply and demand at the best bid and ask prices. The research establishes a linear relationship between order flow imbalance and price changes. This relationship's slope is inversely proportional to the market depth. The study also confirms that these findings are consistent across different time scales and stocks. Furthermore, the linear price impact model implies the observed "square-root" relationship between the magnitude of price moves and trading volume, although this latter relationship is considered to be noisier and less robust.

## Data Sourcing and Collection

This project utilizes high-frequency data from multiple different sources such as the IEX exchange, Nasdaq Itch, CME Globex. These feeds offer granular market activity data which is crucial for the sophisticated quantitative analyses employed in this work.

### Databento Overview:
- Provides a streamlined method to obtain market data.
- Integrated with over a hundred trading venues and data providers.
- Aims to reduce the effort and cost of bringing a new dataset to production ([source](https://databento.com/about)).

#### Data Offered:
- Live and historical market data.
- Covers multiple asset classes and venues.
- Data is provided through a unified message format.
- Datasets from various exchanges like CME and Nasdaq are available.
- The data catalog can be browsed to explore datasets and their details ([source](https://docs.databento.com/browse)).

#### Data Formats and Features:
- Various schemas like BBO (Best Bid Offer), order book, bars, minute, daily, tick.
- Multiple encodings are supported.
- Sub-5 microsecond normalization latency and zero packet loss on billions of data messages per day, making it ideal for mission-critical applications such as order routing and risk management.
- Fast, zero-copy message encoding and storage format for market data.
- Standards and conventions for date and timestamp formats, price formats, flags, symbology, among others are defined ([source](https://docs.databento.com/knowledge)).

#### Getting Started:
- Databento has a small API surface and simple protocols, allowing for quick onboarding, reportedly in as little as three minutes ([source](https://docs.databento.com)).

#### Additional Resources:
- [Databento Documentation](https://docs.databento.com/)
- [Databento Data Catalog](https://docs.databento.com/browse)

### IEX DEEP (Depth of Book) Data

IEX's DEEP (Depth of Book) data feed provides an extensive overview of the market's order book for each security traded on the IEX exchange. It consists of real-time order book information including bids and asks at various price levels, along with the size of these orders. DEEP also contains data about executed trades, system event messages, and auction information. This data is critical for comprehending market microstructure, liquidity, and supply and demand dynamics at various price points. The granularity of DEEP data is instrumental for designing, backtesting, and implementing high-frequency trading strategies.

### IEX TOPS (Top of Book) Data

IEX's TOPS (Top of Book) data feed provides real-time top-of-book market data including the best bid and ask prices, and the size of orders at these prices, for each security traded on the IEX exchange. Additionally, TOPS contains last trade price and size information, which is crucial for a multitude of quantitative analyses. This data is vital for understanding the latest market pricing and liquidity information, and serves as a valuable input for price discovery, market microstructure studies, and alpha model development.

#### Data Collection Methodology

The data collection process entails subscribing to the IEX DEEP and TOPS data feeds, setting up a robust data ingestion pipeline to handle the high-frequency data efficiently. The pipeline is designed to accommodate the high-throughput and low-latency requirements that are inherent to high-frequency trading environments, ensuring data integrity and facilitating easy data retrieval and analysis for subsequent model development and backtesting.

#### Data Parsing

<!--section to be filled based on professors code -->

### Data periods to run tests on
- Covid crash of march 2020: 14 Feb 2022 - 14 April 2022
- High VIX periods during russian invasion of ukraine: 9 Feb 2022 - 9 April 2022
- Dates with Massive Market moves: 

## Methodology
### Market Making

#### Mean-Reversion Positioning
When the asset exhibits mean-reverting behavior, the inventory control system may allow for more significant positions, expecting the price to revert to the mean. we quote bid and ask prices based on the prices dictated using the price and inventory levels as input. we look to keep our inventory levels to be mean reverting. time for measn reversion cycle must be calibrated to see which generates most profits. Based on empirical results we will decide on this mean reversion value.

#### Momentum-Based Positioning
In trending markets, the strategy may opt to hold positions that are in line with the trend to capture more of the price movement. inventory levels will be decided based on an indicator with values between -1 to 1 multiplied with the max inventory. we can use a simple indicator such as distance of the midprice from the 200 EMA as an indicator to decide how much our target inventory is going to be.
- example with EMA distance as an indicator
   * let's say that 
 
### Inventory Control
#### Parameters:

* s = current market mid price
* q = quantity of assets in inventory of base asset (could be positive/negative for long/short positions)
* σ = market volatility
* T = closing time, when the measurement period ends (conveniently normalized to 1)
* t = current time (T is normalized = 1, so t is a time fraction)
* δa, δb = bid/ask spread, symmetrical → δa=δb
* γ = inventory risk aversion parameter
* κ = order book liquidity parameter


**Formula to calculate the reservation price**
![reservation price](https://miro.medium.com/v2/resize:fit:640/format:webp/0*PBRX7TA7k_RL1MC2.png)


**Formula to calculate the bid ask spread**
![bid ask spread](https://miro.medium.com/v2/resize:fit:640/format:webp/0*Uop6SIjlwO3Pjw77.png)
