
---
title: "Project Proposal"
author: "Group 1"
date: "Oct 31,2023"
output: pdf_document
---

## Team
Hariharan Manickam - MS Financial Engineering - Graduating May 2024<br>
Shumeng Zhang - BS Engineering Mechanics - Graduating May 2025 - Maybe a masters in Finance from CMU or in NY<br>
Samanvay Malapally Sudhakara - M.S.Financial Engineering - Graduating May 2024<br>
Deeraj Kakumani - MS Statistics - Graduating May 2024 <br>

## Technologies
#### Strategy Studio
RCM-X Strategy Studio is a high performance C++ multi asset class strategy
development platform, designed for efficient implementation, testing, and deployment of
algorithmic trading strategies. Strategy Studio provides a Strategy Development API
which is agnostic to data feed and execution providers, allowing you to focus on the
logic of your strategies. Strategy Studio also includes a set of Strategy Servers, which
are responsible for loading and running your trading strategies, along with the Strategy
Manager, a performance monitoring and risk management user interface that you can
use to centralize your research and production trading.

#### Frameworks
- Rest APIs
- PyBind
- Pytorch/Tensorflow

#### Libraries
- numpy
- scikit-learn
- pandas
- databento

#### Languages
- C++
- Python

## Data

#### Assets
Top 50 traded tickers on NASDAQ. We will use these same tickers on IEX

#### Symbols
- Yet to find

### Sources
- NASDAQ ITCH - for C++ parser
- NASDAQ ITCH parsed from databento
- IEX from given scripts
- CME GLOBEX from databento
- OPRA Equity OPTION data from databento

### Date Range
- 2020-2022

### Reasoning
- Significant high VIX periods because of COVID & Wars
- Data is relatively recent

## Bare Minimum

#### Data
- Try different APIs to find data providers
    - Bloomberg
    - Alpha Vantage
    - Twelvedata
    - EoDHD
    - yfinance

- Create datasets for our strategies for statistical analysis
- Parse IEX Data using given scripts
- Parse NASDAQ data using c++ parser
- Fetch data from Databento

## Expected Goals

#### Data Analysis
- Statistical Data Analysis on our data
- Parse all data sources into Strategy Studio format
- Extract more messages from IEX

#### Strategy 1 - Exchange Arbitrage
- Get the open source C++ parser working
- Run backtests using IEX & NASDAQ data

#### Strategy 2 - Machine Learning
- Build featureset
- Basic working Long Short Term Memory model
- Basic working Transformer model

## Strategy 3 - Market Making
- Calibrate parameters
- Backtest on historical data
- Test with different latencies


## Reach Goals
#### Strategy 1 - Exchange Arbitrage

**Price Discrepancies:** We identify assets that have different prices across various exchanges. These discrepancies may arise due to differences in supply and demand dynamics, variations in trading volumes, or temporary inefficiencies in the markets.

**Simultaneous Transactions:** Once an opportunity is identified, the strategy involves buying the undervalued asset on one exchange and selling it where it's overvalued. These transactions typically need to be executed simultaneously to capture the price difference before the market adjusts.

**Latency Sensitivity:** The strategy's success is often dependent on the latency, which is the time taken to execute the trades. Lower latency (faster execution) can lead to more significant profits because the price discrepancy is less likely to disappear before the arbitrage is complete.

**Profit and Loss Analysis:** Traders need to constantly analyze the profitability of their trades, taking into account transaction costs, fees, and potential slippage (the difference between the expected price and the price at which the trade is actually executed).

#### Strategy 2 - Machine Learning

We are predicting next nanosecond price change and taking positions accordingly.

**Models:**
Long Short Term Memory
Transformer Architechture

**Features:**
- Order Flow Imbalance
- Technical Indicators
- Volume
- Statistics of different order types
We would be show reuslts of different combinations of our featureset for our 2 models.

**Stacking:**
We would be trying a combination of both these models together to see how they perform.

#### Strategy 3 - Market Making

Market making is a trading strategy that aims to provide liquidity to financial markets. Market makers quote both a buy and a sell price in a financial instrument or commodity, effectively acting as intermediaries between buyers and sellers. By doing so, market makers facilitate trade and reduce transaction costs by narrowing the bid-ask spread.

**Mean-Reversion Positioning**
When the asset exhibits mean-reverting behavior, the inventory control system may allow for more significant positions, expecting the price to revert to the mean. we quote bid and ask prices based on the prices dictated using the price and inventory levels as input. we look to keep our inventory levels to be mean reverting. time for measn reversion cycle must be calibrated to see which generates most profits.
- start: At the start we quote the best bid and offer and move quotes as this spread moves
- cycle: we run the strategy such that the inventory levels reach 0 every T seconds(needs to be calibrated).
- end: Any remaining inventory is liquidated moments before close at order book bids and asks.

**Momentum-Based Positioning**
In trending markets, the strategy may opt to hold positions that are in line with the trend to capture more of the price movement. inventory levels will be decided based on an indicator with values between -1 to 1 multiplied with the max inventory. target time to achieve said inventory levels would range from 1-2 seconds (must be optimised)
- start: At the start we quote the prices decided by set formula using the indicator.
- cycle: we run the strategy such that the inventory levels reach the target inventory every T seconds(needs to be calibrated).
- end: Any remaining inventory is liquidated moments before close at order book bids and asks based on net inventory.

We will be using the following methods in our market making strategy

**Mechanisms:**
- Quoting Prices: Market makers provide bid and ask quotes, creating a market where traders can buy or sell assets.
- Order Matching: Market makers match incoming market orders with limit orders on the opposite side.
- Spread Profit: The difference between the ask and bid price is known as the spread, which serves as the profit margin for market makers.

**Risks and Challenges**
- Inventory Risk: Holding a large position in an asset exposes the market maker to price volatility.
- Adverse Selection: Skilled traders may exploit the market maker's quotes, leading to potential losses.

**Inventory Control in Market Making**
Inventory control is a critical aspect of market making, requiring rigorous quantitative strategies to manage the inherent risks. The primary objective is to maintain an optimal inventory level that balances profitability with risk exposure. 

**Objectives**

- Risk Minimization: Limit exposure to adverse price movements by maintaining a balanced or neutral inventory position.
- Profit Maximization: Optimize the holding period and size of inventory to take advantage of pricing inefficiencies and earn the spread.

**Strategies**
- Mean-Reversion Positioning: When the asset exhibits mean-reverting behavior, the inventory control system may allow for more significant positions, expecting the price to revert to the mean.
  
- Momentum-Based Positioning: In trending markets, the strategy may opt to hold positions that are in line with the trend to capture more of the price movement.

**Quantitative Techniques**

- Optimal Control Theory: Utilizes stochastic control methods to dynamically adjust inventory levels based on real-time market data and predefined risk parameters.
  
- Utility-Based Models: These models maximize a utility function that accounts for both the expected profit and the associated risk, often parameterized by the market maker's risk aversion.

By incorporating these advanced techniques and considerations, inventory control in market making becomes a highly sophisticated operation that plays a crucial role in the overall profitability and risk management of the trading strategy.
