# High-Frequency Trading Strategy Development with RCMX Strategy Studio

## I. Preliminaries
   - Executive Summary
   - Acknowledgements
   - The Team

## 1. Introduction
   - 1.1 Overview of High-Frequency Trading
   - 1.2 Objectives and Scope of the Project
   - 1.3 Importance of a Thorough and Sophisticated Backtest in HFT

## 2. Data Collection and Preparation
   - 2.1 Data Sources and Acquisition
   - 2.2 Data Cleaning and Pre-processing

## 3. Strategy Development
   - 3.1 Overview of Strategy Studio
   - 3.2 Developing the Trading Algorithms
   - 3.3 Our Strategies
   - 3.4 Risk Management Protocols in High-Frequency Trading (HFT) Strategies

## 4. Exchange Arbitrage
   - 4.1 Overview
   - 4.2 Data Preparation
   - 4.3 Algorithm

## 5. Mid-Price Imbalance Strategy
   - 5.1 Calculation of VWAP and Simple Mid-Price
   - 5.2 Statistical Approach: Z-Score Analysis
   - 5.3 Signal Generation Using Z-Score
   - 5.4 Risk Management and Backtesting
   - 5.5 Performance Metrics

## 6. Momentum Market Making
   - 6.1 Strategy Description
   - 6.2 Algorithm
   - 6.3 Parameters
   - 6.4 Formulas
   - 6.5 Risk Management

## 7. Regression Mean Reversion
   - 7.1 Concept and Calculation of the Line of Best Fit
   - 7.2 Trade Initiation Based on Price Prediction
   - 7.3 Strategy Parameters
   - 7.4 Risk Management and Backtesting
   - 7.5 Performance Metrics

## 8. LSTM Strategy
   - 8.1 Overview
   - 8.2 Exploratory Data Analysis for LSTM Model
   - 8.3 Data Processing
   - 8.4 Algorithm - Long Short Term Memory Model
   - 8.5 Parameter Optimisation
   - 8.6 Conclusion
   - 8.7 Future Work

## 9. Transformer Model Report
   - 9.1 Introduction
   - 9.2 The Core Idea behind the Transformer
   - 9.3 Self-Attention Mechanism
   - 9.4 Setup and Dependencies
   - 9.5 Data Pre-processing
   - 9.6 Transformer Model Architecture
   - 9.7 Model Performance

## 10. Parameter Optimization and Strategy Tuning
   - 10.1 Data Management and Initial Setup
   - 10.2 Generating Parameter Combinations
   - 10.3 Backtesting Workflow
   - 10.4 Optimization and Evaluation
   - 10.5 Testing and Validation
   - 10.6 Problems and Limitations

## 11. Results and Analysis Methodology
   - 11.1 Overview of the Analysis Process
   - 11.2 Key Components of the Analysis

## 12. Conclusions and Future Directions
   - 12.1 Summary of Key Findings
   - 12.2 Strategic Recommendations
   - 12.3 Future Research



## Executive Summary

This document presents a comprehensive exploration of our high-frequency trading (HFT) strategy, developed using Strategy Studio, a multi asset class strategy development software. The strategy, positioned at the intersection of advanced quantitative analysis and cutting-edge technology, aims to capture market inefficiencies through rapid execution and sophisticated algorithms. This project encapsulates our journey from initial concept to deployment, including rigorous backtesting, parameter optimization, and results analysis.

Our objective is to not only detail the strategy's development and performance but also to delve into the challenges faced, the technological tools employed, and the regulatory landscape navigated. We believe this document will serve as an invaluable resource for stakeholders, providing insights into our methodological rigor and strategic foresight.

## Acknowledgements

We extend our gratitude to the team members, collaborators, and mentors who contributed to the success of this project. Their expertise, dedication, and innovative thinking have been instrumental in navigating the complex world of high-frequency trading. We thank RCM Technologies, for allowing us to use their software for educational purposes.

## The Team 

### Hariharan Manickam

Hariharan Manickam, a dedicated second-year graduate student in Financial Engineering at the University of Illinois Urbana Champaign, is deeply engaged in quantitative analysis, algorithmic trading, and generative AI. He is currently working part time with RCM Technologies and Goose Hollow Capital. He has prior experience with the CME Group and Grant Thornton. He will be graduating in May 2024. Please do reach out if you want to connect. [Linkedin](https://www.linkedin.com/in/haran1013/)

### Samanvay Malapally Sudhakara

Samanvay Malapally Sudhakara, a dedicated second-year graduate at the University of Illinois Urbana Champaign, is studying Financial Engineering. He has a growing interest in Quantitative Trading and Research, with a particular focus on Machine Learning Algorithms, Reinforcement Learning, and Large Language Models. He has experience with very large datasets such as the FNFA Fannie Mae Single Family Home Mortgage Database, years of Forex Tick Data, NASDAQ ITCH Market By Order data, Orderbook snapshots, Trades and Tick Data to name a few. With 3 years experience in retail trading, Samanvay aims to integrate this practical knowledge with his academic studies to adapt to an institutional setting effectively. He is on track to graduate in May 2024 and is looking for full-time opportunities as a Quantitative Researcher or Trader, where he can apply his skills and continue to learn in the field. [Linkedin](https://www.linkedin.com/in/samanvay-malapally-sudhakara-148836212/)

### Deeraj Kakumani

Deeraj is a second year graduate student pursuing a Master's in Statistics at the University of Illinois Urbana Champaign. He has interests in quantitative analysis, strategic financial modeling, and machine learning. He has a solid foundation in advanced statistical methods, probability theory, and data analysis. He has a good coding skills and programming experience, which he has honed during his tenure as a Machine Learning Engineer at Qualcomm. He is deeply fascinated by the application of quantitative skills in financial analysis and modeling. He will be graduating in May 2024 and is looking for full-time opportunities as a Quantitative Researcher or Quantitative Analyst. While I have a good foundational understanding of these areas, I am eager to expand my knowledge and apply it in a practical, impactful way.
[Linkedin - Deeraj](https://www.linkedin.com/in/deeraj-kakumani-53916a1b1/) 

### Shumeng Zhang

Shumeng is a third year undergraduate student pursuing his Bachelor in Engineering Mechanics at the University of Illinois Urbana Champaign. He will be graduating in May 2025. His post-graduation plans include further academic pursuits in graduate school. His interests lie in quantitative analysis and algorithmic trading, with a particular emphasis on high-frequency equity trading and applying machine learning algorithms for factor investment. In the spring of 2024, he plans to undertake an internship as a machine learning engineer in Beijing, China.
[LinkedIn](https://www.linkedin.com/in/shumeng-zhang-a54292204/)

# 1. Introduction

## 1.1 Overview of High-Frequency Trading

High-Frequency Trading (HFT) represents a significant evolution in financial markets, leveraging advanced algorithms, high-speed data networks, and ultra-low latency communications. Its primary objective is to execute large numbers of orders at extremely high speeds, often in fractions of a second. HFT strategies are employed by proprietary trading firms and institutional traders to capitalize on very small price discrepancies in financial instruments.

### Order Books in HFT

An order book is a fundamental component in HFT, as it contains all buy and sell orders in the market for a specific financial instrument.

- **Structure**: It's typically organized in a double auction format, displaying bids (buy orders) and asks (sell orders) along with their respective quantities.
- **Depth**: The depth of an order book indicates how many orders are present at different price levels beyond the best available bid and ask prices.
- **Price-Time Priority**: Most exchanges use a price-time priority rule, where the best price gets priority, and for orders at the same price, the order that arrived first has priority.

### Order Types

HFT utilizes various order types to implement strategies:

- **Market Orders**: Orders to buy or sell immediately at the best available price.
- **Limit Orders**: Orders to buy or sell at a specific price or better.
- **Stop Orders**: Orders become active only after a certain price level is reached.
- **Iceberg Orders**: Large orders hidden except for a small portion to mask the true order quantity.
- **Pegged Orders**: Orders with a price tied to a benchmark, often changing as the benchmark changes.

### Latency in HFT

Latency is the time it takes for an order to be transmitted, processed, and executed. In High Frequency Trading, maintaining low latency is crucial.

- **Network Latency**: Time taken for data to travel across the network.
- **Processing Latency**: Time taken for systems to process the orders.
- **Execution Latency**: Time from sending an order to the execution.

Low-latency technologies include co-location (placing servers near exchange servers), advanced hardware (like FPGAs, radio towers), and optimized software algorithms.

### HFT Strategies

Some common HFT strategies include:

- **Market Making**: Providing liquidity by continuously buying and selling.
- **Arbitrage**: Exploiting price discrepancies in different markets or instruments.
- **Statistical Arbitrage**: Using statistical models to predict price movements.
- **Event-Driven Trading**: Capitalizing on events like earnings reports or economic announcements.

## 1.2 Objectives and Scope of the Project
The primary objective of this project is to develop a framework to backtest and optimize high-frequency trading (HFT) strategies that leverage various technological and analytical approaches. Key focuses include latency arbitrage, momentum-based inventory control, and the application of advanced machine learning techniques such as Long Short Term Memory neural networks and Transformers.

**Scope:**
<br>

- **Latency Arbitrage:** Explore and implement strategies that capitalize on the speed advantage in trade execution. This involves identifying price discrepancies caused by latency differences and executing trades before these gaps are closed.

- **Momentum-Based Inventory Control:** Develop algorithms to manage and adjust inventory based on market momentum, ensuring profitability while minimizing risk exposure. Optimize parameters to identify optimal strategy.

- **Machine Learning:** Employ LSTM and Transformer models to analyze market data, predict trends, and make informed trading decisions. These models will be trained on historical data to recognize patterns and adapt to changing market conditions.

## 1.3 Importance of a Thorough and Sophisticated backtest in HFT

Backtesting is a critical component in the development of HFT strategies. Its importance lies in:

- **Risk Management:** Backtesting allows for the evaluation of a strategy's risk profile, enabling traders to understand potential drawdowns and the behavior of the strategy under various market conditions.

- **Strategy Validation:** It provides empirical evidence to validate the effectiveness of a trading strategy. Through backtesting, one can assess if a strategy that performs well in theory also succeeds in real-world market environments.

- **Overfitting Avoidance:** Sophisticated backtesting methods help in identifying and preventing overfitting, where a model is excessively tailored to historical data and fails to generalize to new data.

- **Parameter Optimization:** It enables the fine-tuning of model parameters, ensuring that the strategy remains robust across different time frames and market conditions.

- **Benchmarking:** Backtesting against benchmark indices or control strategies provides a relative performance measure, crucial for evaluating the added value of the developed HFT strategies.

- **Regulatory Compliance:** In many jurisdictions, backtesting is a regulatory requirement to ensure that the strategies are not manipulative and are in compliance with market standards.

# 2 Data Collection and Preparation

## 2.1 Data Sources and Acquisition

This project utilizes high-frequency data from multiple different sources such as the IEX exchange, Nasdaq ITCH, CME Globex. These feeds offer granular market activity data which is crucial for the sophisticated quantitative analyses employed in this work.

### Databento Overview

- Provides a streamlined method to obtain market data.
- Integrated with over a hundred trading venues and data providers.
- Aims to reduce the effort and cost of bringing a new dataset to production ([source](https://databento.com/about)).

#### Data Offered

- Live and historical market data.
- Covers multiple asset classes and venues.
- Data is provided through a unified message format.
- Datasets from various exchanges like CME and Nasdaq are available.
- The data catalog can be browsed to explore datasets and their details ([source](https://docs.databento.com/browse)).

#### Data Formats and Features

- Various schemas like BBO (Best Bid Offer), order book, bars, minute, daily, tick.
- Multiple encodings are supported.
- Sub-5 microsecond normalization latency and zero packet loss on billions of data messages per day, making it ideal for mission-critical applications such as order routing and risk management.
- Fast, zero-copy message encoding and storage format for market data.
- Standards and conventions for date and timestamp formats, price formats, flags, symbology, among others are defined ([source](https://docs.databento.com/knowledge)).

#### Getting Started

- Databento has a small API surface and simple protocols, allowing for quick onboarding, reportedly in as little as three minutes ([source](https://docs.databento.com)).

#### Additional Resources

- [Databento Documentation](https://docs.databento.com/)
- [Databento Data Catalog](https://docs.databento.com/browse)

### IEX DEEP (Depth of Book) Data

IEX's DEEP (Depth of Book) data feed provides an extensive overview of the market's order book for each security traded on the IEX exchange. It consists of real-time order book information including bids and asks at various price levels, along with the size of these orders. DEEP also contains data about executed trades, system event messages, and auction information. This data is critical for comprehending market microstructure, liquidity, and supply and demand dynamics at various price points. The granularity of DEEP data is instrumental for designing, backtesting, and implementing high-frequency trading strategies.

### IEX TOPS (Top of Book) Data

IEX's TOPS (Top of Book) data feed provides real-time top-of-book market data including the best bid and ask prices, and the size of orders at these prices, for each security traded on the IEX exchange. Additionally, TOPS contains last trade price and size information, which is crucial for a multitude of quantitative analyses. This data is vital for understanding the latest market pricing and liquidity information, and serves as a valuable input for price discovery, market microstructure studies, and alpha model development.

### NASADAQ ITCH (Market By Order) Data

NASDAQ's TotalView-ITCH is an advanced market data feed providing a comprehensive view of the order book in the NASDAQ exchange. It delivers detailed information about all visible limit orders, including the depth of the market with bids and asks at multiple price levels. The ITCH feed includes details about each individual order, such as the order's size, price, and the time of entry, modification, or cancellation. This granular data is essential for analyzing market dynamics, understanding the distribution of liquidity, and assessing the impact of individual orders on market movement. TotalView-ITCH is particularly valuable for traders and analysts involved in high-frequency trading, algorithmic trading strategies, and detailed market microstructure analysis. It offers a deeper insight into the supply and demand balance across different price levels, enabling more informed trading decisions and sophisticated market analysis.

#### Data Collection Methodology

The data collection process entails subscribing to the IEX DEEP and TOPS data feeds, setting up a robust data ingestion pipeline to handle the high-frequency data efficiently. The pipeline is designed to accommodate the high-throughput and low-latency requirements that are inherent to high-frequency trading environments, ensuring data integrity and facilitating easy data retrieval and analysis for subsequent model development and backtesting.

## 2.2 Data Cleaning and Preprocessing

This section focusses on the procurement, analysis, and parsing of equity data. It contains a set of Jupyter notebooks and a Python script, each tailored for specific tasks within the domain of financial data analysis.

### Stock Selection

We gathered Market By Order data for 10 stocks from the NASDAQ 100. we picked 5 stocks with the highest traded volume and 5 stocks with the lowest traded volume between the period of 1st of January 2019 and the 1st of December 2023. for the dates to test these stocks on we first picked 26 weeks of data from 1st June till 30th of November 2023. to this we added 5 days for each of the following criteria.

- highest and lowest intraday moves (high-low)
- The most positive moves and negative moves in a day ((close-open)/open) 
- highest and lowest volume days for the stock.

over the same period as our stock selection period (roughly 4 years)

### Trade Data

Trade data, often referred to as Time and Sales data, is a comprehensive record of all executed transactions in a financial market. This data includes vital details such as the time of each trade, the price at which it was executed, and the size of the trade. For a quantitative trader, this data is invaluable as it provides a granular view of market activity, helping to analyze the market's behavior and liquidity over time. By studying patterns and volumes in trade data, one can infer important market dynamics, such as the impact of large trades on price movements or the emergence of high-frequency trading patterns.

#### Visualisation

![Trade visualisation](images/trade_visualisation.png)

### Market By Order Data

Market By Order data is an advanced form of market data that provides detailed insights into the order book. Unlike aggregated Level 2 data, MBO offers a line-by-line view of each individual order on the book, including the order's unique identifier, price, size, and whether it is a buy or sell order. This granularity allows traders to understand the structure of the market in-depth, identifying the participation of individual market participants, and analyzing the order flow. MBO data is particularly useful in strategies that require precise understanding of order book dynamics, such as order book imbalance trading or detecting iceberg orders.

#### Labels

- `ts_recv`  
  The capture-server-received timestamp expressed as the number of nanoseconds since the UNIX epoch.

- `ts_event`  
  The matching-engine-received timestamp expressed as the number of nanoseconds since the UNIX epoch.

- `rtype`  
  The record type. Each schema corresponds with a single rtype value.

- `publisher_id`  
  The publisher ID assigned by Databento, which denotes dataset and venue.

- `instrument_id`  
  The numeric instrument ID.

- `action`  
  The event action. Can be [A]dd, [C]ancel, [M]odify, clea[R] book, [T]rade, or [F]ill.

- `side`  
  The order side. Can be [A]sk, [B]id or [N]one.

- `price`  
  The order price expressed as a signed integer where every 1 unit corresponds to 1e-9, i.e. 1/1,000,000,000 or 0.000000001.

- `size`  
  The order quantity.

- `channel_id`  
  The channel ID assigned by Databento as an incrementing integer starting at zero.

- `order_id`  
  The order ID assigned at the venue.

- `flags`  
  A combination of packet end with matching engine status.

- `ts_in_delta`  
  The matching-engine-sending timestamp expressed as the number of nanoseconds before ts_recv.

- `sequence`  
  The message sequence number assigned at the venue.

- `symbol`  
  The requested symbol for the instrument.

![Overview of the Various Timestamping points from DataBento](images/DataBentoTimestamping.png)

#### Visualisation

![MBO_full](images/mbo_snapshot_full.png)

![MBO_zoomed](images/mbo_snapshot_zoomedin.png)

![MBO_zoomed_further](images/mbo_snapshot_zoomed_full.png)

![latency chart](images/latencychart.png)

#### Note

- Every trade action has a corresponding fill and cancel message with the side on the trade message corresponding to the incoming order and the side on the fill message corresponding to the order that was taken out. An exception to this is when the trade has a [N]one type and this corresponds to other trades not visble on the orderbook like hidden orders.

### Orderbook Snapshoots Level 10 (MBP-10)

Level 10 Order Book data represents a snapshot of the top 10 levels of the buy and sell sides of the order book at a given moment. This data includes information about the price and size of the orders at each level, but does not identify individual orders or their owners. Level 10 data is essential for strategies that rely on understanding the depth of the market and the supply-demand dynamics at different price levels. Traders utilize this data to gauge market sentiment, identify potential support and resistance levels, and anticipate price movements based on the accumulation of orders around certain price points. This data is less granular than MBO, but it provides a quick overview of market depth, which is critical for many short-term and high-frequency trading strategies.

![MBP Chart](images/MBO_with_no_lim.png)

![MBP Chart with lim](images/MBO_with_lim.png)

### Data Parsing

We Parsed the Data into two specific formats message formats compatible with the TickTextReader of Strategy Studio:- `Depth Update by Order` and `Trades`.
Strategy Studio use two Timestamps :- SOURCETIME and COLLECTIONTIME. we also matched all other sections to their corresponding section in stratgy studio format refer to the python script `databento_parser.py` in `Resources/Data/Equity_and_Commodity` for an indepth explanation.

### Notebooks

1. **Databento API Usage (`databento.ipynb`)**
   - Purpose: Demonstrates the usage of the Databento API for data retrieval and analysis in a finance context. contains an indepth visualisation of the different data types available and an In-depth dive into the nuances of each type of data. 
   - Usage: Execute the notebook to see examples of Databento API calls and data manipulation. Adjust API queries to fetch different datasets.

2. **Date Choice Analysis (`date_choice.ipynb`)**
   - Purpose: Focuses on selecting and analyzing financial data to select the specific dates to test our strategy on, using daily candlestick data from Yahoo Finance.
   - Usage: Use this notebook to examine the methodology followed to select our date ranges for the strategy, also contains miscellaneous code that helps generate the current, front month and n month contract codes given a particular date for CL(Crude Oil Futures) traded on CME.

3. **Equity Notebook (`Equity_notebook.ipynb`)**
   - Purpose: Specializes in the analysis of equity data using the Databento parser.
   - Usage: Analyze various equity datasets by running the notebook. Can be customized for different equities or analysis techniques.

4. **Commodity Notebook (`Commodity_notebook.ipynb`)**
   - Note :- Parser Function needs slight modifcations and is still incomplete as we decided to use only equity data
   - Purpose: Analyzes commodity-related data, integrating with the Databento parser for data retrieval.
   - Usage: Open in Jupyter and run cells to process commodity data. Modify parameters or functions as needed for specific commodity analysis.

### Python Script

- **Databento Parser (`databento_parser.py`)**
  - Purpose: A Python class for parsing and handling data using the Databento API. It facilitates data retrieval and manipulation in pandas DataFrames.
  - Usage: Import this script into your Python projects to leverage Databento API's capabilities. Use its methods to set up data retrieval parameters and process the data as needed.

### Installation

Ensure you have Python 3.6 installed, as Databento is compatible with this version. Additionally, install these dependencies:

- Jupyter Notebook or Jupyter Lab
- Pandas
- Numpy
- Databento API package
- `yfinance` for Yahoo Finance data
- `dotenv` for managing environment variables


# 3. Strategy Development

## 3.1 Overview of Strategy Studio

Strategy Studio is a high performance C++ multi asset class strategy
development platform, designed for efficient implementation, testing, and deployment of
algorithmic trading strategies. Strategy Studio provides a Strategy Development API
which is agnostic to data feed and execution providers, allowing you to focus on the
logic of your strategies. Strategy Studio also includes a set of Strategy Servers, which
are responsible for loading and running your trading strategies, along with the Strategy
Manager, a performance monitoring and risk management user interface that you can
use to centralize your research and production trading.

## 3.2 Developing the Trading Algorithms

### **Market Making**

- **Description**: Engaging in continuous buying and selling of securities to provide liquidity to the market, profiting from the bid-ask spread.
- **Key Techniques**: Inventory management and short-term price movement predictions for quote adjustment.

### **Arbitrage Strategies**

- **Types**:
  - **Technological Arbitrage**: Leverages speed of sending and recieving market data to different market centers.
  - **Statistical Arbitrage**: Exploits statistical mispricings based on historical relationships.
  - **Triangular Arbitrage**: Targets price discrepancies among three currencies in the FX market.
  - **Merger Arbitrage**: Trades on stocks of merging companies.
- **Key Techniques**: Real-time data analysis, predictive models, and swift execution.

### **Momentum and Mean Reversion Strategies**

- **Momentum Trading**: Betting on the continuation of current market trends.
- **Mean Reversion**: Assuming prices will revert to their historical average.
- **Key Techniques**: Trend analysis, moving averages, and statistical models like Ornstein-Uhlenbeck.

### **Event-Driven Strategies**

- **Description**: Trading based on anticipated events like earnings reports or economic data releases.
- **Key Techniques**: Sentiment analysis, news parsing algorithms, and market psychology understanding.

### **Liquidity Detection**

- **Description**: Identifies trades based on the activities of large institutional traders.
- **Key Techniques**: Volume analysis, order book imbalance analysis, and iceberg order detection.

## 3.3 Our Strategies:

- [ExchangeArbitrage](#4-exchangearbitrage) - Technological arbitrage strategy, captures low latency discrepancies in price movement across exchanges.
- [MidPriceImbalanceStrategy](#5-midpriceimbalancestrategy) - Liquidity detection strategy, captures the effects of imbalance in the local orderbook upto 20 levels.
- [MomentumMarketMaking](#6-momentummarketmaking) - Market making strategy that dynamically shifts between mean reversion and momentum based on market regime.
- [RegressionMeanReversion](#7-regressionmeanreversion) - Mean reverting strategy with the mean being the line of best fit over n periods of data.
- [Long Short Term Memory](#8-lstm-strategy) - A strategy which uses a long short term memory model to use to forecast future price to take positions.
- [Transformer](#9-transformer-model-report) - A transformer like architecture that classifies the future magnitude of price change into buckets to help take positions.

## 3.4 Risk Management Protocols in High-Frequency Trading (HFT) Strategies

### Core Risk Management Protocols

1. **Position Limits**: Imposing limits on the size of positions to control exposure.
2. **Stop-Loss Orders**: Setting predetermined levels at which positions are automatically closed to limit losses.
3. **Real-Time Monitoring**: Continuously tracking positions and market conditions to identify potential risks.
4. **Leverage Limits**: Restricting the use of borrowed funds to manage risk exposure.
5. **Stress Testing**: Simulating various market scenarios to understand potential impacts on the trading strategy.
6. **Compliance Oversight**: Ensuring adherence to regulatory requirements and internal trading limits.
7. **Algorithm Supervision**: Monitoring algorithms for malfunction or unintended behaviors.

### Our Risk Management Techniques

- **Maximum Inventory Constraints**: Limiting the maximum inventory held at any time helps control exposure and reduces the impact of market fluctuations. It is effective in limiting market exposure and ensuring that the positions remain manageable under various market conditions. This approach is particularly important in HFT where large positions can be accumulated rapidly.
- **Inventory Reduction by Day-End**: Gradually decreasing maximum inventory levels to zero by the end of the trading day mitigates overnight market risk. This approach aligns with the principle of reducing open positions to avoid unexpected market moves that could occur outside of trading hours. It helps in avoiding overnight risks, which is crucial in HFT due to the potential for significant market moves during non-trading hours.

### Conclusion

Effective risk management in HFT involves a combination of real-time monitoring, strict controls on position sizes, leverage, and adherence to compliance protocols. Your approach of constraining maximum inventory and reducing it by day-end complements these principles and adds an extra layer of risk mitigation, particularly against overnight market volatility.

# 4 ExchangeArbitrage

## 4.1 Overview

We implement a low latency trading strategy that capitalizes on price discrepancies in financial instruments across different markets. The system is primarily focused on exploiting arbitrage opportunities in real-time between multiple market centers. We tested feasibility using NASDAQ and IEX as market centers. The strategy required data preparation and pattern recognition in tick data which will be explained in more detail in its respective subsection. All the files for this strategy can be found inside the ExchangeArbitrage directory on the main branch.

## 4.2 Data Preparation

The primary objectives of our data preparation process include:
<br>

**Consolidation of Data Sources**<br>
Our data consolidation focused on aggregating tick data from NASDAQ and IEX for AAPL and AMZN, spanning three days: June 21, 22, and 23, 2023. Adhering to Strategy Studio's requirements, we organized the data symbol and day-wise, resulting in a dataset with 9,016,737 ticks. We streamlined the process and ensured compatibility with Strategy Studio's sequencing.

**Ensuring Chronological Integrity**<br>
Maintaining chronological integrity to ensure a realistic representation of market conditions for each trading day was paramount during data preparation in order to represent the historical order book accurately for backtesting using Strategy Studio.

**Optimizing for Subsequent Processing**<br>
Using **generator** functions and employing the **heapq** library helped us handle large volumes of tick data from multiple stocks and market centers simultaneosuly. This choice was driven by the need to efficiently process and sort large datasets without loading every tick into memory.
<br>
The heapq library in Python provides an implementation of the heap queue algorithm, also known as the priority queue algorithm. Min Heaps are binary trees for which every parent node has a value less than or equal to any of its children. This property of heaps makes them ideal for efficiently finding and retrieving the smallest element, which is always at the root. 
<br><br>
Efficient Merging: Each tick data file acts as an input stream where ticks are read sequentially. We use time in a lexicographical manner to find the earliest tick update. This significantly reducing the computational overhead compared to traditional sorting algorithms.
<br><br>
Minimizing Memory Footprint: As we process large volumes of data, heapq helps in minimizing the memory footprint. By keeping only a fixed portion of the data in memory at any given time, we optimize memory usage, which is critical when dealing with large datasets like tick updates.
<br><br>
Fast Access to Data: The heap structure ensures that the smallest element (earliest tick) can be accessed and removed in O(log n) time, where n is the number of elements in the heap. This efficiency is demonstrated by our ability to process 9 million+ rows in about 70 seconds.

![Min & Max Heap](/images/min_and_max_heap.png)

## 4.3 Algorithm

In addition to using Strategy Studio's in-built functionality, we built the following classes to support our trading strategy.
<br><br>
**StrategyConfigGrid**<br>
This class allows us to dynamically change the config grid for our trading strategy. It plays a crucial role in our system, reading a CSV file to construct a grid of trading parameters.

These parameters include:

- Ticks: Helps to identify which row to return when grid is fetched inside strategy.
- Lots: Specifies number of lots to place in a trade.
- Stop: Specifies stop loss.
- Target: Target tick movement to book profit.


**QuoteTracker**<br>
This is an essential component of our system, tasked with tracking a rolling window of real-time bid and ask prices of subscribed symbols from both NASDAQ and IEX. It maintains a ScalarRollingWindow for each symbol and exchange, ensuring that we have a continuous and updated view of market Quotes. The tracker helps evaluate if these quotes satisfy predefined conditions to determine the optimal moment to execute a trade.

**OnDepth**<br>
This is the heart of the strategy. This is an inbuilt function hook which will be called whenever there is a depth update in our market data. We check our predefined conditions to place a trade.
We can see in the below image how there appears a latency and the lagging market center catches up.

- In the first and second observations, the bid and ask prices for both NASDAQ and IEX market centers are in equilibrium.- In the third observation, there is a notable increase in NASDAQ's bid price, which has risen by one tick.
- By the fourth observation, IEX's ask price has adjusted to match NASDAQ's, restoring the equilibrium between the two market centers.

<p align="center">
  <img src="/images/tick_movement.jpg" alt="Required sequence">
</p>

Since we have a rolling window size of 3, we have our strategy placing a trade whenever there is a pattern like above detected in the rolling windows.
<br><br>
We can also use the same rolling window to detect if equilibrium has been restored which would then trigger a callback to liquidate our trade. This has not been explored in this project, but could be explored by future projects on Exchange Arbitrage.

# 5 MidPriceImbalanceStrategy

This Strategy strategy the exploits the differences between the Volume-Weighted Mid-Price (VWMP) and the Simple Mid-Price, using a Z-score methodology to determine the trading threshold. This approach relies on statistical measures to identify significant deviations in price behavior, providing a foundation for timely and informed trading decisions.

## 5.1 Calculation of VWAP and Simple Mid-Price

#### Volume-Weighted Mid Price (VWMP)

- **Definition**: 
  The Volume-Weighted Mid Price (VWMP) is a financial measure that calculates the average mid-price of a security, weighted by the volume of bids and asks in the order book. It reflects an average price level considering the depth and volume of orders on both sides of the market, rather than executed trades.

- **Calculation**:
  - **Formula**: 
    \[ \text{VWMP} = \frac{\sum (\text{Bid Price} \times \text{Bid Volume}) + \sum (\text{Ask Price} \times \text{Ask Volume})}{\text{Total Bid Volume} + \text{Total Ask Volume}} \]
  - **Process**: 
    For each bid and ask in the order book, multiply the price by its respective volume. Sum these values for all bids and asks separately. Then, add these two sums together. Finally, divide this total by the sum of the total bid volume and the total ask volume on the book.


#### Simple Mid-Price

- **Definition**: The Simple Mid-Price is a straightforward measure that represents the midpoint between the best (highest) bid price and the best (lowest) ask price in the market at any given time.

- **Calculation**:
  - **Formula**:
    \[ \text{Simple Mid-Price} = \frac{\text{Best Bid} + \text{Best Ask}}{2} \]
  - **Process**: Simply take the average of the current highest bid price and the current lowest ask price in the order book.

These calculations are fundamental to our high-frequency trading strategy, as they provide key insights into market pricing and liquidity. The VWAP offers a volume-weighted perspective on the average trading price, while the Simple Mid-Price gives an immediate view of the current market equilibrium.

## 5.2 Statistical Approach: Z-Score Analysis

- **Z-Score Concept**: The Z-score is a statistical measure that describes a value's relationship to the mean of a group of values, measured in terms of standard deviations. In this strategy, we use it to identify how far and how unusual the divergence between VWMP and Simple Mid-Price is, relative to historical norms.

- **Calculating the Z-Score**:
  - We calculate the Z-score over a defined window of the differences between VWMP and Simple Mid-Price.
  - This rolling window approach captures recent market dynamics, ensuring our strategy is responsive to current market conditions.

## 5.3 Signal Generation Using Z-Score

1. **Determining the Threshold**:
   - We establish a Z-score threshold that indicates a significant divergence. Common thresholds might be a Z-score of ±1.5, ±2, or more, based on our desired sensitivity and risk tolerance.

2. **Buy Signal Criteria**:
   - We generate a buy signal when the Z-score of the VWMP-Simple Mid-Price difference is significantly high (e.g., above +1.5 or +2), indicating a potential upward price trend.

3. **Sell Signal Criteria**:
   - Conversely, we generate a sell signal when the Z-score is significantly low (e.g., below -1.5 or -2), suggesting a potential downward price trend.

## 5.4 Risk Management and Backtesting

1. **Risk Management**:
   - For managing Risk we imposed a constraint on the maximum lots `max_inventory` that can be held in any particular direction. we also gradually decrease this inventory in equal weighted sizes determined as a percentage of the initial maximum inventory `inventory_liquidation_decrement` every predetermined interval `inventory_liquidation_interval` from the market close.The start for this interval is automatically determined by calculating the number of steps it would take to reach zero and the time interval.

2. **Absolute Stop**:
   - When the Account reaches an absolute stop level percentage level  `absolute_loss` assumed as 20 percent we shut of the strategy and take no more trades.

## 5.5 Performance Metrics

### General Information

- **Unique ID**: 224020
- **Symbol**: XNAS.ITCH-AAPL
- **Instance Name**: MidPriceImbalanceStrategy0
- **Period**: November 30, 2023 to November 30, 2023
- **CSV Availability**: True

### Strategy Parameters

- **Debug Mode**: false
- **Absolute Stop**: 0.2
- **Max Inventory**: 100
- **Inventory Liquidation Decrement**: 10
- **Inventory Liquidation Interval**: 10
- **Levels**: 10
- **Interval (ms)**: 1
- **Smoothing Interval**: 200
- **Z_threshold**: 4

### Performance Statistics

#### PnL Metrics

- **Sharpe Ratio**: -0.9256286086579198
- **Sortino Ratio**: -0.5630905290744171
- **Max Drawdown**: -0.05487306136216982
- **Net PnL**: -54863.024505000096

#### Trading Metrics

- **Total Execution Cost**: 18221.314919
- **Total Trades**: 57562


#### Plots 

![Imbalance Main Plot](images/224020results_plot.png)

![Imbalance Main Plot](images/224020additional_results_plot.png)

#### Notes

  - refer to the [Parameter Optimization and Strategy Tuning](#10-parameter-optimization-and-strategy-tuning) for hyper parameter tuning and to [Results and Analysis Methodology](#11-results-and-analysis-methodology) .

# 6 MomentumMarketMaking

This section provides an in-depth description of a high-frequency trading (HFT) strategy based on the Avellaneda-Stoikov market-making model. The strategy balances mean-reverting and momentum-based positioning to optimize inventory levels and manage risk across different market conditions.

## 6.1 Strategy Description

### Mean-Reversion Positioning

This strategy is designed to take advantage of the mean-reverting properties of an asset's price. By calculating the reservation price, we adjust bid and ask quotes to maintain inventory levels around a mean value. The strategy requires calibration of the mean reversion cycle, which is empirically determined for optimal profitability.

### Momentum-Based Positioning

In trending markets, the strategy shifts to align with the prevailing trend. Inventory levels are adjusted based on a momentum indicator. This approach uses the distance of the mid-price from a long-term moving average (e.g., 200 EMA) to set inventory targets.

## 6.2 Algorithm

### Target Update

Based on whether the strategy is in a mean reversion position or momentum position the target is dynamicall adjusted at the interval of `time_interval`. same way the Regime and Volatility maps too are updated and new values are recieved prior to inventory calculation.

### Market Making

We constantly place bids and asks as decided by our formula below and 

### Inventory Control

The core of the inventory control mechanism relies on dynamic parameters to adapt to real-time market conditions. It uses the market mid-price, volatility, and remaining time to closing to optimize bid and ask quotes and manage inventory risk.

## 6.3 Parameters

- `position_size_`: Standard unit size of the positions taken in the market.
- `time_interval`: Frequency of order placement or adjustment in seconds.
- `short_window_size_`: Size of the window for short-term price analysis.
- `long_window_size_`: Size of the window for long-term price analysis.
- `vol_window_size_`: Window size for volatility estimation.
- `i_threshold_min/max`: Thresholds for switching between mean-reversion and momentum strategies.
- `max_inventory`: Maximum inventory level allowed.
- `inventory_liquidation_decrement/interval`: Parameters controlling the pace and scale of inventory liquidation.
- `inventory_risk_aversion`: A parameter that scales the risk aversion in the inventory control.
- `liquidity_scale`: A scaling parameter for the liquidity in the order book.

## 6.4 Formulas

### Reservation Price

The reservation price is the price at which the strategy aims to be indifferent to buying or selling an infinitesimal amount of the asset.

```plaintext
r(s, q, t) = s - qγσ²(T - t)
```

Where:

- `s`: current market mid-price
- `q`: current inventory level
- `γ`: inventory risk aversion parameter
- `σ`: market volatility estimate
- `T`: normalized closing time
- `t`: current time, normalized against closing time

### Bid-Ask Spread

The bid-ask spread is calculated to cover potential adverse selection risk and inventory risk.

```plaintext
δa + δb = γσ²(T - t) + 2/γ * ln(1 + γ/κ)
```

Where:

- `δa`, `δb`: half spread for ask and bid respectively
- `γ`: inventory risk aversion parameter
- `σ`: market volatility estimate
- `κ`: order book liquidity parameter

### Orderbook Liquidity

The Orderbook liquidity parameter is tied to the orderbook spread and is calculated by looking at what the current spread is and substituting it into the spread formula at the time of target updation. this helps us to use the current spread to dynamically adjust our calculated bid ask spread for our orders.

```plaintext
κ = γ/(exp((δ - γσ²) * γ * 0.5) - 1)
```

- `δ`: Current Actual Spread
- `γ`: inventory risk aversion parameter
- `σ`: market volatility estimate
- `κ`: order book liquidity parameter

## 6.5 Risk Management

1. Adjust `inventory_risk_aversion` to manage the risk associated with holding inventory.

2. **Position Management**:
   - For managing Risk we imposed a constraint on the maximum lots `max_inventory` that can be held in any particular direction. we also gradually decrease this inventory in equal weighted sizes determined as a percentage of the initial maximum inventory `inventory_liquidation_decrement` every predetermined interval `inventory_liquidation_interval` from the market close.The start for this interval is automatically determined by calculating the number of steps it would take to reach zero and the time interval.

3. **Absolute Stop**:
   - When the Account reaches an absolute stop level percentage level  `absolute_loss` assumed as 20 percent we shut of the strategy and take no more trades.

#### Note

- refer to the [Parameter Optimization and Strategy Tuning](#10-parameter-optimization-and-strategy-tuning) for hyper parameter tuning and to [Results and Analysis Methodology](#11-results-and-analysis-methodology).

# 7 RegressionMeanReversion

This strategy exploits mean-reverting behavior in asset prices, using a predictive line of best fit over a specified period to guide trade decisions. Unlike traditional threshold-based strategies, it initiates trades based on the divergence of current prices from those predicted by the line of best fit.

## 7.1 Concept and Calculation of the Line of Best Fit

#### Predictive Line of Best Fit

- **Definition**: The line of best fit, in this context, is a statistical tool used to predict future values based on past data trends. It represents a trend line through a scatter plot of data points (price data over 'n' periods).

- **Calculation**:
  - **Formula**: 
    \[ \text{Line of Best Fit} = \text{a} + \text{b} \times \text{Period} \]
    where 'a' is the y-intercept and 'b' is the slope of the line.
  - **Process**: Utilize regression analysis to determine the line that minimizes the sum of the squares of the distances of the points from the line.

This calculation is critical for our strategy, as it provides a predictive view of an asset's price, considering its historical performance.

## 7.2 Trade Initiation Based on Price Prediction

- **Trade Decision**:
  - **Long Position**: Initiated when the current ask price is below the predicted price by the line of best fit, indicating potential undervaluation. will be acted on when the slope is positive
  - **Short Position**: Executed when the current bid price exceeds the predicted price, suggesting overvaluation. will be acted on when the slope is negative.

## 7.3 Strategy Parameters

1. **Lookback Period ('n')**: Determines the number of past data points used for the line of best fit calculation.

## 7.4 Risk Management and Backtesting

1. **Position Management**:
   - For managing Risk we imposed a constraint on the maximum lots `max_inventory` that can be held in any particular direction. we also gradually decrease this inventory in equal weighted sizes determined as a percentage of the initial maximum inventory `inventory_liquidation_decrement` every predetermined interval `inventory_liquidation_interval` from the market close.The start for this interval is automatically determined by calculating the number of steps it would take to reach zero and the time interval.

2. **Absolute Stop**:
   - When the Account reaches an absolute stop level percentage level  `absolute_loss` assumed as 20 percent we shut of the strategy and take no more trades.

## 7.5 Performance Metrics 

### Example 
#### General Information

- **Unique ID**: 194850
- **Symbol**: XNAS.ITCH-AAPL
- **Instance Name**: RegressionMeanReversion
- **Period**: November 30, 2023 to November 30, 2023
- **CSV Availability**: True

#### Strategy Parameters

- **Absolute Stop**: 0.2
- **Strategy Active**: true
- **Debug Mode**: false
- **Max Inventory**: 100
- **Window Size**: 30
- **Previous Prediction**: 0
- **Inventory Liquidation Increment**: 10
- **Inventory Liquidation Interval**: 10
- **Bar Interval**: 1

#### Performance Statistics

##### PnL Metrics

- **Sharpe Ratio**: -0.4349290391983707
- **Sortino Ratio**: -0.3285766348435018
- **Max Drawdown**: -0.0015528403288935422
- **Net PnL**: -1527.491369000054

##### Trading Metrics

- **Total Execution Cost**: 488.281369
- **Total Trades**: 1004

#### Plots

![Regression Main Plot](images/194850results_plot.png)

![Regression Main Plot](images/194850additional_results_plot.png)

#### Note

- refer to the [Parameter Optimization and Strategy Tuning](#10-parameter-optimization-and-strategy-tuning) for hyper parameter tuning and to [Results and Analysis Methodology](#11-results-and-analysis-methodology).


# 8 LSTM Strategy

## 8.1 Overview

### The Project: Leveraging LSTM for Stock Movement Prediction

This strategy aims to harness the power of LSTM in predicting stock movements using tick-by-tick data over a span of three days. The fine granularity of tick-by-tick data provides a detailed view of stock activity, capturing every price change and trade execution, thereby offering a rich dataset for the LSTM model to learn from. The strategy involves training an LSTM model with relevant features – such as price changes, volume, and possibly order book dynamics – to predict future stock movements. This approach is particularly relevant in today's fast-paced trading environment, where high-frequency trading and algorithmic strategies dominate, and where even minor price movements can hold significant implications.

### Integration and Application in Real-world Trading

Post-training, the LSTM model will be exported into a C++ file, enabling integration with Strategy Studio for backtesting. This step is crucial, as it allows for the evaluation of the model's predictive power and robustness in a simulated trading environment, closely mimicking real-world conditions. Backtesting is an essential phase, providing insights into the model's effectiveness and helping fine-tune it before any actual deployment in live trading scenarios.

---

### Setup and Dependencies

In this, we utilize a range of Python libraries, each serving a specific purpose in the process of data handling, preprocessing, and model building. Below is a comprehensive list of these libraries and their respective roles:

- **gzip**: This module provides a simple interface to compress and decompress files using the GZIP file format. It is used for reading or writing data in a compressed format, potentially for efficient data storage and transfer.

- **pandas (as pd)**: Pandas is a fast, powerful, flexible, and easy-to-use open-source data analysis and manipulation tool. It is fundamental for data handling tasks such as data loading, cleaning, transformation, and exploration.

- **datetime (time)**: Imported from the datetime module, the time class represents time, independent of any particular day. This is used for handling time-related data within datasets or for timing operations in the code.

- **matplotlib.pyplot (as plt)**: This is a data visualization library in Python. It's instrumental for plotting graphs, charts, and other visual data representations.

- **itertools**:  This module provides a collection of tools for handling iterators, efficient looping and data manipulation tasks.

- **torch**: This is the main PyTorch library, a popular open-source machine learning library. It provides a wide range of functionalities for array operations, neural network construction, and training models.

- **torch.nn (as nn)**: A sub-module of PyTorch, nn provides a way to efficiently build and train neural networks with predefined layers, loss functions, etc.

- **numpy (as np)**: Numpy is fundamental for numerical operations in Python. It offers powerful n-dimensional array objects and a variety of tools for working with these arrays.

- **sklearn.preprocessing (MinMaxScaler)**: MinMaxScaler is typically used to scale features to a given range, often a necessity for neural network inputs.

- **time (as time_module)**: This is used for tracking the duration of certain operations in the code, such as model training or data processing.

---

## 8.2 Exploratory Data Analysis for LSTM Model

### Data Loading Functions

- `load_dataframe_from_csv` and `load_dataframe_from_gz`: 
  - **Purpose**: These functions are designed to load data into pandas DataFrames from CSV files and gzip-compressed files, respectively. 
  - **Benefit**: This allows handling both standard and compressed data formats efficiently.

### Data Filtering and Processing

- `filter_stock_data`:
  - **Functionality**: Filters the DataFrame to include data only for specified stock tickers, enabling targeted analysis of particular stocks.

- `calculate_price_and_indicators`:
  - A comprehensive function that adds various technical indicators to the DataFrame.
  - **Indicators Included**:
    - Order Flow Imbalance (OFI)
    - Moving Average Convergence Divergence (MACD)
    - Relative Strength Index (RSI)
    - Volume-Weighted Average Price (VWAP)
    - Volatility
    - Exponential Moving Average (EMA)
  - These indicators are crucial for analyzing market trends and making predictions.

- `filter_time_window`:
  - **Purpose**: Filters the data within a specified start and end time, useful for analyzing data in specific time frames.

### Analysis of Trading Activity

- `find_max_trading_hour`:
  - **Functionality**: Identifies the one-hour time window with the highest trading activity.
  - **Importance**: This analysis is pivotal in understanding market dynamics and peak trading periods.

### Visualization Functions

- `plot_histograms` and `plot_histograms_and_stats`:
  - **Features**: These functions visualize the distribution of different factors (like OFI, MACD, etc.) using histograms. 
  - **Enhancement**: The latter function provides basic statistics, enhancing the understanding of data distribution and trends.

### Challenges and Opportunities

- **Data Quality and Integrity**: Ensuring high data quality is essential, as the accuracy of the analysis depends on it.
- **Indicator Selection and Interpretation**: The choice of indicators and their interpretation plays a crucial role in stock market analysis. Understanding their implications in different market conditions is key.
- **Time-Sensitive Analysis**: The algorithm's ability to analyze specific time windows can uncover crucial insights into trading patterns.

### Histogram Analysis

#### Distribution of OFI (Order Flow Imbalance)

- **Observation**: Shows a relatively uniform distribution across the range, with some spikes.
- **Implication**: Suggests varied order flow with periods of both high buying and selling pressure.


#### Distribution of MACD (Moving Average Convergence Divergence)

- **Observation**: Centered around zero, with most data points within a narrow range.
- **Implication**: Indicates that the stock price did not deviate significantly from its trend.


#### Distribution of RSI (Relative Strength Index)

- **Observation**: Concentrated around the 50 mark, fewer occurrences of extreme values.
- **Implication**: Suggests the stock was not typically in an overbought or oversold condition.


#### Distribution of VWAP (Volume Weighted Average Price)

- **Observation**: Normal-like distribution centered around 151.
- **Implication**: Suggests a consistent average price weighted by volume.


#### Distribution of Volatility

- **Observation**: Lower volatility values are more common, with a rapid decrease in frequency as volatility increases.
- **Implication**: Indicates relative stability in stock price during the period analyzed.


#### Distribution of EMA (Exponential Moving Average)

- **Observation**: Exhibits a multi-modal distribution.
- **Implication**: Indicates several price levels around which the stock price tended to stabilize.


- ![Distribution plots](LSTM/images/histrogram.png "Distribution Image")


#### Distribution of BidpriceChange and AskpriceChange

- **BidPriceChange**: Represents changes in the bid price, indicating buying pressure.
- **AskPriceChange**: Represents changes in the ask price, indicating selling pressure.

- ![Distribution of BidpriceChange and AskpriceChange](LSTM/images/price_hist.png "Distribution Image 2")

### Interpretation and Implications

- **OFI**: A spike could indicate potential price movement due to an imbalance in buy and sell orders.
- **MACD**: Distribution near zero might suggest the absence of strong trends or reversion to the average.
- **RSI**: Concentration around 50 implies no extreme bullish or bearish momentum.
- **VWAP**: Provides insights into the fair value of the stock, considering volume.
- **Volatility**: Lower values favor certain trading strategies, like options.
- **EMA**: Highlights price levels for potential trade setups around these stabilizing points.

---

## 8.3 Data Processing

#### Data Loading and Aggregation

- **Files**: The script processes a three day trade data of `.gz` files containing updates from the stock market.
- **Concatenation**: It uses `pd.concat` to amalgamate data from multiple files into a singular DataFrame.

#### Mid Price Calculation

- **Calculation**: The `mid_price` is determined as the mean of `ASK_PRICE_1` and `BID_PRICE_1`.
- **Purpose**: This calculation aims to provide a straightforward estimate of the stock's current market price.

#### Filtering Specific Stock Data

- **Selection**: We filter the data specifically for the stock symbol "AAPL".
- **Cleaning**: It eliminates rows with missing values to maintain data integrity.

#### Time Series Processing

- **Conversion**: `COLLECTION_TIME` is converted into a datetime format.
- **Indexing**: The script sets `COLLECTION_TIME` as the DataFrame's index, facilitating time series analysis.

#### OHLC Data Aggregation

- **Resampling**: It generates OHLC data for varying time frames (1 second, 5 seconds, 1 minute) using `DataFrame.resample`.
- **Aggregation**: An aggregation dictionary is applied to summarize `mid_price` and volume data.
- **Column Renaming**: The script modifies column names for clarity, merging multi-level columns into a single level.

#### Technical Indicator Computation

##### Order Flow Imbalance (OFI)

- **Computation**: Calculated from the difference between total bid and ask volumes.
- **Usage**: This metric is indicative of the equilibrium between buying and selling pressure.

- ![OFI data plot](LSTM/images/ofi.png "OFI")

##### Relative Strength Index (RSI)

- **Calculation**: It determines overbought or oversold conditions based on price changes.
- **Window**: The calculation uses a 14-period rolling window.

##### Volatility

- **Computation**: Volatility is assessed via the rolling standard deviation of the percentage changes in `MID_PRICE_close`.
- **Window**: A 20-period window is used to gauge the intensity of price fluctuations.

- ![Volatility data plot](LSTM/images/Volatility.png "Volatility")

##### Moving Average Convergence Divergence (MACD)

- **Components**: This is calculated using fast and slow exponential moving averages and their divergence.
- **Signal Line**: The MACD includes a signal line, which is an EMA of the MACD itself.

- ![MACD data plot](LSTM/images/macd.png "MACD")

##### Exponential Moving Average (EMA)

- **Purpose**: The EMA is used to smooth out price data, aiding in identifying underlying trends over a chosen period.

#### Feature Engineering

##### Price Movement Features

- **Construction**: The is used to create features based on the changes in `MID_PRICE_close`.
- **Lagging**: It introduces a lagged feature (`Y2`) for predictive modeling purposes. (`Y2`) is the future stock movement (going up or down) that LSTM model is predicting.

- ![Y2 data plot](LSTM/images/Y2.png "Y2")

##### Data Cleaning

- **NaN Handling**: Rows with NaN values are removed to ensure the completeness of the dataset.

#### Data Scaling and Transformation

##### Adjustments for Specific Columns

- **Scaling Factors**: Specific columns (MACD, SIGNAL_LINE, Y2, OFI) are multiplied by a factor of 5.
- **Volatility and RSI Scaling**: The script adjusts `Volatility` (x 100) and `RSI` ([-5,5]) for better data representation.

### Data Segmentation for Model Training and Testing

#### Data Splitting

- **Objective**: Divides the dataset into training and testing subsets.
- **Methodology**: We can split the data based on a predefined ratio (e.g., 80% for training and 20% for testing).
- **Time Window Definition**: This sets distinct time windows for training (`train_start` to `train_end`) and testing (`test_start` to `test_end`).

#### `filter_time_window` Function

- **Purpose**: Filters the dataset based on a specified time window.
- **Implementation**: Converts 'COLLECTION_TIME' to `datetime` if necessary and applies a mask to select data within the given time range.

#### `get_train_test_windows` Function

- **Functionality**: Retrieves specific windows for training and testing datasets from the main DataFrame.
- **Error Handling**: Includes checks to ensure the specified time windows are within the dataset's overall time range and error handling for exceptions.

### Feature and Label Preparation

#### Feature and Label Selection

- **Selection of Features**: Relevant features for the model, such as `OFI`, `MACD`, `Volatility`.
- **Target Variable**: `Y2`.

#### Data Split into Features and Labels

- **Training Data**: Splits the training data into features (`train_features`) and labels (`train_labels`).
- **Testing Data**: Similarly, splits the testing data into features (`test_features`) and labels (`test_labels`).

### Conversion to Tensors and DataLoader Creation

#### Data Conversion

- **Purpose**: Converts data into tensors suitable for use with PyTorch.
- **Process**: Transforms both features and labels into tensors for the training and testing sets.

#### DataLoader Setup

- **Batch Size**: Sets a batch size for loading the data (e.g., 64).
- **Training DataLoader**: Creates a DataLoader for the training dataset, facilitating batch-wise processing.
- **Testing DataLoader**: Similarly, prepares a DataLoader for the testing dataset.

---

## 8.4 Algorithm - Long Short Term Memory model

In this section, we delve into the intricacies of the LSTM model architecture designed for predicting stock movements.

### Class Definition

- **Inheritance from `nn.Module`**: The `LSTMModel` class is derived from PyTorch's `nn.Module`. This provides essential functionalities like easy parameter management, forward pass definition, and compatibility with other PyTorch utilities.

### Constructor and Layer Definitions

1. **`__init__` Method**:
   - This constructor method is the heart of the model, setting up the layers and defining the model's architecture. It initializes the LSTM, batch normalization, ReLU activation, and fully connected layers with specified dimensions and functionalities.

2. **Layer Parameters**:
   - `input_dim`: This parameter specifies the number of input features, reflecting the dimensionality of the input data. In the context of stock data, it corresponds to the number of variables like price, volume, or other technical indicators used for each time step.
   - `hidden_dim`: The size of the hidden layers in the LSTM. A larger `hidden_dim` enables the model to capture more complex patterns in the data but increases computational complexity.
   - `output_dim`: Set to 1, indicating the model's task to predict a single value, such as the next price movement.
   - `num_layers`: The number of LSTM layers. Multiple layers can increase the model's ability to learn complex patterns but also make the model more prone to overfitting and increase training time.
   - `dropout`: A regularization technique to prevent overfitting. It randomly zeroes some of the elements of the input tensor with probability equal to the dropout rate during training.

3. **Layer Definitions**:
   - `nn.LSTM`: The LSTM layer captures temporal dependencies and sequences in the data. The `batch_first=True` parameter indicates that the first dimension of the input and output tensors will be the batch size.
   - `nn.BatchNorm1d`: Batch normalization standardizes the outputs of the LSTM layer, stabilizing and accelerating the training process.
   - `nn.ReLU`: The ReLU activation function introduces non-linearity, enabling the model to learn complex patterns beyond linear relationships.
   - `nn.Linear`: The fully connected layer that maps the output of the LSTM to the desired output dimension, providing the final prediction.

### Forward Pass Function

- **`forward` Method**:
   - This method defines the forward pass of the network. It sequentially passes the input through the LSTM layer, batch normalization, the ReLU activation, and finally through the fully connected layer to obtain the output.
   - The architecture of the forward pass is crucial as it dictates how the data flows through the model, influencing the model's ability to learn from the sequential stock data effectively.

### Rationale Behind Architecture Choices

- The choice of parameters and layers is driven by the need to capture the complex, time-dependent patterns inherent in stock price movements. 
- The multi-layer LSTM structure is chosen for its proficiency in modeling time-series data, essential for understanding sequential dependencies in stock prices.
- The balance between model complexity (number of layers, hidden dimensions) and the risk of overfitting is carefully considered, ensuring the model remains robust and generalizable to unseen data.

### Training the Model

1. **Hyperparameter Selection**:
   - `batch_size`: Set to 64, determining the number of samples propagated through the network before updating the model parameters.
   - `input_dim`: Set to 3, indicating the number of input features.
   - `hidden_dim`: Set to 20, defining the size of the LSTM hidden layers.
   - `output_dim`: Set to 1, as the model predicts a single value.
   - `num_layers`: Set to 5, indicating the number of stacked LSTM layers.
   - `learning_rate`: Set to 0.001, controlling the step size during gradient descent.
   - `num_epochs`: Set to 40, defining the number of complete passes through the training dataset.

2. **Model, Loss Function, Optimizer, and Scheduler**:
   - The LSTM model is initialized with the specified dimensions and layers.
   - Mean Squared Error Loss (`MSELoss`) is used as the criterion for training.
   - Adam optimizer is selected for optimizing the model, with a learning rate scheduler to adjust the learning rate over epochs.

3. **Training Process**:
   - For each epoch, the model enters the training mode, and a forward pass is conducted with subsequent loss calculation.
   - Backward pass and optimization steps are performed to update the model weights.

4. **Evaluation and Learning Rate Update**:
   - After each epoch, the model is set to evaluation mode, and the performance is evaluated on the test set.
   - The scheduler updates the learning rate based on the predefined schedule.

5. **Output**:
   - Training and testing losses are printed after each epoch to monitor the model's performance and convergence.

### Model Evaluation and Results

The model's performance was evaluated using a variety of metrics, including visual comparisons of predicted versus actual labels and a confusion matrix for classification accuracy.

- **Comparison of Actual and Predicted Values**: 
  - The first graph provides a broad comparison over the entire test set. It's evident that while the model captures the trend at times, there are discrepancies that could be addressed.
  - ![Overall Actual vs Predicted](LSTM/images/modelfinal_1.png)
  - The second graph focuses on a narrower sample range, where it can be seen that the predicted labels track the actual labels more closely, indicating a more precise prediction in this segment.
  - ![Focused Actual vs Predicted](LSTM/images/modelfinal_2.png)

- **Confusion Matrix and Accuracy**:
  - The confusion matrix is a critical tool to assess the model's performance in classification tasks. The matrix and the subsequent accuracy calculation suggest the model is performing at a level above random chance, but there is significant room for improvement.
  - The calculated accuracy of the model, around 50.94%, serves as a benchmark for future iterations and enhancements.

### C++ Integration

1. **Model Integration in Trading System**:
   - The will check if an LSTM model object for a specific financial instrument is already in the `nn_map_`. If not, it creates a new `PyTorchLSTMModel` object and inserts it into this map. This ensures that each instrument has its own model instance for prediction.

2. **Feature Extraction for Prediction**:
   - Three features are calculated: `feature1`, `feature2`, and `feature3`. These are features for our LSTM model: Order Flow Imbalance (`OFIx5`), Moving Average Convergence Divergence (`MACDx5`), and a measure of market volatility (`Volatilityx100`).

3. **Model Prediction**:
   - The features are fed into the LSTM model to predict the next-second price change of the stock. The prediction is a `torch::Tensor`, which is then converted into a double for our logical use.

4. **Decision Making Based on Prediction**:
   - The system uses a threshold (set at `0.15`) to determine trading actions. If the prediction is equal to or above this threshold, it indicates a potential upward price movement, triggering a buy order. Conversely, a prediction equal to or below `-0.15` suggests a downward movement, leading to a sell order.

5. **Order Execution**:
   - For a buy order, the size of the order is determined based on the ask size of the stock's top quote. For a sell order, it's based on the bid size. The `SendOrder` function is then called to execute the trade.

6. **Debugging and Monitoring**:
   - If the prediction does not meet the threshold criteria, no trade is executed. In debug mode, this is logged with a "No trade" message.

---

## 8.5 Parameter Optimisation

**Hyperparameter Tuning**

- `batch_size`:  determining the number of samples propagated through the network before updating the model parameters.
  - `input_dim`: indicating the number of input features.
  - `hidden_dim`: defining the size of the LSTM hidden layers.
  - `output_dim`: as the model predicts a single value.
  - `num_layers`:  indicating the number of stacked LSTM layers.
  - `learning_rate`: controlling the step size during gradient descent.
  - `num_epochs`: defining the number of complete passes through the training dataset.

<table>
  <tr>
    <td><img src="LSTM/saved_models/model_all_batch_norm_1.png" alt="Image 1" width="100"/></td>
    <td><img src="LSTM/saved_models/model_all_batch_norm_relu_1.png" alt="Image 2" width="100"/></td>
    <td><img src="LSTM/saved_models/model_sig_NOrsi_batch_norm_relu_1.png" alt="Image 3" width="100"/></td>
    <td><img src="LSTM/saved_models/model_NO_RSI_batch_norm_1.png" alt="Image 4" width="100"/></td>
    <td><img src="LSTM/saved_models/model_NO_RSI_batch_norm_relu_1.png" alt="Image 5" width="100"/></td>
    <td><img src="LSTM/saved_models/model_NO_RSI_MIDprice_batch_norm_relu_1.png" alt="Image 6" width="100"/></td>
    <td><img src="LSTM/saved_models/model_NO_RSI_Y2_batch_norm_relu_1.png" alt="Image 7" width="100"/></td>
  </tr>
  <tr>
    <td><img src="LSTM/saved_models/model_all_batch_norm_2.png" alt="Image 8" width="100"/></td>
    <td><img src="LSTM/saved_models/model_all_batch_norm_relu_2.png" alt="Image 9" width="100"/></td>
    <td><img src="LSTM/saved_models/model_sig_NOrsi_batch_norm_relu_2.png" alt="Image 10" width="100"/></td>
    <td><img src="LSTM/saved_models/model_NO_RSI_batch_norm_2.png" alt="Image 11" width="100"/></td>
    <td><img src="LSTM/saved_models/model_NO_RSI_batch_norm_relu_2.png" alt="Image 12" width="100"/></td>
    <td><img src="LSTM/saved_models/model_NO_RSI_MIDprice_batch_norm_relu_2.png" alt="Image 13" width="100"/></td>
    <td><img src="LSTM/saved_models/model_NO_RSI_Y2_batch_norm_relu_2.png" alt="Image 14" width="100"/></td>
  </tr>
</table>

---

## 8.6 Conclusion 

The key insights from the model evaluation indicate that while the model has learned to some extent from the data, its predictive power is limited. The results show that the model's effectiveness in accurately predicting stock price movements is near a binary classification threshold, which may not be sufficient for high-stakes financial decisions. This suggests the complexity of the market dynamics might require more sophisticated models or additional features.

---

## 8.7 Future Work
The C++ integration was not entirely successful. Future improvements can be made by using the saved models and current C++ code to make this entirely possible for backtesting and results.

To enhance the model's accuracy and reliability, future work could include:

- **Feature Engineering**: Exploring additional features that may have predictive power.
- **Hyperparameter Tuning**: Further tuning the model's hyperparameters could yield better results.
- **Model Complexity**: Testing more complex models or ensemble methods that could capture the nuances of the stock market more effectively.

Ongoing research and iterative testing are crucial to improving the model's performance in future developments.

# 9 Transformer Model Report

## 9.1 Introduction

The Transformer model, introduced in the paper "Attention Is All You Need" by Vaswani et al., has revolutionized the field of deep learning, particularly in natural language processing. Itsdistinctive architecture, which relies heavily on the self-attention mechanism, allows it to process sequences of data in parallel, providing significant improvements in terms of both performance and training speed. In the field of financial data analysis, particularly in tasks like instrument price forcasting, the transformer model can capture complex dependencies and relationships in the data, making it a valuable tool for accurate forcasting.

In our project, we set out to leverage the ability of Transformer model for forecasting the future movements of stock equities in high-frequency markets, with the ultimate goal of executing trades based on these predictions. However, our initial results did not meet our expectations when applied to our sample datasets. This can be attributed to the fact that micro-market fluctuations often lack sufficient temporal and model features, making it challenging for deep learning algorithms to provide accurate predictions. In essence, the rapid and complex dynamics of micro-market changes pose a unique set of challenges that require tailored solutions beyond what traditional deep learning models can offer.

## 9.2 The Core Idea behind the Transformer

### Overview

The core idea behind the Transformer model is to replace the recurrent layers typically used in sequence processing with attention mechanisms, specifically self-attention. This design choice enables the model to process entire sequences of data simultaneously, making it highly efficient and effective at capturing long-range dependencies. The archetecture diagram above illustrates the structure of the transformer using in the translation, where we have three self-attention blocks, where one  serves as encoder and two serves in the decoder structure. The encoder is crucial for the numerical representation of the input sentances and decoder is for the output sentences.
<img src="images/report/image_model.png" alt="Image Alt Text" width="600" height="500">

<img src="images/report/1702795051547.png" alt="Image Alt Text" width="450" height="90">

### Key Components

1. **Multi-Head Attention**: The Transformer employs a multi-head attention mechanism, which allows the model to jointly attend to information from different representation subspaces at different positions. This is crucial in understanding the varying relationships and dependencies in financial time-series data.
2. **Positional Encoding**: Since the Transformer lacks recurrence and convolution, it requires a way to incorporate the order of the sequence. Positional encodings are added to the input embeddings to give the model information about the position of each item in the sequence.
3. **Feed-Forward Networks**: Each layer of the Transformer contains a fully connected feed-forward network, which applies two linear transformations and a ReLU activation in between.
4. **Layer Normalization and Residual Connections**: Each sub-layer in the encoder and decoder (including attention layers and feed-forward networks) employs residual connections followed by layer normalization. This design helps in stabilizing the training of deep networks.

## 9.3 Self-Attention Mechanism

<img src="images/report/image.png" alt="Image Alt Text" width="700" height="400">


### Concept

Self-attention, sometimes called intra-attention, is an attention mechanism relating different positions of a single sequence. It allows each position in the input sequence to attend to all positions in the preceding layer, which makes it particularly useful for modeling sequential data like time series in our case.

### Working

1. **Query, Key, and Value Vectors**: The input to the attention layer three vectors: query (Q), key (K), and value (V). In the case of self-attention, these three vesctors are identical. They are transformed using a fully connected layer to project the features dimension into the size of chosen enbidding vector.
2. **Multi-Head**: This process involves dividing the vectors into multiple parts, or so called: heads. It allows the model to focus on different parts of the input sequence simultaneously. This process repeated multiple times with different, learned linear projections to the queries, keys, and values. The independent attention outputs are concatenated and linearly transformed into the expected dimensions after the attention scores computation.
3. **Scaled Dot-Product Attention**: The attention scores are computed by taking the dot product of the query with all keys, dividing each by square root of k to control the scale of the gradients and applying a softmax function to obtain the weights on the values.

## 9.4 Setup and Dependencies

In this project, we utilize a range of python libraries for data analysis and building deep learning model. In the backtesting period, the model will be exported a pth format and then exercised in the strategy studio.

### Libraries and Their Roles

* **`gzip`** : This module is used for reading and writing files in gzip-compressed format. In this code, it's likely used to read data from a compressed CSV file.
* **`matplotlib.pyplot` (as plt)** : This is a widely used data visualization library in Python. It's instrumental for plotting graphs, charts, and other visual data representations.
* **`numpy` (as np)** : Numpy is fundamental for numerical operations in Python. It offers powerful n-dimensional array objects and a variety of tools for working with these arrays.
* **`pandas` (as pd)** : Pandas is a fast, powerful, flexible, and easy-to-use open-source data analysis and manipulation tool. It is fundamental for data handling tasks such as data loading, cleaning, transformation, and exploration.
* **`scikit-learn`** : or sklearn for short, stands as a highly acclaimed open-source Python machine learning library. Its extensive repertoire of tools and features caters to a wide range of machine learning tasks. In the context of our project, we will make use of several submodules, notably `preprocessing` and `metrics` for feature scaling and accuracy evaluation, respectively. Furthermore, we will leverage linear models from the `linear_model` submodule and employ random forest models from the `ensemble` submodule to establish a performance benchmark against which we can evaluate the prediction accuracy of our Transformer model.
* **`talipp`** : This library provides various indicators and tools for analyzing financial time series data. Our code uses several indicators from this library to generate features for modeling financial data.
* **`torch`** : This is the main PyTorch library, a popular open-source machine learning library. It provides a wide range of functionalities for array operations, neural network construction, and training models.
* **`torch.nn` (as nn)** : A sub-module of PyTorch, nn provides a way to efficiently build and train neural networks with predefined layers, loss functions, etc.
* **`tqdm`** : tqdm is a library for adding progress bars to loops and other iterative tasks. It serves an crucial role in monitering the progress of model training. The `notebook` submodule is specifically designed for Jupyter Notebook environments. It's used here to visualize progress during training or iterations.

## 9.5 Data Preprocessing

Our objective is to utilize high-frequency AAPL data sourced from IEX, which includes information from all three layers of the order book. We amalgamate and preprocess this raw data to create second-based candlestick data for the first stock layer. This candlestick data comprises essential attributes such as the opening price, closing price, highest price, and lowest price of the mid-prices within each specified time interval. Additionally, it includes the total trading volume during that period, calculated as the sum of both bid and ask volumes from the first order book layer.

Furthermore, we enrich this dataset by incorporating various trading indicators, such as the Exponential Moving Average (EMA), Balance of Power (BOP),  and more, which are generated using the `talipp` library. Exponential Moving Average is a type of moving average that assigns greater weight to more recent data while less weight to older data points. In addition, balance of power is an indicator calculates the balance between buyers and sellers by considering the relationship between the closing price and the opening price for each period, computed the ratio of the difference between the close and open prices to the difference between the high and low prices. If BOP > 0, it indicates that buyers (bulls) were more dominant during the period. On the contrary, if BOP > 0, it suggests that sellers (bears) were more dominant during the period, and the closing price was closer to the low of the period.

Regarding the target labels for our predictive model, we formulate a classification task with three distinct trading labels. If the closing price of the next candlestick exceeds the current closing price by at least one standard deviation of the current candlestick's price, we assign a label of 1 to indicate an expected increase. Conversely, if the closing price is lower than the current price, we assign a label of -1 to signify an anticipated decrease. In all other cases, the label is set to zero, representing no significant expected change in price. This labeling scheme aligns with our trading strategy, where a label of 1 prompts a long position on the instrument, while a label of -1 triggers a short position.

The dataset is split into training and testing sets following a 4:1 ratio. A `MinMaxScaler` is fitted witht the input training features and then transform all the input featrues. Additionally, an `OneHotEncoder` is applied to labels. A sliding window approach is used to create input sequences for the model. We use 10 seconds as the sequential length of the input varialbel, each contains the attributaes and indicators of the data.

![1702797072658](images/report/1702797072658.png)

## 9.6 Transformer Model Archetecture

In our Transformer model architecture, we incorporate positional encodings, multi-head self-attention layers, and feedforward layers. Unlike the traditional Transformer model used in translation tasks, our model does not include a decoder component. This is because our model is designed for a different task: one-hot encoding trend prediction classification for the next second following the last second in the sequence.

Furthermore, the model's output consists of a sequence of one-hot encoding vectors which aligns with the input. If this is a translation task, we may need to convert those vectors into words which is a sentence. In our case, each vector represents the trend for the close price of the instrument. Therefore, we choose to extract the final vector from this sequence as the prediction result. The hyperparameters in our model include the number of layers, attention heads, dropout rate, and hidden dimensions. When compared to other deep learning models, the transformer requires less parameter tuning.

### Training Process

During the training process, we employ the use of the `DataLoader` to efficiently handle and load all the training data in mini-batches. This approach streamlines data management, as the DataLoader takes care of shuffling and randomization within each training loop, ultimately contributing to improved model generalization. We train our model using the `CrossEntropyLoss` loss function and optimize it using the Adam optimizer. Training occurs over multiple epochs, and we periodically evaluate the model's performance on the test set. To assess the model, we utilize accuracy as the evaluation metric, and we print the model's performance after each epoch.

In the early stages of training, we noticed that the model exhibited a bias towards predicting no change as the targets. To address this, we introduced class weights for the other two classes, giving them a higher penalty in the loss function. This adjustment was made to encourage the model to provide more balanced predictions across all classes.

## 9.7 Model Performance

When we examine the performance of a dummy classifier in a three-class classification scenario, we observe an accuracy level of around 0.33. Surprisingly, our transformer model manages to attain an accuracy of 0.54, outperforming both the linear classifier and random forest, both of which exhibit accuracies below 0.5. However, it's important to note that this result still doesn't meet our expectations. If the model only has a 50% chance of making the correct prediction, it may not be reliable enough to provide meaningful investment insights.


# 10. Parameter Optimization and Strategy Tuning

for [MidPriceImbalanceStrategy](#5-midpriceimbalancestrategy) , [MomentumMarketMaking](#6-momentummarketmaking) , [RegressionMeanReversion](#7-regressionmeanreversion)

The success of a strategy is critically dependent on its adaptability and optimization to varying market conditions. This section delves into the process of parameter optimization and strategy tuning, utilizing a sophisticated Python script designed for this purpose.

The provided script `Optimizer` is a comprehensive tool designed for optimizing trading strategies. It encompasses functionalities like data loading, parameter combination generation, backtesting, and evaluating trading strategies based on various financial metrics.

To run this optimizer provide comma separated values for each parameter in the `parameter_combinations.txt` file. the script will automatically generate combinations using the privided parameter values. The `optimisation_info.txt` file contains various import parameters of the strategy such as the strategy name , account value , stocks to test on , and the train cutoff data for the hyperparameter search.

The Optimizer splits up the data into train and test sets based on the `TRAIN_CUTOFF_DATE` for the stocks `TRAIN_STOCKS` set in the file `optimisation_info.txt` provided and runs on all the combinations created on all the dates up until the cutoff `gen_train_data` then once all CRA files have been generated we use `gen_train_results` to create the csv files and the corresponding plots and result jsons. The strategy then proceeds to find the best parameters based on your criteria :- `sharpe`, `sortino`, `Max Drawdown`  amd `net_pnl`. after generating these best parameters and saving them in a separate json it then moves on to validating these parameters by running the strategy on the remaining dates as the test dataset (`test_set_gen`,`gen_train_results`).

This technique has been applied to the strategies :- `MidPriceImbalanceStrategy0` , `MomentumMarketMaking` , `RegressionMeanReversion` and the files have been generalised to work on any strategy script (provided the generalised makefile and provision scripts too are modified for this optimizer - see the scripts in these strategies or refer to the respective folder READMEs)

## 10.1 Data Management and Initial Setup

The script begins by setting up the necessary environment, loading stock and date data, and initializing configuration files. This setup is crucial for aligning the backtesting environment with the specific requirements of the strategy being tested.

- `load_available_data()`: Loads stock and date data from a predefined dataset.
- `init_makefile()`: Initializes the makefile with parameters relevant to the current backtest.

## 10.2 Generating Parameter Combinations

One of the core features of this script is its ability to generate and test various combinations of strategy parameters.

- `generate_combinations()`: Creates a comprehensive list of parameter combinations to be tested.

## 10.3 Backtesting Workflow

The script conducts an extensive backtesting process, evaluating each parameter combination over a range of dates for selected stocks.

- `gen_train_data()`: Runs backtests for each combination of parameters and dates.
- `gen_train_results()`: Generates results for each backtest.

## 10.4 Optimization and Evaluation

Post backtesting, the script focuses on identifying the best performing parameter sets based on various metrics such as Sharpe Ratio, Sortino Ratio, Maximum Drawdown (MDD), and Net Profit and Loss (PNL).

- `find_best_parameters()`: Identifies the best parameters based on different performance metrics.
- `get_best_parameters()`: Retrieves the best parameters for further testing or implementation.

## 10.5 Testing and Validation

Finally, the script tests the optimized parameters on a separate validation dataset to ensure their robustness and effectiveness.

- `test_set_gen()`: Generates the test dataset using the best parameters.
- `test_result_gen()`: Generates results on the test dataset for final evaluation.

## 10.6 Problems and Limitations

This optimizer continuously runs till all combinations are exhausted. due to some reasons beyond our understanding the strategy instances fail to generate ,
we had an issue where the code would try to export the CRA files much before the backtest would end and that can be fixed by adding a long sleep time before we generate the CSV files. But we were unable to fix the case of the strategy server disconnecting and the Strategy Backtest instances failing to get generated. Due to this some combinations of the strategy fail to generate and throw errors hindering this process. Hence this section of code needs to be improved to robustly check if all functions are being performed and rerun the test till it succeeds. **For this reason we decided to present just one backtest result for now while we fix this mechanism in order to get the best parameters**

# 11. Results and Analysis Methodology

for [MidPriceImbalanceStrategy](#5-midpriceimbalancestrategy) , [MomentumMarketMaking](#6-momentummarketmaking) , [RegressionMeanReversion](#7-regressionmeanreversion)

This section outlines the methodology we employ to analyze the results of our high-frequency trading strategy. Our Python-based analytics framework plays a crucial role in dissecting and understanding the performance of our strategy. refer to the script `provision_scripts/result_analytics.py` which the following corresponds too.

## 11.1 Overview of the Analysis Process

The Python script provided serves as the backbone for our results analysis. It systematically processes the trading data, applying statistical methods to extract meaningful insights from our backtesting results.

## 11.2 Key Components of the Analysis

### Data Collection and Preparation

- **Utility Class**: A utility class, `Utils`, handles the loading and preprocessing of CSV files containing data on PnL, fills, and orders. It also manages the extraction and storage of backtest details, such as unique identifiers and strategy parameters.

- **CSV File Processing**: The script dynamically searches for and loads relevant CSV files based on unique identifiers, ensuring that the most current and relevant data is analyzed.

### Performance Metrics Calculation

- **PnL Analytics**: The `PnLAnalytics` class is central to evaluating the strategy's profitability. It computes various metrics, including cumulative PnL, account value over time, and percentage returns.

- **Risk-Adjusted Returns**: The script calculates key risk-adjusted performance metrics, such as the Sharpe and Sortino ratios, providing insights into the risk-reward profile of the strategy.

- **Drawdown Analysis**: Maximum drawdown is calculated, offering a view into the strategy's potential losses and the resilience required during downturns.

### Execution Analysis

- **Fill Analytics**: The `FillAnalytics` class assesses trade execution efficiency. It analyzes the total volume traded, execution costs, and average trade prices, offering a detailed view of the strategy's market impact.

### Visualization and Reporting

- **Graphical Representations**: The script generates plots to visually represent the account value, PnL, returns, and other key metrics over time, aiding in the intuitive understanding of the strategy's performance.

- **Result Storage**: All the analytical results, including statistical measures and plots, are systematically stored in group storage, facilitating easy access and review.

- **Access**: We use SCP (secure copy protocol) to shift all the json files containing the trade metrics and the plots from the storage in the VM to our local machine for visualisation and further indepth analysis for visualisation.

# 12. Conclusions and Future Directions

## 12.1 Summary of Key Findings

The exploration of high-frequency trading strategies using Strategy Studio yielded the following insights:

- **Importance of Data Quality**: High-quality, granular market data, especially from sources like IEX DEEP and TOPS, was crucial. The depth and accuracy of this data enabled more nuanced strategy development and backtesting.

- **Role of Advanced Quantitative Techniques**: The incorporation of machine learning models, including LSTM and Transformer architectures, revealed the potential and challenges of using advanced AI in stock price prediction. The complexity of financial markets requires sophisticated, well-tuned models to capture market dynamics effectively.

- **Performance of Various Strategies**: The strategies like MidPriceImbalanceStrategy, MomentumMarketMaking, and RegressionMeanReversion demonstrated varying degrees of success. Each strategy's performance was closely tied to its unique market approach and the underlying data it leveraged.

- **Parameter Optimization and Risk Management**: The importance of fine-tuning strategy parameters was evident. Effective risk management protocols, including position limits and inventory controls, were key to maintaining balance between profit maximization and risk mitigation.

- **Challenges in Model Integration**: Integrating deep learning models, particularly in a real-time, high-frequency trading context, posed significant challenges. The Transformer model, for instance, showed promise but also highlighted the difficulty in achieving reliable predictions due to the market's complexity.

## 12.2 Strategic Recommendations

Based on our findings, we reccomend the following for future projects:

- **Continuous Data Enhancement**: Prioritize high-quality, comprehensive data sources and continuously seek ways to enrich the data set. This could involve integrating alternative data sources or enhancing data processing techniques.

- **Model Development and Refinement**: Continue experimenting with and refining AI and machine learning models. Given the transformative potential of these technologies, dedicating resources to their development could yield substantial long-term benefits.

- **Dynamic Strategy Interactions**: Develop strategies capable of interacting with each other to adapt to changing market conditions. This might include more flexible algorithms or strategies that can switch between different trading styles based on market signals.

- **Risk Management Focus**: Strengthen risk management protocols, ensuring they are well-integrated with trading strategies. This includes setting stricter limits, improving real-time monitoring, and developing more sophisticated risk assessment tools.

- **Collaboration and Knowledge Sharing**: Foster a collaborative environment where knowledge and insights are shared across teams. This encourages innovation and helps in quickly identifying and addressing potential issues.

- **Training and Development**: Invest time in understanding the basics of Object Oriented Programming and financial markets to give you a better intuition of how the software works.

## 12.3 Future Research

Following are potential areas for future research:

- **Alternative Machine Learning Models**: Investigate other machine learning architectures and techniques that may offer better predictive accuracy in the context of HFT.

- **Deeper Market Microstructure Analysis**: Conduct more granular studies on market microstructure to understand the minute intricacies that drive price movements and liquidity patterns.

- **Expansion of exchange arbitrage strategy**: Detect patterns which will trigger booking profits from previously entered positions.

- **Cross-Market and Cross-Asset Strategies**: Expand research to include cross-market and cross-asset strategies that can exploit opportunities in different financial markets and instruments

# 13. References

1. **"Enhancing Trading Strategies with Order Book Signals" by [Álvaro Cartea, Ryan Donnelly, Sebastian Jaimungal](https://ora.ox.ac.uk/objects/uuid:006addde-3a03-4d75-89c1-04b59026e1c0/download_file?file_format=application%2Fpdf&safe_filename=Imbalance_AMF_resubmit.pdf) , [article](https://towardsdatascience.com/price-impact-of-order-book-imbalance-in-cryptocurrency-markets-bf39695246f6)**
   - This paper utilizes high-frequency Nasdaq data to create a volume imbalance measure in the limit order book (LOB). The measure is shown to predict the sign of upcoming market orders and immediate price changes. The authors introduce a Markov chain modulated pure jump model for price, spread, and order arrivals, and solve a stochastic control problem to optimize trading strategies. The inclusion of volume imbalance significantly enhances strategy profits.

2. **"High-frequency Trading in a Limit Order Book" by [Marco Avellaneda and Sasha Stoikov](https://math.nyu.edu/~avellane/HighFrequencyTrading.pdf) ,[article](https://medium.com/hummingbot/a-comprehensive-guide-to-avellaneda-stoikovs-market-making-strategy-102d64bf5df6)**
   - This paper explores the optimal strategies for placing bid and ask orders in high-frequency trading within a limit order book. It combines market microstructure theory with econophysics to model a dealer's role and associated risks. The paper is particularly relevant for high-frequency trading and market-making strategies.

3. **"Dealing with the Inventory Risk: A solution to the market making problem" by [Olivier Guéant, Charles-Albert Lehalle, Joaquin Fernandez-Tapia](https://arxiv.org/pdf/1105.3115.pdf) , [alternate paper](https://hal.science/hal-00628528/document)**
   - This paper addresses the complex optimization problem faced by market makers in setting bid and ask quotes. It extends the model introduced by Avellaneda and Stoikov by adding inventory constraints. The paper employs stochastic optimal control and Hamilton-Jacobi-Bellman equations to derive optimal quoting strategies. It also provides closed-form approximations of the optimal quotes. The paper is particularly relevant for understanding the dynamics of high-frequency market making and inventory risk management.

