# Transformer Model Report

# Introduction

The Transformer model, introduced in the paper "Attention Is All You Need" by Vaswani et al., has revolutionized the field of deep learning, particularly in natural language processing. Itsdistinctive architecture, which relies heavily on the self-attention mechanism, allows it to process sequences of data in parallel, providing significant improvements in terms of both performance and training speed. In the field of financial data analysis, particularly in tasks like instrument price forcasting, the transformer model can capture complex dependencies and relationships in the data, making it a valuable tool for accurate forcasting.

In our project, we set out to leverage the ability of Transformer model for forecasting the future movements of stock equities in high-frequency markets, with the ultimate goal of executing trades based on these predictions. However, our initial results did not meet our expectations when applied to our sample datasets. This can be attributed to the fact that micro-market fluctuations often lack sufficient temporal and model features, making it challenging for deep learning algorithms to provide accurate predictions. In essence, the rapid and complex dynamics of micro-market changes pose a unique set of challenges that require tailored solutions beyond what traditional deep learning models can offer.

## The Core Idea behind the Transformer

### Overview

The core idea behind the Transformer model is to replace the recurrent layers typically used in sequence processing with attention mechanisms, specifically self-attention. This design choice enables the model to process entire sequences of data simultaneously, making it highly efficient and effective at capturing long-range dependencies. The archetecture diagram above illustrates the structure of the transformer using in the translation, where we have three self-attention blocks, where one  serves as encoder and two serves in the decoder structure. The encoder is crucial for the numerical representation of the input sentances and decoder is for the output sentences.

<img src="image/report/1702766841078.png" alt="Image Alt Text" width="600" height="500">

<img src="image/report/1702795051547.png" alt="Image Alt Text" width="450" height="100">

### Key Components

1. **Multi-Head Attention**: The Transformer employs a multi-head attention mechanism, which allows the model to jointly attend to information from different representation subspaces at different positions. This is crucial in understanding the varying relationships and dependencies in financial time-series data.
2. **Positional Encoding**: Since the Transformer lacks recurrence and convolution, it requires a way to incorporate the order of the sequence. Positional encodings are added to the input embeddings to give the model information about the position of each item in the sequence.
3. **Feed-Forward Networks**: Each layer of the Transformer contains a fully connected feed-forward network, which applies two linear transformations and a ReLU activation in between.
4. **Layer Normalization and Residual Connections**: Each sub-layer in the encoder and decoder (including attention layers and feed-forward networks) employs residual connections followed by layer normalization. This design helps in stabilizing the training of deep networks.

## Self-Attention Mechanism
![Alt text](image.png)

<img src="image/report/1702753856820.png" alt="Image Alt Text" width="700" height="400">

#### Concept

Self-attention, sometimes called intra-attention, is an attention mechanism relating different positions of a single sequence. It allows each position in the input sequence to attend to all positions in the preceding layer, which makes it particularly useful for modeling sequential data like time series in our case.

#### Working

1. **Query, Key, and Value Vectors**: The input to the attention layer three vectors: query (Q), key (K), and value (V). In the case of self-attention, these three vesctors are identical. They are transformed using a fully connected layer to project the features dimension into the size of chosen enbidding vector.
2. **Multi-Head**: This process involves dividing the vectors into multiple parts, or so called: heads. It allows the model to focus on different parts of the input sequence simultaneously. This process repeated multiple times with different, learned linear projections to the queries, keys, and values. The independent attention outputs are concatenated and linearly transformed into the expected dimensions after the attention scores computation.
3. **Scaled Dot-Product Attention**: The attention scores are computed by taking the dot product of the query with all keys, dividing each by square root of k to control the scale of the gradients and applying a softmax function to obtain the weights on the values.

# Setup and Dependencies

In this project, we utilize a range of python libraries for data analysis and building deep learning model. In the backtesting period, the model will be exported a pth format and then exercised in the strategy studio.

## Libraries and Their Roles

* **`gzip`** : This module is used for reading and writing files in gzip-compressed format. In this code, it's likely used to read data from a compressed CSV file.
* **`matplotlib.pyplot` (as plt)** : This is a widely used data visualization library in Python. It's instrumental for plotting graphs, charts, and other visual data representations.
* **`numpy` (as np)** : Numpy is fundamental for numerical operations in Python. It offers powerful n-dimensional array objects and a variety of tools for working with these arrays.
* **`pandas` (as pd)** : Pandas is a fast, powerful, flexible, and easy-to-use open-source data analysis and manipulation tool. It is fundamental for data handling tasks such as data loading, cleaning, transformation, and exploration.
* **`scikit-learn`** : or sklearn for short, stands as a highly acclaimed open-source Python machine learning library. Its extensive repertoire of tools and features caters to a wide range of machine learning tasks. In the context of our project, we will make use of several submodules, notably `preprocessing` and `metrics` for feature scaling and accuracy evaluation, respectively. Furthermore, we will leverage linear models from the `linear_model` submodule and employ random forest models from the `ensemble` submodule to establish a performance benchmark against which we can evaluate the prediction accuracy of our Transformer model.
* **`talipp`** : This library provides various indicators and tools for analyzing financial time series data. Our code uses several indicators from this library to generate features for modeling financial data.
* **`torch`** : This is the main PyTorch library, a popular open-source machine learning library. It provides a wide range of functionalities for array operations, neural network construction, and training models.
* **`torch.nn` (as nn)** : A sub-module of PyTorch, nn provides a way to efficiently build and train neural networks with predefined layers, loss functions, etc.
* **`tqdm`** : tqdm is a library for adding progress bars to loops and other iterative tasks. It serves an crucial role in monitering the progress of model training. The `notebook` submodule is specifically designed for Jupyter Notebook environments. It's used here to visualize progress during training or iterations.

# Data Preprocessing

Our objective is to utilize high-frequency AAPL data sourced from IEX, which includes information from all three layers of the order book. We amalgamate and preprocess this raw data to create second-based candlestick data for the first stock layer. This candlestick data comprises essential attributes such as the opening price, closing price, highest price, and lowest price of the mid-prices within each specified time interval. Additionally, it includes the total trading volume during that period, calculated as the sum of both bid and ask volumes from the first order book layer.

Furthermore, we enrich this dataset by incorporating various trading indicators, such as the Exponential Moving Average (EMA), Balance of Power (BOP),  and more, which are generated using the `talipp` library. Exponential Moving Average is a type of moving average that assigns greater weight to more recent data while less weight to older data points. In addition, balance of power is an indicator calculates the balance between buyers and sellers by considering the relationship between the closing price and the opening price for each period, computed the ratio of the difference between the close and open prices to the difference between the high and low prices. If BOP > 0, it indicates that buyers (bulls) were more dominant during the period. On the contrary, if BOP > 0, it suggests that sellers (bears) were more dominant during the period, and the closing price was closer to the low of the period.

Regarding the target labels for our predictive model, we formulate a classification task with three distinct trading labels. If the closing price of the next candlestick exceeds the current closing price by at least one standard deviation of the current candlestick's price, we assign a label of 1 to indicate an expected increase. Conversely, if the closing price is lower than the current price, we assign a label of -1 to signify an anticipated decrease. In all other cases, the label is set to zero, representing no significant expected change in price. This labeling scheme aligns with our trading strategy, where a label of 1 prompts a long position on the instrument, while a label of -1 triggers a short position.

The dataset is split into training and testing sets following a 4:1 ratio. A `MinMaxScaler` is fitted witht the input training features and then transform all the input featrues. Additionally, an `OneHotEncoder` is applied to labels. A sliding window approach is used to create input sequences for the model. We use 10 seconds as the sequential length of the input varialbel, each contains the attributaes and indicators of the data.

![1702797072658](image/report/1702797072658.png)

# Transformer Model Archetecture

In our Transformer model architecture, we incorporate positional encodings, multi-head self-attention layers, and feedforward layers. Unlike the traditional Transformer model used in translation tasks, our model does not include a decoder component. This is because our model is designed for a different task: one-hot encoding trend prediction classification for the next second following the last second in the sequence.

Furthermore, the model's output consists of a sequence of one-hot encoding vectors which aligns with the input. If this is a translation task, we may need to convert those vectors into words which is a sentence. In our case, each vector represents the trend for the close price of the instrument. Therefore, we choose to extract the final vector from this sequence as the prediction result. The hyperparameters in our model include the number of layers, attention heads, dropout rate, and hidden dimensions. When compared to other deep learning models, the transformer requires less parameter tuning.

# Training Process

During the training process, we employ the use of the `DataLoader` to efficiently handle and load all the training data in mini-batches. This approach streamlines data management, as the DataLoader takes care of shuffling and randomization within each training loop, ultimately contributing to improved model generalization. We train our model using the `CrossEntropyLoss` loss function and optimize it using the Adam optimizer. Training occurs over multiple epochs, and we periodically evaluate the model's performance on the test set. To assess the model, we utilize accuracy as the evaluation metric, and we print the model's performance after each epoch.

In the early stages of training, we noticed that the model exhibited a bias towards predicting no change as the targets. To address this, we introduced class weights for the other two classes, giving them a higher penalty in the loss function. This adjustment was made to encourage the model to provide more balanced predictions across all classes.

# Model Performance

When we examine the performance of a dummy classifier in a three-class classification scenario, we observe an accuracy level of around 0.33. Surprisingly, our transformer model manages to attain an accuracy of 0.54, outperforming both the linear classifier and random forest, both of which exhibit accuracies below 0.5. However, it's important to note that this result still doesn't meet our expectations. If the model only has a 50% chance of making the correct prediction, it may not be reliable enough to provide meaningful investment insights.
