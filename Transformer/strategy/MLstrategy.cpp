/*================================================================================                               
*     Source: ../RCM/StrategyStudio/examples/strategies/SimpleMomentumStrategy/SimpleMomentumStrategy.cpp                                                        
*     Last Update: 2013/6/1 13:55:14                                                                            
*     Contents:                                     
*     Distribution:          
*                                                                                                                
*                                                                                                                
*     Copyright (c) RCM-X, 2011 - 2013.                                                  
*     All rights reserved.                                                                                       
*                                                                                                                
*     This software is part of Licensed material, which is the property of RCM-X ("Company"), 
*     and constitutes Confidential Information of the Company.                                                  
*     Unauthorized use, modification, duplication or distribution is strictly prohibited by Federal law.         
*     No title to or ownership of this software is hereby transferred.                                          
*                                                                                                                
*     The software is provided "as is", and in no event shall the Company or any of its affiliates or successors be liable for any 
*     damages, including any lost profits or other incidental or consequential damages relating to the use of this software.       
*     The Company makes no representations or warranties, express or implied, with regards to this software.                        
/*================================================================================*/   

#ifdef _WIN32
    #include "stdafx.h"
#endif

#include "MLstrategy.h"

#include "FillInfo.h"
#include "AllEventMsg.h"
#include "ExecutionTypes.h"
#include <Utilities/Cast.h>
#include <Utilities/utils.h>

#include <math.h>
#include <iostream>
#include <cassert>


#include <fdeep/fdeep.hpp>


using namespace RCM::StrategyStudio;
using namespace RCM::StrategyStudio::MarketModels;
using namespace RCM::StrategyStudio::Utilities;

using namespace std;

// This is the global model we will use to make predictions in OnTrade(). We exported this model from our Python notebook.
const auto model = fdeep::load_model("fdeep_model.json");

// Initialize the input_tensor with dimension 60 x 4, with all zeros.
// This will be fed into the model to make predictions. We cannot make a prediction until the entire input_tensor's 60 rows are filled.
// After the tensor gets filled initially, we will constantly update the last row with the incoming row, and remove the top row.
// The "removal" of the top row will be done by manually shifting up elements.
fdeep::tensor input_tensor(fdeep::tensor_shape(60, 4), 0.0);

// We will use this global counter to tell which row of the input_tensor we should populate next, until our input_tensor is completely full.
int row_to_fill = 0;

LSTMStrategy::LSTMStrategy(StrategyID strategyID, const std::string& strategyName, const std::string& groupName):
    Strategy(strategyID, strategyName, groupName)
{

}

LSTMStrategy::~LSTMStrategy()
{

}

void LSTMStrategy::OnResetStrategyState()
{

}

void LSTMStrategy::DefineStrategyParams()
{

}

void LSTMStrategy::DefineStrategyCommands()
{
    StrategyCommand command1(1, "Reprice Existing Orders");
    commands().AddCommand(command1);

    StrategyCommand command2(2, "Cancel All Orders");
    commands().AddCommand(command2);
}

void LSTMStrategy::RegisterForStrategyEvents(StrategyEventRegister* eventRegister, DateType currDate)
{    
    for (SymbolSetConstIter it = symbols_begin(); it != symbols_end(); ++it)
    {
        eventRegister->RegisterForBars(*it, BAR_TYPE_TIME, 10);
    }
}

void LSTMStrategy::OnTrade(const TradeDataEventMsg& msg)
{
    // For each trade, we need to get the price, size, and current bid and ask.
    // These 4 numbers will make up the newest row that we fill up in our input_tensor.
    // Then we remove top row from input_tensor. We do this manually by shifting up elements.
    // Then we use model.predict to get a prediction, and then we buy or sell.

    // The order of the columns for a single row should be price, size, bid, ask

    // Get trade price and size
    double trade_price = msg.trade().price();
    double trade_size = msg.trade().size();

    // Get the best bid and ask prices 
    double bid_price = msg.instrument().aggregate_order_book().BidPriceLevelAtLevel(1)->price();
    double ask_price = msg.instrument().aggregate_order_book().AskPriceLevelAtLevel(1)->price();

    // Should normalize all 4 values before putting them in

    if (row_to_fill < 60) {
        input_tensor.set(fdeep::tensor_pos(row_to_fill, 0), trade_price);
        input_tensor.set(fdeep::tensor_pos(row_to_fill, 0), trade_size);
        input_tensor.set(fdeep::tensor_pos(row_to_fill, 0), bid_price);
        input_tensor.set(fdeep::tensor_pos(row_to_fill, 0), ask_price);
        ++row_to_fill;
    } else {
        for (int row = 0; row < 59; ++row) {
            for (int col = 0; col < 4; ++col) {
                input_tensor.set(fdeep::tensor_pos(row, col), input_tensor.get(fdeep::tensor_pos(row + 1, col)));
            }
        }
        input_tensor.set(fdeep::tensor_pos(59, 0), trade_price);
        input_tensor.set(fdeep::tensor_pos(59, 0), trade_size);
        input_tensor.set(fdeep::tensor_pos(59, 0), bid_price);
        input_tensor.set(fdeep::tensor_pos(59, 0), ask_price);
    }

    // Only if our input tensor actually has 60 filled-up rows, can we start making predictions
    if (row_to_fill == 60) {
        // Now make a prediction with the updated input_tensor
        const auto result = model.predict({input_tensor});

        // Remember that we need to un-normalize the result

        // I'm not sure what the type of result is yet, so we'll probably have to figure that out and then convert it to a float
        std::cout << "Here is the result tensor: " << fdeep::show_tensors(result) << std::endl;
        std::cout << "Here is the shape of the result tensor: " << fdeep::show_tensor_shape(result.front().shape());

        // Send a buy or sell order depending on whether the output is higher or lower than the current price
        // if (result > ask_price) {
        //     // send buy order
        //     OrderParams params(
        //         msg.instrument(),
        //         10,
        //         ask_price,
        //         MARKET_CENTER_ID_NASDAQ,
        //         ORDER_SIDE_BUY,
        //         ORDER_TIF_DAY,
        //         ORDER_TYPE_LIMIT
        //     );
        //     trade_actions()->SendNewOrder(params);
        // } else if (result < bid_price) {
        //     // send sell order
        //     OrderParams params(
        //         msg.instrument(),
        //         10,
        //         bid_price,
        //         MARKET_CENTER_ID_NASDAQ,
        //         ORDER_SIDE_SELL,
        //         ORDER_TIF_DAY,
        //         ORDER_TYPE_LIMIT
        //     );
        //     trade_actions()->SendNewOrder(params);
        // }
    }

    // At this point we might also have to sell / buy the order that we sent 10 minutes ago...
    // Somehow we need to remember what trade we made at that time...
    // Maybe we should actually keep track of time with some C++ time library...


    std::cout << "-----------------------------------------------------" << std::endl;
}

// Helper function to send an order to buy or sell
// void LSTMstrategy::SendOrder() {

// }

// TODO: need to fix the data source to get these three callbacks working

void LSTMStrategy::OnTopQuote(const QuoteEventMsg& msg)
{
	// std::cout << "OnTopQuote(): (" << msg.adapter_time() << "): " << msg.instrument().symbol() << ": " <<
	// 	msg.instrument().top_quote().bid_size() << " @ $"<< msg.instrument().top_quote().bid() <<
	// 	msg.instrument().top_quote().ask_size() << " @ $"<< msg.instrument().top_quote().ask() <<
	// 	std::endl;

}

void LSTMStrategy::OnQuote(const QuoteEventMsg& msg)
{
	// std::cout << "OnQuote(): (" << msg.adapter_time() << "): " << msg.instrument().symbol() << ": " <<
	// 		msg.instrument().top_quote().bid_size() << " @ $"<< msg.instrument().top_quote().bid() <<
	// 		msg.instrument().top_quote().ask_size() << " @ $"<< msg.instrument().top_quote().ask() <<
	// 		std::endl;
}

void LSTMStrategy::OnDepth(const MarketDepthEventMsg& msg)
{
	// std::cout << "OnDepth(): (" << msg.adapter_time() << "): " << msg.instrument().symbol() << ": " <<
	// 			msg.instrument().top_quote().bid_size() << " @ $"<< msg.instrument().top_quote().bid() <<
	// 			msg.instrument().top_quote().ask_size() << " @ $"<< msg.instrument().top_quote().ask() <<
	// 			std::endl;
}

void LSTMStrategy::OnBar(const BarEventMsg& msg)
{

}

void LSTMStrategy::OnOrderUpdate(const OrderUpdateEventMsg& msg)
{    
	// std::cout << "OnOrderUpdate(): " << msg.update_time() << msg.name() << std::endl;
}

void LSTMStrategy::OnStrategyCommand(const StrategyCommandEventMsg& msg)
{
    switch (msg.command_id()) {
        case 1:
//            RepriceAll();
            break;
        case 2:
            trade_actions()->SendCancelAll();
            break;
        default:
            logger().LogToClient(LOGLEVEL_DEBUG, "Unknown strategy command received");
            break;
    }
}

void LSTMStrategy::OnParamChanged(StrategyParam& param)
{    

}
