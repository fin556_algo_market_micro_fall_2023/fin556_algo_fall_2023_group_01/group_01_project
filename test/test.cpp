// Refer to the header files for the explanation of the functions in detail
// In VSCode, hovering your mouse above the function renders the explanation of from the .h file as a pop up
#ifdef _WIN32
    #include "stdafx.h"
#endif

#include "test.h"

using namespace RCM::StrategyStudio;
using namespace RCM::StrategyStudio::MarketModels;
using namespace RCM::StrategyStudio::Utilities;

using namespace std;

// Constructor to initialize member variables of the class to their initial values.
test::test(StrategyID strategyID,
                    const string& strategyName,
                    const string& groupName):
    Strategy(strategyID, strategyName, groupName),
    debug_trades(false),
    debug_bars(true),
    debug_quotes(false),
    debug_order_updates(false),
    debug_scheduled_events(false),
    debug_d(false)
    {}
// Destructor for class
test::~test() {
}

void test::DefineStrategyParams() {
}

// By default, SS will register to trades/quotes/depth data for the instruments you have requested via command_line.
void test::RegisterForStrategyEvents(StrategyEventRegister* eventRegister, DateType currDate)
{    
    for (SymbolSetConstIter it = symbols_begin(); it != symbols_end(); ++it) {
        eventRegister->RegisterForBars(*it, BAR_TYPE_TIME, 3600);
    }

    eventRegister->RegisterForRecurringScheduledEvents("hourly ping", USEquityOpenUTCTime(currDate), USEquityCloseUTCTime(currDate), boost::posix_time::hours(1));
}

void test::OnTrade(const TradeDataEventMsg& msg) {
    const Trade* trade = &msg.trade();
    const Instrument* instrument = &msg.instrument();
    if (debug_trades){
    cout<< "Trade on " << instrument->symbol() << " at " << msg.event_time() << " at " << trade->price() << " for " << trade->size() << " at " << msg.side() << endl;
    cout << "bid is " << instrument->top_quote().bid() << " , ask is " << instrument->top_quote().ask() << endl;
    if (msg.IsConsolidated())
        cout << "Trade is from consolidated feed" << endl;
    else if (msg.IsDirect())
        cout << "Trade is from direct feed" << endl;
    else if (msg.IsFromDepth())
        cout << "Trade is from depth feed" << endl;
    //RecordAccountStats(instrument);
    }
}

void test::OnScheduledEvent(const ScheduledEventMsg& msg) {
    if (debug_scheduled_events)
        cout << "Scheduled event " << msg.scheduled_event_name() << " at " << msg.event_time() << endl;
}

void test::OnQuote(const QuoteEventMsg& msg) {
    if (debug_quotes){
    cout << msg.name() << endl;
    cout << "Quote on " << msg.instrument().symbol() << " at " << msg.event_time() << " bid " << msg.quote().bid() << " ask " << msg.quote().ask() << endl;
    cout << "bid size " << msg.quote().bid_size() << " ask size " << msg.quote().ask_size() << endl;
    cout << "Source time " << msg.source_time() << ", feed handler time " << msg.feed_handler_time() << " , Adapter_time" << endl;
    }
}

void test::OnDepth(const MarketDepthEventMsg& msg) {
    if (debug_d){
        cout << msg.name() << endl;
        cout << "Depth Update " << msg.instrument().symbol() << " at " << msg.event_time() <<  " at price " << msg.depth_update().price() << " for " << msg.depth_update().size() << " at " << msg.depth_update().side() << endl;
        cout << "Source time " << msg.source_time() << ", feed handler time " << msg.feed_handler_time() << " , Adapter_time" << endl;
    }
}

void test::OnOrderUpdate(const OrderUpdateEventMsg& msg) {
    if (debug_quotes)
        cout << "Hi" << endl;
    if (debug_order_updates){
        // Examples of other information that can be accessed through the msg object, but they are not used in this implementation.
        cout << "name = " << msg.name() << endl;
        cout << "order id = " << msg.order_id() << endl;
        cout << "fill occurred = " << msg.fill_occurred() << endl;
        cout << "update type = " << msg.update_type() << endl;

        // Update time of the order update event is printed to the console using std::cout.
        cout << "time " << msg.update_time() << endl;
    }
}

void test::OnBar(const BarEventMsg& msg) {

    // Uncomment below code to print bar statistics to the console
    const Bar& bar = msg.bar(); // Get the bar object from the BarEventMsg object
    const Instrument* instrument = &msg.instrument(); // Get the instrument object from the BarEventMsg object

    if ((bar.IsValid()) && (debug_bars)){ // Check if the bar is valid
        cout << msg.bar_time() << "bid:- " << msg.instrument().top_quote().bid() << ", ask:- " << msg.instrument().top_quote().ask() << endl;
        cout << msg.bar_time() << "high:- " << bar.high() << ", low:- " << bar.low() << ", open:- " << bar.open() << ", close:- " << bar.close() << endl;
        cout << msg.bar_time() << "volume:- " << bar.volume() << endl;
        cout << msg.bar_time() << "spread:- " << CalculateCurrentSpread(instrument) << endl;
    }
}

double test::CalculateCurrentSpread(const Instrument* instrument){
    double top_bid = instrument->top_quote().bid();
    double top_ask = instrument->top_quote().ask();
    double spread = abs(top_ask - top_bid);
    return spread;
}

void test::RecordAccountStats(const Instrument* instrument){
    cout << "Total profit " << portfolio().total_pnl(instrument) << endl;
    cout << "Unrealised profit :- " << portfolio().unrealized_pnl(instrument) << endl;
    cout << "Realised Profit :- " << portfolio().realized_pnl(instrument) << endl;
}

void test::OnMarketState(const MarketStateEventMsg& msg){
    // cout << msg.name() << endl;
    // cout << msg.event_time() << endl;
}

void test::OnResetStrategyState() {
}

void test::OnParamChanged(StrategyParam& param) {
}
