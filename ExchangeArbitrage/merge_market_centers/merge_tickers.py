import heapq
import os
import re
import shutil
import gzip
import tempfile

class MergeTickers:
    def file_line_generator(self, file_path):
        if file_path.endswith('.gz'):
            with gzip.open(file_path, 'rt', encoding='utf-8') as file:
                for line in file:
                    yield line.strip()

    def parse_time_from_line(self, line, element=0):
        return line.split(',')[element]

    def modify_line_with_sequence_number(self, line, sequence_number):
        parts = line.split(',')
        parts[2] = str(sequence_number)  # Update the third element
        return ','.join(parts)

    def write_buffer_to_file(self, buffer, file_path):
        os.makedirs(os.path.dirname(file_path), exist_ok=True)
        with open(file_path, 'a') as file:
            for line in buffer:
                file.write(line + '\n')

    def merge_files(self, stock_files_dict, output_directory, sequence_number=800000, buffer_size=10000):
        heaps = {stock: [] for stock in stock_files_dict}
        common_heap = []
        buffers = {stock: [] for stock in stock_files_dict}
        generators = {stock: [self.file_line_generator(path) for path in paths] for stock, paths in stock_files_dict.items()}

        for stock, gens in generators.items():
            for gen_index, gen in enumerate(gens):
                try:
                    line = next(gen)
                    heapq.heappush(common_heap, (self.parse_time_from_line(line), stock, gen_index, line))
                except StopIteration:
                    continue

        while common_heap:
            _, stock, gen_index, line = heapq.heappop(common_heap)
            line = self.modify_line_with_sequence_number(line, sequence_number)
            buffers[stock].append(line)
            sequence_number += 1

            try:
                next_line = next(generators[stock][gen_index])
                heapq.heappush(common_heap, (self.parse_time_from_line(next_line), stock, gen_index, next_line))
            except StopIteration:
                pass

            if len(buffers[stock]) >= buffer_size:
                output_file_path = os.path.join(output_directory, f"tick_{stock}.txt")
                self.write_buffer_to_file(buffers[stock], output_file_path)
                buffers[stock].clear()

        for stock, buffer in buffers.items():
            if buffer:
                output_file_path = os.path.join(output_directory, f"tick_{stock}.txt")
                self.write_buffer_to_file(buffer, output_file_path)

    def find_matching_files(self, stock_symbol, date, directory):
        matched_files = []
        regex = re.compile(f"tick_.*{stock_symbol}_.*{date}.txt")
        
        for file in os.listdir(directory):
            if regex.match(file):
                matched_files.append(os.path.join(directory, file))
        
        return matched_files

    def compress_to_gzip(self, src_file, gz_file):
        with open(src_file, 'rb') as f_in, gzip.open(gz_file, 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)
        os.remove(src_file)  # Remove the original file

    def store_as_text(self, src_file, txt_file):
        with open(src_file, 'rb') as f_in, open(txt_file, 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)

    def merge_files_for_stock_and_dates(self, stocks, dates, tick_data_directory, merged_data_directory):
        for date in dates:
            print(f"Processing date: {date}")
            stock_files_dict = {}
            for stock_symbol in stocks:
                gz_file_path = os.path.join(merged_data_directory, f"tick_{stock_symbol}.txt.gz")

                # Check if the final compressed file already exists
                if not os.path.exists(gz_file_path):
                    gz_files = self.find_matching_files(stock_symbol, date, tick_data_directory)
                    if gz_files:
                        stock_files_dict[stock_symbol] = gz_files
                else:
                    print(f"Merged tick data for {stock_symbol} on {date} already exists.")

            if stock_files_dict:
                self.merge_files(stock_files_dict, merged_data_directory)
                for stock_symbol in stock_files_dict.keys():
                    gz_file_path = os.path.join(merged_data_directory, f"tick_{stock_symbol}_{date}.txt.gz")
                    output_file_path = os.path.join(merged_data_directory, f"tick_{stock_symbol}.txt")
                    # self.compress_to_gzip(output_file_path, gz_file_path)
                    self.store_as_text(output_file_path, gz_file_path)
                    print(f"Merged tick data for {stock_symbol} on {date} is compressed and stored in {gz_file_path}")
            else:
                print(f"No new files to process for {date}")

if __name__ == '__main__':
    mergeTickers = MergeTickers()
    stock_symbol = ['AAPL', 'AMZN'] # supports single stock multiple market centers.
    dates = ['20230621', '20230622', '20230623']  # List of dates
    tick_data_directory = 'NasdaqData/TickData/ZipFiles'  # Directory where tick data for all market centers in Strategy Studio text_tick format is stored
    merged_data_directory = 'NasdaqData/TickData/MergedTickData'  # Directory to store merged data
    mergeTickers.merge_files_for_stock_and_dates(stock_symbol, dates, tick_data_directory, merged_data_directory)