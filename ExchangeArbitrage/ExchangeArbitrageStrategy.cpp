/*================================================================================
*     Source: ../RCM/StrategyStudio/examples/strategies/SimplePairsStrategy/SimplePairsStrategy.cpp
*     Last Update: 2021/04/15 13:55:14
*     Contents:
*     Distribution:
*
*
*     Copyright (c) RCM-X, 2011 - 2021.
*     All rights reserved.
*
*     This software is part of Licensed material, which is the property of RCM-X ("Company"),
*     and constitutes Confidential Information of the Company.
*     Unauthorized use, modification, duplication or distribution is strictly prohibited by Federal law.
*     No title to or ownership of this software is hereby transferred.
*
*     The software is provided "as is", and in no event shall the Company or any of its affiliates or successors be liable for any
*     damages, including any lost profits or other incidental or consequential damages relating to the use of this software.
*     The Company makes no representations or warranties, express or implied, with regards to this software.
/*================================================================================*/

#ifdef _WIN32
    #include "stdafx.h"
#endif

#include "ExchangeArbitrageStrategy.h"

#include "FillInfo.h"
#include "AllEventMsg.h"
#include "ExecutionTypes.h"
#include <Utilities/Cast.h>
#include <Utilities/utils.h>

#include <math.h>
#include <iostream>
#include <cassert>

using namespace RCM::StrategyStudio;
using namespace RCM::StrategyStudio::MarketModels;
using namespace RCM::StrategyStudio::Utilities;

using namespace std;

ExchangeArbitrage::ExchangeArbitrage(StrategyID strategyID, const std::string& strategyName, const std::string& groupName):
    Strategy(strategyID, strategyName, groupName),
    bars_(),
    config_path_("/home/vagrant/ss/sdk/RCM/StrategyStudio/examples/strategies/group_01_project/ExchangeArbitrage/config.csv"),
    instrument_x_(nullptr),
    instrument_y_(nullptr),
    quote_tracker_(3),
    z_score_threshold_(2.5),
    position_size_(10000),
    target_position_(0),
    debug_(true)
{
    //this->set_enabled_pre_open_data_flag(true);
    //this->set_enabled_pre_open_trade_flag(true);
    //this->set_enabled_post_close_data_flag(true);
    //this->set_enabled_post_close_trade_flag(true);
}

ExchangeArbitrage::~ExchangeArbitrage()
{
}

void ExchangeArbitrage::OnResetStrategyState()
{
    target_position_ = 0;
    // rolling_window_.clear();
    bars_.clear();
}

void ExchangeArbitrage::DefineStrategyParams()
{
    params().CreateParam(CreateStrategyParamArgs("config_path", STRATEGY_PARAM_TYPE_RUNTIME, VALUE_TYPE_STRING, config_path_));
    params().CreateParam(CreateStrategyParamArgs("z_score", STRATEGY_PARAM_TYPE_RUNTIME, VALUE_TYPE_DOUBLE, z_score_threshold_));
    params().CreateParam(CreateStrategyParamArgs("position_size", STRATEGY_PARAM_TYPE_RUNTIME, VALUE_TYPE_INT, position_size_));
    params().CreateParam(CreateStrategyParamArgs("debug", STRATEGY_PARAM_TYPE_RUNTIME, VALUE_TYPE_BOOL, debug_));
}

void ExchangeArbitrage::DefineStrategyGraphs()
{
    graphs().series().add("Mean");
    graphs().series().add("ZScore");
}

void ExchangeArbitrage::RegisterForStrategyEvents(StrategyEventRegister* eventRegister, DateType currDate)
{    

    config_.set_logger(&logger());
    if (!config_.read_config(config_path_)) {
        if (debug_) {
        std::stringstream ss;
        ss << "Config file " << config_path_ << " is not valid." ;
        logger().LogToClient(LOGLEVEL_DEBUG, ss.str());
        }
    }
    else {
        cout << "Config file is valid." << std::endl;
        logger().LogFormatted(LOGDEBUG, "Config file %s is valid.", config_path_.c_str());
    }

    if (debug_) {
        std::stringstream ss;
        ss << "Config file " << config_path_ << " is not valid." ;
        logger().LogToClient(LOGLEVEL_DEBUG, ss.str());

        StrategyConfigRow configRow = config_.at(0.02);
        std::stringstream rowSS;
        rowSS << "Config for change " << 0.02 << ": "
              << "Ticks: " << configRow.ticks << ", "
              << "Lots: " << configRow.lots << ", "
              << "Stop: " << configRow.stop << ", "
              << "Target: " << configRow.target << std::endl;
        logger().LogToClient(LOGLEVEL_DEBUG, rowSS.str());
        // std::cout << rowSS.str();
    }
    

    int count = 0;

    for (SymbolSetConstIter it = symbols_begin(); it != symbols_end(); ++it) {
        EventInstrumentPair event_instrument_pair = eventRegister->RegisterForBars(*it, BAR_TYPE_TIME, 10);    
        
        if (count == 0) {
            instrument_x_ = event_instrument_pair.second;
        } else if (count == 1) {
            instrument_y_ = event_instrument_pair.second;
        }
    
        ++count;
        cout << "Registering for bars on " << *it << endl;
        cout << count << endl;
    }
}

void ExchangeArbitrage::OnTrade(const TradeDataEventMsg& msg){
}

void ExchangeArbitrage::OnDepth(const MarketDepthEventMsg& msg) {
    static int counter = 0;
    static bool activeTrade = false;
    if (debug_) {
        std::stringstream ss;
        const Instrument* instrument = &msg.instrument();
        MarketModels::DepthUpdateType depth_update_type = msg.depth_update_type();
        const MarketModels::DepthUpdate* depth_update = &msg.depth_update();
        MarketModels::MarketCenterID mc_id = depth_update->market_center();
        std::string market_center_str = MarketCenterToString(mc_id);
        TimeType adapterTime = msg.adapter_time();
        std::string timeStr = boost::posix_time::to_simple_string(adapterTime);
        std::string name = msg.name();
        
        ss << "Instrument Symbol: " << instrument->symbol() << std::endl
            << "Market Center ID: " << mc_id << std::endl
            << "Market Center: "  << market_center_str << std::endl
            << "Adapter Time: "  << timeStr << std::endl
            << "Quote Feed Type: " << depth_update << std::endl
            << "Name: " << name << std::endl;

        if (msg.updated_top_quote()) {
            const Quote quote = instrument->top_quote();
            std::string symbol = instrument->symbol();
            double min_tick_size = instrument->min_tick_size();
            std::string market_center_str = MarketCenterToString(depth_update->market_center());

            // Update QuoteTracker for both bid and ask prices for the specific market center
            quote_tracker_.UpdateQuote(symbol, market_center_str, "ask", quote.ask());
            quote_tracker_.UpdateQuote(symbol, market_center_str, "bid", quote.bid());

            // Trading logic: Checking for matching quotes across NASDAQ and IEX
            if (quote_tracker_.IsWindowFull(symbol, "NASDAQ", "ask") && quote_tracker_.IsWindowFull(symbol, "IEX", "ask")) {
                const double NfirstAsk = quote_tracker_.GetRollingWindowElement(symbol, "NASDAQ", "ask", 0);
                const double NsecondAsk = quote_tracker_.GetRollingWindowElement(symbol, "NASDAQ", "ask", 1);
                const double NthirdAsk = quote_tracker_.GetRollingWindowElement(symbol, "NASDAQ", "ask", 2);
                const double IfirstAsk = quote_tracker_.GetRollingWindowElement(symbol, "IEX", "ask", 0);
                const double IsecondAsk = quote_tracker_.GetRollingWindowElement(symbol, "IEX", "ask", 1);
                const double IthirdAsk = quote_tracker_.GetRollingWindowElement(symbol, "IEX", "ask", 2);

                const double NfirstBid = quote_tracker_.GetRollingWindowElement(symbol, "NASDAQ", "bid", 0);
                const double NsecondBid = quote_tracker_.GetRollingWindowElement(symbol, "NASDAQ", "bid", 1);
                const double NthirdBid = quote_tracker_.GetRollingWindowElement(symbol, "NASDAQ", "bid", 2);
                const double IfirstBid = quote_tracker_.GetRollingWindowElement(symbol, "IEX", "bid", 0);
                const double IsecondBid = quote_tracker_.GetRollingWindowElement(symbol, "IEX", "bid", 1);
                const double IthirdBid = quote_tracker_.GetRollingWindowElement(symbol, "IEX", "bid", 2);

                if (!activeTrade) {
                    if ((NfirstBid - NsecondBid) >= 0.01) {
                        if ((NsecondBid == IsecondBid) && (NsecondBid == IfirstBid)) {  
                            counter++;
                            if (counter < 2) {
                                cout << counter << endl;
                                cout << "Placing Trade" << symbol << "Counter: " << counter << std::endl;
                                cout << "NASDAQ Ask Window: [" << NfirstAsk << ", " << NsecondAsk << ", " << NthirdAsk << "]" << endl;
                                cout << "IEX Ask Window:     [" << IfirstAsk << ", " << IsecondAsk << ", " << IthirdAsk << "]" << endl;
                                cout << "NASDAQ Bid Window: [" << NfirstBid << ", " << NsecondBid << ", " << NthirdBid << "]" << endl;
                                cout << "IEX Bid Window:     [" << IfirstBid << ", " << IsecondBid << ", " << IthirdBid << "]" << endl;

                                // activeTrade = true;
                                // Place X % volume on the book at the ask on IEX
                                StrategyConfigRow configRow = config_.at(0.01);
                                cout << "Config Row: " << configRow.ticks << ", " << configRow.lots << ", " << configRow.stop << ", " << configRow.target << endl;
                                OrderID orderID = SendOrder(instrument, configRow.lots, IfirstAsk, "IEX");
                                cout << "OrderID: " << orderID << endl;
                            }
                        
                        }
                    }
                }
                // else
                // if (activeTrade) {
                //     if ((NsecondBid - NthirdBid) == 0.01) {
                //         if(((IfirstBid - IsecondBid) == 0.01)) {                            
                //             if ((NthirdBid == IthirdBid) && (NthirdBid == IsecondBid)) {
                //                 if (NfirstBid == IfirstBid) {
                //                     // close trade
                //                     counter++;
                //                 }
                //             }
                //         }
                //     }

                // }

            }


                // if ((IfirstBidDiff - ))


                // if (nasdaqCurrentAskDiff >= min_tick_size * 1) {
                //     if (nasdaqPreviousAsk == iexCurrentAsk && nasdaqPreviousAsk == iexPreviousAsk) {
                //         // This condition means that there was a discrepancy. We should place an order on IEX
                //         // The price should be the current best ask on IEX, a percentage of volume of the top ask quantity on IEX
                //         // Returned OrderID should be stored in a map with the OrderID as the key
                //         // There should be a flag which is set to true when the order is placed on IEX
                //         // If that flag is true
                //         // The checkk can also be if all present asks are equal but previous asks have min_tick diff
                //         // which was half our initial condition to place the trade, then
                //         // The check should be then if all 4 are equal, then we close our position using the OrderID

                //         // N_P_B == I_P_B && N_C_B == (I_C_B + 0.01) ---- 0.01 is min_tick_size
                //         // This means we place the trade
                //         // Store the OrderID in a map with the OrderID as the key and position and price as the value
                //         // This trade should trigger a bool flag to True
                //         // We store the price in a rolling window dedicated to store the traded price

                //         // If the flag is true,
                //             // Check for the following conditions. (N_P_B - I_P_B == 0.01) && (N_C_B == I_C_B) && (I_C_B - I_P_B == 0.01)
                //                 // If it has come till this point, then it means that we have had a trade and now there is no more diff
                //                 // We have to liquidate all positions.
                //         // A 3rd sequemce will get added to the scalarrolling window?
                //         // Or we maintain a seperate class to keep track of orders and their corresposning prices
                //         // Whenever 


                //         counter++;
                //         if (counter < 100) {
                //             std::cout << "Ask - IEX - " << symbol << " - nasdaqAskDiff: " << nasdaqCurrentAskDiff << std::endl;
                //             cout << "First Element: " << firstElement << std::endl;
                //             cout << "Second Element: " << secondElement << std::endl;
                //             cout << "Third Element: " << thirdElement << std::endl;

                //         }
                //         // Place 10% volume on the book at the ask on IEX
                //         // OnTrade
                //         // OrderID orderID = SendOrder(msg.instrument(), 1, iexCurrentAsk, "IEX");
                //     }
                // } else if (iexAskDiff >= min_tick_size * 1) {
                //     if (nasdaqPreviousAsk == iexCurrentAsk && nasdaqPreviousAsk == iexPreviousAsk) {
                        
                // }
                //     if (nasdaqCurrentAskDiff == 0 && iexAskDiff == 0) {
                //             if (nasdaqPreviousAsk ){
                //         }
                //     }
                // }
        }
    }
}

void ExchangeArbitrage::OnBar(const BarEventMsg& msg)
{
}

void ExchangeArbitrage::AdjustPortfolio()
{
}

OrderID ExchangeArbitrage::SendOrder(const Instrument* instrument, int trade_size, double price, const std::string& market_center_str)
{
    MarketCenterID market_center_id;

    if (market_center_str == "NASDAQ") {
        market_center_id = MARKET_CENTER_ID_NASDAQ;
    } else if (market_center_str == "IEX") {
        market_center_id = MARKET_CENTER_ID_IEX;
    }
    return SendLimitOrder(instrument, trade_size, price, market_center_id);
}

OrderID ExchangeArbitrage::SendLimitOrder(const Instrument* instrument, int trade_size, double price, MarketCenterID market_center_id)
{
    OrderParams params(*instrument,
                       abs(trade_size),
                       price,
                       market_center_id,
                       (trade_size > 0) ? ORDER_SIDE_BUY : ORDER_SIDE_SELL,
                       ORDER_TIF_DAY,
                       ORDER_TYPE_LIMIT);
    
    // params.custom_params.emplace_back(std::make_pair(116, on_behalf_of_sub_id_));
    
    TradeActionResult result = trade_actions()->SendNewOrder(params);
    if (result == TRADE_ACTION_RESULT_SUCCESSFUL) {
        cout << "Order sent successfully" << endl;
        return params.order_id;
    } else {
        cout << "Order send failed" << endl;
        return 0;
    }
}

void ExchangeArbitrage::OnMarketState(const MarketStateEventMsg& msg)
{
}

void ExchangeArbitrage::OnOrderUpdate(const OrderUpdateEventMsg& msg)  
{
}

void ExchangeArbitrage::OnAppStateChange(const AppStateEventMsg& msg)
{
}

void ExchangeArbitrage::OnParamChanged(StrategyParam& param)
{    
    if (param.param_name() == "z_score") {
        if (!param.Get(&z_score_threshold_))
            throw StrategyStudioException("Could not get z_score");
    } else if (param.param_name() == "position_size") {
        if (!param.Get(&position_size_))
            throw StrategyStudioException("Could not get position_size");
    } else if (param.param_name() == "debug") {
        if (!param.Get(&debug_))
            throw StrategyStudioException("Could not get debug");
    }        
}

static inline std::string& trim(std::string& s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
        return !std::isspace(ch);
    }));
    s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
        return !std::isspace(ch);
    }).base(), s.end());
    return s;
}

static bool GetLineWithTrim(std::ifstream& ifs, std::string& line) {
    if (std::getline(ifs, line)) {
        trim(line);
        return true;
    }
    return false;
}

StrategyConfigGrid::StrategyConfigGrid() : valid_(false), logger_(nullptr) {}

bool StrategyConfigGrid::read_config(const boost::filesystem::path& filename) {
    // Use this to get current working directory
    // try {
    //     boost::filesystem::path current_path = boost::filesystem::current_path();
    //     std::cout << "Current working directory: " << current_path << std::endl;
    // } catch (const boost::filesystem::filesystem_error& e) {
    //     std::cerr << "Error: " << e.what() << std::endl;
    // }

    // cout << "reading config: " << filename.string() << endl;
    std::ifstream ifs(filename.string());
    if (!ifs) {
        cout << "Failed to read strategy config file " << filename << endl;
        // logger_->LogFormatted(LOGERROR, "Failed to read strategy config file %s.", filename.c_str());
        return false;
    }
    std::string line;
    GetLineWithTrim(ifs, line);
    if (line != "Ticks,Lots,Stop,Target") {
        cout << "First line mismatch in the config file." << endl;
        logger_->LogFormatted(LOGERROR, "First line mismatch in %s.", filename.c_str());
        return false;
    }
    std::map<StrategyStudioInt64, StrategyConfigRow> new_rows;
    DoubleHasher hasher;
    bool zero_inserted = false;
    try {
        // cout << "Now reading config file..." << endl;
        while (GetLineWithTrim(ifs, line)) {
            StrategyConfigRow newRow;
            std::vector<std::string> values;
            boost::split(values, line, boost::is_any_of(","));
            if (values.size() != 4) {
                logger_->LogFormatted(LOGERROR, "Not enough values in %s.", line.c_str());
                return false;
            }
            newRow.ticks = boost::lexical_cast<double>(boost::trim_copy(values[0]));
            newRow.lots = boost::lexical_cast<int>(boost::trim_copy(values[1]));
            newRow.stop = boost::lexical_cast<double>(boost::trim_copy(values[2]));
            newRow.target = boost::lexical_cast<double>(boost::trim_copy(values[3]));
            if (hasher(newRow.ticks) == 0) {
                zero_inserted = true;
            }
            logger_->LogFormatted(LOGDEBUG, "Ticks: %lf, Lots: %d, Stop: %lf, Target: %lf", newRow.ticks, newRow.lots, newRow.stop, newRow.target);
            new_rows.insert(std::make_pair(hasher(newRow.ticks), newRow));
        }
    } catch (std::exception& e) {
        logger_->LogFormatted(LOGERROR, "Reading config file %s failed: %s", filename.c_str(), e.what());
        return false;
    }
    if (!zero_inserted) {
        new_rows.insert(std::make_pair(hasher(0.0), StrategyConfigRow{0.0, 0, 0.0, 0.0}));
    }
    std::swap(rows_, new_rows);
    valid_ = true;
    return true;
}

StrategyConfigRow StrategyConfigGrid::at(double change) const {
    if (!valid_) {
        return {0.0, 0, 0.0, 0.0};
    }
    DoubleHasher hasher;
    auto ub_iter = rows_.upper_bound(hasher(change));
    if (ub_iter == rows_.begin()) {
        return ub_iter->second;
    }
    if (ub_iter == rows_.end()) {
        ub_iter--;
        return ub_iter->second;
    }
    auto prev_iter = ub_iter;
    prev_iter--;
    auto ub_change = ub_iter->first;
    auto prev_change = prev_iter->first;
    if (std::abs(ub_change) < std::abs(prev_change)) {
        return ub_iter->second;
    }
    return prev_iter->second;
}