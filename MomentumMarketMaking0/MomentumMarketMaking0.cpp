/*================================================================================
*     Source: ../RCM/StrategyStudio/examples/strategies/SimpleMomentumStrategy/SimpleMomentumStrategy.cpp
*     Last Update: 2021/04/15 13:55:14
*     Contents:
*     Distribution:
*
*
*     Copyright (c) RCM-X, 2011 - 2021.
*     All rights reserved.
*
*     This software is part of Licensed material, which is the property of RCM-X ("Company"), 
*     and constitutes Confidential Information of the Company.
*     Unauthorized use, modification, duplication or distribution is strictly prohibited by Federal law.
*     No title to or ownership of this software is hereby transferred.
*
*     The software is provided "as is", and in no event shall the Company or any of its affiliates or successors be liable for any 
*     damages, including any lost profits or other incidental or consequential damages relating to the use of this software.       
*     The Company makes no representations or warranties, express or implied, with regards to this software.                        
/*================================================================================*/

#ifdef _WIN32
    #include "stdafx.h"
#endif

#include "MomentumMarketMaking0.h"

#include "FillInfo.h"
#include "AllEventMsg.h"
#include "ExecutionTypes.h"
#include <Utilities/Cast.h>
#include <Utilities/utils.h>

#include <cmath>
#include <iostream>
#include <cassert>
#include <algorithm>

using namespace RCM::StrategyStudio;
using namespace RCM::StrategyStudio::MarketModels;
using namespace RCM::StrategyStudio::Utilities;

using namespace std;

MomentumMarketMaking0::MomentumMarketMaking0(StrategyID strategyID, const std::string& strategyName, const std::string& groupName):
    Strategy(strategyID, strategyName, groupName),
    //strategy parameters
    regime_map_(), 
    volatility_map_(),   
    target_inventory(0),
    target_time(),
    current_time(),
    volatility(0),
    strategy_active(true),
    // below are adjustable parameters
    debug_(false),
    absolute_stop(0.2),
    position_size_(100),
    time_interval(1),
    short_window_size_(9),
    long_window_size_(50),
    vol_window_size_(10),
    i_threshold_min(2),
    i_threshold_max(5),
    max_inventory(10000),
    inventory_liquidation_decrement(10),
    inventory_liquidation_interval(10),
    inventory_risk_aversion(0.1),
    liquidity_scale(1.0)
{}

MomentumMarketMaking0::~MomentumMarketMaking0()
{
}

void MomentumMarketMaking0::OnResetStrategyState()
{
    if (debug_)
        cout << "Resetting strategy state" << endl;
    regime_map_.clear();
    volatility_map_.clear();
}

void MomentumMarketMaking0::DefineStrategyParams(){
}

void MomentumMarketMaking0::DefineStrategyCommands(){
}

void MomentumMarketMaking0::RegisterForStrategyEvents(StrategyEventRegister* eventRegister, DateType currDate)
{   
    ReadParameters();
    SetInventoryParams();
    eventRegister->RegisterForRecurringScheduledEvents("target_update", USEquityOpenUTCTime(currDate), USEquityCloseUTCTime(currDate), boost::posix_time::seconds(time_interval));
    eventRegister->RegisterForSingleScheduledEvent("strategy terminate", USEquityCloseUTCTime(currDate) - boost::posix_time::seconds(inventory_liquidation_interval/2));

    eventRegister->RegisterForRecurringScheduledEvents("liquidation_time",USEquityCloseUTCTime(currDate)-boost::posix_time::minutes((inventory_liquidation_decrement+1)*inventory_liquidation_interval),USEquityCloseUTCTime(currDate)-boost::posix_time::minutes(inventory_liquidation_interval),boost::posix_time::minutes(inventory_liquidation_interval));
    eventRegister->RegisterForRecurringScheduledEvents("Account stats", USEquityOpenUTCTime(currDate), USEquityCloseUTCTime(currDate), boost::posix_time::minutes(30));
    target_time = USEquityOpenUTCTime(currDate);
}

void MomentumMarketMaking0::OnScheduledEvent(const ScheduledEventMsg& msg) {
    if (msg.scheduled_event_name() == "liquidation_time"){
        InventoryLiquidation();
        if (debug_)
            cout << "liquidation time" << endl;
    }
    if (msg.scheduled_event_name() == "strategy terminate"){
        TerminateStrategy();
        if (debug_)
            cout << "strategy terminate" << endl;
    }
    if (msg.scheduled_event_name() == "target_update"){
        for (InstrumentSetConstIter it = instrument_begin(); it != instrument_end(); ++it) {
            const Instrument* instrument = it->second;
            StrategyLogic(instrument);
            if (strategy_active)
                OrderRefill(instrument);
        }
    }
    if (msg.scheduled_event_name() == "Account stats"){
        for (InstrumentSetConstIter it = instrument_begin(); it != instrument_end(); ++it) {
            const Instrument* instrument = it->second;
            RecordAccountStats(instrument);
        }
    }
}

void MomentumMarketMaking0::StrategyLogic(const Instrument* instrument){
    AbsoluteStopCheck(instrument);
    //check if we're already tracking the momentum object for this instrument, if not create a new one
    RegimeMapIterator regime_iter = regime_map_.find(instrument);
    if (regime_iter == regime_map_.end()) {
        regime_iter = regime_map_.insert(make_pair(instrument, MarketRegime(short_window_size_, long_window_size_))).first;
    }

    double indicator = regime_iter->second.Update(Midprice(instrument));

    VolatilityMapIterator vol_iter = volatility_map_.find(instrument);
    if (vol_iter == volatility_map_.end()) {
        vol_iter = volatility_map_.insert(make_pair(instrument, VolatilityEstimator(vol_window_size_))).first;
    }

    volatility = vol_iter->second.Update(Midprice(instrument));

    if (debug_)
        cout << "volatility is " << volatility << endl;

    if (regime_iter->second.FullyInitialized() and strategy_active) {
        if (indicator > i_threshold_min * instrument->min_tick_size() and strategy_active){
            target_inventory = CalculateTargetInventory(instrument,indicator);
        } else if (indicator < -i_threshold_min * instrument->min_tick_size() and strategy_active){
            target_inventory = CalculateTargetInventory(instrument,indicator);
        } else {
            target_inventory = 0;
        }
    }
    target_time = target_time + boost::posix_time::seconds(time_interval);
}

void MomentumMarketMaking0::OnTrade(const TradeDataEventMsg& msg){
}

void MomentumMarketMaking0::RecordAccountStats(const Instrument* instrument){
    cout << "Total profit " << portfolio().total_pnl(instrument) << endl;
    cout << "Unrealised profit :- " << portfolio().unrealized_pnl(instrument) << endl;
    cout << "Realised Profit :- " << portfolio().realized_pnl(instrument) << endl;
}

double MomentumMarketMaking0::CalculateInventoryGap(const Instrument* instrument)
{
    int current_inventory = portfolio().position(instrument);
    if (debug_){
        cout << "current inventory is " << current_inventory << endl;
        cout << "target inventory is " << target_inventory << endl;
    }
    return (current_inventory - target_inventory);
}

int MomentumMarketMaking0::CalculateTargetInventory(const Instrument* instrument,double indicator)
{
    double indicator_scale = min(indicator/instrument->min_tick_size(),double(i_threshold_max))/(i_threshold_max - i_threshold_min);
    int position_ub = floor(max_inventory/(double(i_threshold_max)/(i_threshold_max - i_threshold_min)));
    int target = int(indicator_scale*position_ub);
    if (debug_){
        cout << "indicator is " << indicator << endl;
        cout << "indicator scale is " << indicator_scale << endl;
        cout << "target inventory is " << target << endl;
    }
    return target;
}

void MomentumMarketMaking0::OnOrderUpdate(const OrderUpdateEventMsg& msg){
    if (msg.fill_occurred() and strategy_active){
        if (debug_)
            cout << "fill occurred orders repriced" << endl;
        Order* order = const_cast<Order*>(&msg.order());
        Reprice(order);
    }
}

void MomentumMarketMaking0::OnDepth(const MarketDepthEventMsg& msg){
    if (strategy_active){
        if (debug_)
            cout << "updated top quote orders repriced" << endl;
        RepriceAll(msg.adapter_time());
    }
}

double MomentumMarketMaking0::NormalisedTimeRemaining()
{
    TimeDuration t1 = target_time - current_time;
    return 1- (t1.total_microseconds()/(time_interval*1000000.0));
}

void MomentumMarketMaking0::PlaceOrder(const Instrument* instrument, OrderSide action){
    double price = (action == ORDER_SIDE_BUY) ? instrument->top_quote().bid() : instrument->top_quote().ask();
    OrderParams params(*instrument,
                       position_size_,
                       price,
                       MARKET_CENTER_ID_NASDAQ,
                       action,
                       ORDER_TIF_DAY,
                       ORDER_TYPE_LIMIT);

    // Print a message indicating that a new order is being sent
    if (debug_)
    std::cout << "Placing order for size "
            << position_size_
            << " at $"
            << price
            << " for symbol "
            << instrument->symbol()
            << "on the "
            << (action == ORDER_SIDE_BUY ? "bid" : "ask")
            << " side"
            << endl;
    trade_actions()->SendNewOrder(params);
}

void MomentumMarketMaking0::OrderRefill(const Instrument* instrument){
    bool bid_exists = false;
    bool ask_exists = false;
    for (IOrderTracker::WorkingOrdersConstIter ordit = orders().working_orders_begin(); ordit != orders().working_orders_end(); ++ordit) {
        Order* order = const_cast<Order*>(*ordit);
        if (order->params().instrument == instrument){
            if (order->order_side() == ORDER_SIDE_BUY)
                bid_exists = true;
            else if (order->order_side() == ORDER_SIDE_SELL)
                ask_exists = true;
        }
    }
    if (!bid_exists)
        PlaceOrder(instrument, ORDER_SIDE_BUY);
    if (!ask_exists)
        PlaceOrder(instrument, ORDER_SIDE_SELL);
}

void MomentumMarketMaking0::RepriceAll(TimeType time)
{
    current_time = time;
    for (IOrderTracker::WorkingOrdersConstIter ordit = orders().working_orders_begin(); ordit != orders().working_orders_end(); ++ordit) {
        Order* order = const_cast<Order*>(*ordit);
        if (debug_)
        cout << "Repricing order for size "
                << order->params().quantity
                << " at $"
                << order->params().price
                << " for symbol "
                << order->params().instrument->symbol()
                << "on the "
                << (order->order_side() == ORDER_SIDE_BUY ? "bid" : "ask")
                << " side"
                << endl;
        Reprice(order);
    }
}

double MomentumMarketMaking0::Midprice(const Instrument* instrument){
    return (instrument->top_quote().bid() + instrument->top_quote().ask())/2;
}

double MomentumMarketMaking0::ReservationPrice(const Instrument* instrument){
    double price = Midprice(instrument);
    int inventory_gap = CalculateInventoryGap(instrument);
    double time_remaining = NormalisedTimeRemaining();
    double reference_price = price - inventory_gap*inventory_risk_aversion*volatility*volatility*time_remaining;
    if (debug_){
        cout << "time remaining is " << time_remaining << endl;
        cout << "inventory gap is " << inventory_gap << endl;
        cout << "inventory risk aversion is " << inventory_risk_aversion << endl;
        cout << "volatility is " << volatility << endl;
        cout << "reservation price is " << reference_price << endl;
        cout << "current midprice is " << price << endl;
    }
    return reference_price;
}

double MomentumMarketMaking0::CalculateAdjustment(const Instrument* instrument){
    double spread = CalculateCurrentSpread(instrument);
    double orderbook_liquidity = NormaliseLiquidityParameter(instrument);
    double time_remaining = NormalisedTimeRemaining();
    double adjustment = inventory_risk_aversion*volatility*volatility*time_remaining + 2*log(1+inventory_risk_aversion/orderbook_liquidity)/inventory_risk_aversion;
    if (debug_){
        cout << "adjustment is " << adjustment << endl;
        cout << "current_spread is " << spread << endl;
    }
    return adjustment;
}

double MomentumMarketMaking0::CalculateCurrentSpread(const Instrument* instrument){
    double top_bid = instrument->top_quote().bid();
    double top_ask = instrument->top_quote().ask();
    double spread = abs(top_ask - top_bid);
    return spread;
}

double MomentumMarketMaking0::NormaliseLiquidityParameter(const Instrument* instrument){
    double spread = CalculateCurrentSpread(instrument);
    //cout << inventory_risk_aversion*volatility*volatility << endl;
    double orderbook_liquidity = inventory_risk_aversion/(exp((spread-inventory_risk_aversion*volatility*volatility) * inventory_risk_aversion * 0.5) - 1);
    if (debug_)
        cout << "orderbook liquidity is " << orderbook_liquidity << endl;
    return orderbook_liquidity * liquidity_scale;
}

void MomentumMarketMaking0::Reprice(Order* order){
    // Get a reference to OrderParams
    OrderParams& params = order->params(); // Change to reference

    // As 'instrument' is a part of 'params', we can access it directly
    const Instrument* instrument = params.instrument; // Access via params

    double r_price = ReservationPrice(instrument);
    double adjustment = CalculateAdjustment(instrument);

    // Update the price in the original params object
    params.price = (order->order_side() == ORDER_SIDE_BUY) ? r_price - adjustment/2 : r_price + adjustment/2;
    params.price = (order->order_side() == ORDER_SIDE_BUY) ? round(params.price/instrument->min_tick_size())*instrument->min_tick_size() : (round(params.price/instrument->min_tick_size())+1)*instrument->min_tick_size();
    // Send updated order
    trade_actions()->SendCancelReplaceOrder(order->order_id(), params);
    if (debug_){
        // Print a message indicating that a new order is being sent
        cout << "Repricing order for size "
                << params.quantity
                << " at $"
                << params.price
                << " for symbol "
                << instrument->symbol()
                << "on the "
                << (order->order_side() == ORDER_SIDE_BUY ? "bid" : "ask")
                << " side"
                << endl;
        cout << "current best bid and ask are " << instrument->top_quote().bid() << " and " << instrument->top_quote().ask() << endl;
    }
}

void MomentumMarketMaking0::SetInventoryParams(){
    inventory_liquidation_decrement = ceil(max_inventory/inventory_liquidation_decrement)+1;
        if (debug_)
        cout << "liquidation decrement :- " << inventory_liquidation_decrement << endl;
}

void MomentumMarketMaking0::InventoryLiquidation(){
    max_inventory -= inventory_liquidation_decrement;
    if (max_inventory < 0)
        max_inventory = 0;

    if (debug_){
        cout << "liquidating inventory by " << inventory_liquidation_decrement << endl;
        cout << "max inventory is now " << max_inventory << endl;
    }
}

void MomentumMarketMaking0::TerminateStrategy() {
    for (InstrumentSetConstIter it = instrument_begin(); it != instrument_end(); ++it) {
        const Instrument* instrument = it->second;
        LiquidationOrder(instrument);
    }
    strategy_active = false;
}

void MomentumMarketMaking0::LiquidationOrder(const Instrument* instrument){
    int inventory = portfolio().position(instrument);
    double price = (inventory > 0) ? instrument->top_quote().bid() - 10 : instrument->top_quote().ask() + 10;
    OrderSide action = (inventory > 0) ? ORDER_SIDE_SELL : ORDER_SIDE_BUY;

    OrderParams params(*instrument,
                       abs(inventory),
                       price,
                       MARKET_CENTER_ID_NASDAQ,
                       action,
                       ORDER_TIF_DAY,
                       ORDER_TYPE_LIMIT);

    if (debug_){
    // Print a message indicating that a new order is being sent
    std::cout << "Closing out all positions with order for size "
            << inventory
            << " at $"
            << price
            << " for symbol "
            << instrument->symbol()
            << std::endl;
    }
    trade_actions()->SendNewOrder(params);
}

void MomentumMarketMaking0::ReadParameters(){
    string line;
    ifstream myfile("/home/vagrant/ss/sdk/RCM/StrategyStudio/examples/strategies/group_01_project/MomentumMarketMaking0/parameters.txt"); 
    if (myfile.is_open()) {
        while ( getline (myfile, line) ) {
            string key;
            string value;
            istringstream iss(line);
            getline(iss, key, '='); // Extract string up to the '=' character
            getline(iss, value);
            SetParamFromKey(key,value);
        }
        myfile.close();
    } else {
        cout << "Unable to open file to read parameters from file" << endl;
    }
}

void MomentumMarketMaking0::SetParamFromKey(string key, string value) {
    if (key == "debug_") {
        debug_ = (value == "true" || value == "1");
    }
    else if (key == "position_size_") {
        position_size_ = stoi(value);
    }
    else if (key == "inventory_liquidation_decrement") {
        inventory_liquidation_decrement = stoi(value);
    }
    else if (key == "inventory_liquidation_interval") {
        inventory_liquidation_interval = stoi(value);
    }
    else if (key == "max_inventory") {
        max_inventory = stoi(value);
    }
    else if (key == "short_window_size_") {
        short_window_size_ = stoi(value);
    }
    else if (key == "long_window_size_") {
        long_window_size_ = stoi(value);
    }
    else if (key == "vol_window_size_") {
        vol_window_size_ = stoi(value);
    }
    else if (key == "i_threshold_min") {
        i_threshold_min = stoi(value);
    }
    else if (key == "i_threshold_max") {
        i_threshold_max = stoi(value);
    }
    else if (key == "turn_off_time") {
        turn_off_time = stoi(value);
    }
    else if (key == "inventory_risk_aversion") {
        inventory_risk_aversion = stod(value);
    }
    else if (key == "time_interval") {
        time_interval = stoi(value);
    }
    else if (key == "liquidity_scale") {
        liquidity_scale = stod(value);
    }
    else if (key == "absolute_stop") {
        absolute_stop = stod(value);
    }
}

void MomentumMarketMaking0::AbsoluteStopCheck(const Instrument* instrument){
    double pnl = portfolio().total_pnl(instrument)/portfolio().initial_cash();
    if (pnl < -absolute_stop){
        LiquidationOrder(instrument);
        trade_actions()->SendCancelAll();
        strategy_active = false;
    }
}