#pragma once

#ifndef _STRATEGY_STUDIO_LIB_MomentumMarketMaking0_H_
#define _STRATEGY_STUDIO_LIB_MomentumMarketMaking0_H_

#ifdef _WIN32
    #define _STRATEGY_EXPORTS __declspec(dllexport)
#else
    #ifndef _STRATEGY_EXPORTS
    #define _STRATEGY_EXPORTS
    #endif
#endif

#include <Strategy.h>
#include <Analytics/ScalarRollingWindow.h>
#include <Analytics/InhomogeneousOperators.h>
#include <Analytics/IncrementalEstimation.h>
#include <MarketModels/Instrument.h>
#include <Utilities/ParseConfig.h>


#include <vector>
#include <map>
#include <iostream>

using namespace RCM::StrategyStudio;
using namespace std;

class MarketRegime {
public:
    MarketRegime(int short_window_size = 10, int long_window_size = 30) : short_window_(short_window_size), long_window_(long_window_size) {}

    void Reset()
    {
        short_window_.clear();
        long_window_.clear();
    }

    double Update(double val)
    {
        short_window_.push_back(val);
        long_window_.push_back(val);
        return short_window_.Mean() - long_window_.Mean();
    }

    bool FullyInitialized() { return (short_window_.full() && long_window_.full()); }
    
    Analytics::ScalarRollingWindow<double> short_window_;
    Analytics::ScalarRollingWindow<double> long_window_;
};

class VolatilityEstimator {
    public:
        VolatilityEstimator(int window_size = 10) : window_(window_size) {}

        void Reset()
        {
            window_.clear();
        }

        double Update(double val)
        {
            window_.push_back(val);
            return window_.StdDev();
        }

        bool FullyInitialized() { return window_.full(); }

        Analytics::ScalarRollingWindow<double> window_;
};

class MomentumMarketMaking0 : public Strategy {
public:
    typedef boost::unordered_map<const Instrument*, MarketRegime> RegimeMap; 
    // creates an unordered map where keys are instrument names and values are corresponding classes 
    // all these pairs are set to a map called MomentumMap
    typedef RegimeMap::iterator RegimeMapIterator; 
    // we define an iterator that iterates through these key value pairs 
    typedef boost::unordered_map<const Instrument*, VolatilityEstimator> VolatilityMap;

    typedef VolatilityMap::iterator VolatilityMapIterator;

public:
    MomentumMarketMaking0(StrategyID strategyID, const std::string& strategyName, const std::string& groupName);
    ~MomentumMarketMaking0();

public: /* from IEventCallback */
    /**
     * This event triggers whenever trade message arrives from a market data source.
     */ 
    virtual void OnTrade(const TradeDataEventMsg& msg);

    /**
     * This event triggers whenever aggregate volume at best price changes, based 
     * on the best available source of liquidity information for the instrument.
     *
     * If the quote datasource only provides ticks that change the NBBO, top quote will be set to NBBO
     */ 
    virtual void OnTopQuote(const QuoteEventMsg& msg){}
    
    /**
     * This event triggers whenever a new quote for a market center arrives from a consolidate or direct quote feed,
     * or when the market center's best price from a depth of book feed changes.
     *
     * User can check if quote is from consolidated or direct, or derived from a depth feed. This will not fire if
     * the data source only provides quotes that affect the official NBBO, as this is not enough information to accurately
     * mantain the state of each market center's quote.
     */ 
    virtual void OnQuote(const QuoteEventMsg& msg){}
    /**
     * This event triggers whenever a Bar interval completes for an instrument
     */ 
    virtual void OnBar(const BarEventMsg& msg){}

    /**
     * This event contains alerts about the state of the market
     */
    virtual void OnMarketState(const MarketStateEventMsg& msg){}

    /**
     * This event triggers whenever new information arrives about a strategy's orders
     */ 
    virtual void OnOrderUpdate(const OrderUpdateEventMsg& msg);
    
    virtual void OnDepth(const MarketDepthEventMsg& msg);
    /**
     * This event contains strategy control commands arriving from the Strategy Studio client application (eg Strategy Manager)
     */ 
    virtual void OnStrategyControl(const StrategyStateControlEventMsg& msg){}

    virtual void OnScheduledEvent(const ScheduledEventMsg& msg);

    /**
     *  Perform additional reset for strategy state 
     */
    void OnResetStrategyState();

    /**
     * This event contains alerts about the status of a market data source
     */ 
    void OnDataSubscription(const DataSubscriptionEventMsg& msg){}

    /**
     * This event triggers whenever a custom strategy command is sent from the client
     */ 
    void OnParamChanged(StrategyParam& param){}
    /**
     * Notifies strategy for every succesfull change in the value of a strategy parameter.
     *
     * Will be called any time a new parameter value passes validation, including during strategy initialization when default parameter values
     * are set in the call to CreateParam and when any persisted values are loaded. Will also trigger after OnResetStrategyState
     * to remind the strategy of the current parameter values.
     */ 

private: // Helper functions specific to this strategy
    void PlaceOrder(const Instrument* instrument, OrderSide action);
    void OrderRefill(const Instrument* instrument);
    void RepriceAll(TimeType time);
    void Reprice(Order* order);
    double CalculateInventoryGap(const Instrument* instrument);
    int CalculateTargetInventory(const Instrument* instrument,double indicator);
    double NormalisedTimeRemaining();
    void SetInventoryParams();
    void InventoryLiquidation();
    void ReadParameters();
    void SetParamFromKey(string key, string value);
    void StrategyLogic(const Instrument* instrument);
    double ReservationPrice(const Instrument* instrument);
    double Midprice(const Instrument* instrument);
    double CalculateAdjustment(const Instrument* instrument);
    double CalculateCurrentSpread(const Instrument* instrument);
    double NormaliseLiquidityParameter(const Instrument* instrument);
    void TerminateStrategy();
    void LiquidationOrder(const Instrument* instrument);
    void RecordAccountStats(const Instrument* instrument);
    void AbsoluteStopCheck(const Instrument* instrument);

private: /* from Strategy */
    virtual void RegisterForStrategyEvents(StrategyEventRegister* eventRegister, DateType currDate); 
    
    /**
     * Define any params for use by the strategy 
     */     
    virtual void DefineStrategyParams();

    /**
     * Define any strategy commands for use by the strategy
     */ 
    virtual void DefineStrategyCommands();

private:
    boost::unordered_map<const Instrument*, MarketRegime> regime_map_;
    boost::unordered_map<const Instrument*, VolatilityEstimator> volatility_map_;
    int vol_window_size_;
    int position_size_;
    int time_interval;
    TimeType target_time;
    TimeType current_time;
    int short_window_size_;
    int long_window_size_;
    double volatility;
    bool debug_;
    int i_threshold_min;
    int i_threshold_max;
    int target_inventory;
    int turn_off_time;
    int max_inventory;
    int inventory_liquidation_decrement;
    int inventory_liquidation_interval;
    double inventory_risk_aversion;
    bool strategy_active;
    double liquidity_scale;
    double absolute_stop;
};

extern "C" {
    _STRATEGY_EXPORTS const char* GetType()
    {
        return "MomentumMarketMaking0";
    }

    _STRATEGY_EXPORTS IStrategy* CreateStrategy(const char* strategyType,
                                   unsigned strategyID,
                                   const char* strategyName,
                                   const char* groupName) {
        if (strcmp(strategyType, GetType()) == 0) {
            return *(new MomentumMarketMaking0(strategyID, strategyName, groupName));
        } else {
            return NULL;
        }
    }

    _STRATEGY_EXPORTS const char* GetAuthor() {
        return "dlariviere";
    }

    _STRATEGY_EXPORTS const char* GetAuthorGroup() {
        return "UIUC";
    }

    _STRATEGY_EXPORTS const char* GetReleaseVersion() {
        return Strategy::release_version();
    }
}

#endif
