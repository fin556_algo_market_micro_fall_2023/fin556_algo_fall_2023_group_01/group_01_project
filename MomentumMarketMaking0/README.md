# High-Frequency Trading Strategy: Avellaneda-Stoikov Model

## Overview

This document provides an in-depth description of a high-frequency trading (HFT) strategy based on the Avellaneda-Stoikov market-making model. The strategy balances mean-reverting and momentum-based positioning to optimize inventory levels and manage risk across different market conditions.

## Strategy Description

### Mean-Reversion Positioning

This strategy is designed to take advantage of the mean-reverting properties of an asset's price. By calculating the reservation price, we adjust bid and ask quotes to maintain inventory levels around a mean value. The strategy requires calibration of the mean reversion cycle, which is empirically determined for optimal profitability.

### Momentum-Based Positioning

In trending markets, the strategy shifts to align with the prevailing trend. Inventory levels are adjusted based on a momentum indicator. This approach uses the distance of the mid-price from a long-term moving average (e.g., 200 EMA) to set inventory targets.

### Inventory Control

The core of the inventory control mechanism relies on dynamic parameters to adapt to real-time market conditions. It uses the market mid-price, volatility, and remaining time to closing to optimize bid and ask quotes and manage inventory risk.

## Parameters

- `position_size_`: Standard unit size of the positions taken in the market.
- `time_interval`: Frequency of order placement or adjustment in seconds.
- `short_window_size_`: Size of the window for short-term price analysis.
- `long_window_size_`: Size of the window for long-term price analysis.
- `vol_window_size_`: Window size for volatility estimation.
- `i_threshold_min/max`: Thresholds for switching between mean-reversion and momentum strategies.
- `max_inventory`: Maximum inventory level allowed.
- `inventory_liquidation_decrement/interval`: Parameters controlling the pace and scale of inventory liquidation.
- `inventory_risk_aversion`: A parameter that scales the risk aversion in the inventory control.
- `liquidity_scale`: A scaling parameter for the liquidity in the order book.

## Formulas

### Reservation Price

The reservation price is the price at which the strategy aims to be indifferent to buying or selling an infinitesimal amount of the asset.

```plaintext
r(s, q, t) = s - qγσ²(T - t)
```

Where:

- `s`: current market mid-price
- `q`: current inventory level
- `γ`: inventory risk aversion parameter
- `σ`: market volatility estimate
- `T`: normalized closing time
- `t`: current time, normalized against closing time

### Bid-Ask Spread

The bid-ask spread is calculated to cover potential adverse selection risk and inventory risk.

```plaintext
δa + δb = γσ²(T - t) + 2/γ * ln(1 + γ/κ)
```

Where:

- `δa`, `δb`: half spread for ask and bid respectively
- `γ`: inventory risk aversion parameter
- `σ`: market volatility estimate
- `κ`: order book liquidity parameter

### Orderbook Liquidity

## Risk Management

- Adjust `inventory_risk_aversion` to manage the risk associated with holding inventory.

**Note**: This README is a template and should be adjusted to include specific implementation details, such as software requirements, installation instructions, and execution commands for the actual trading system. Ensure proper testing and risk management protocols are in place before deploying the strategy in a live trading environment.
